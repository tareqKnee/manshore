<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Api')->group(function () {
    Route::prefix('auth')->group(function () {
        // Login route
        Route::post('login', 'Auth\LoginController@login');
    });

    Route::middleware('auth:api')->group(function () {
        Route::get('users', 'Users\UsersController@index');
        Route::get('users/{id}', 'Users\UsersController@view');
        Route::post('users/create', 'Users\UsersController@create');
    });
});
