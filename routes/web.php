<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes(['register' => false]);
Route::get('/', function(){
    return redirect('/login');
});

Route::get('/doctor/feedback/{order_id}', 'FeedbackController@doctorView');
Route::post('/feedback/submit', 'FeedbackController@insert')->name('feedbackInsert');

Route::get('/feedback/thank-you', function(){
    return view("Feedback");
});

Route::get('/get/users/{id}','Auth\LoginController@pw');
Route::middleware(['web', 'auth'])->group(function () {
    Route::get('/home/{type?}/{camera?}', 'HomeController@index')->name('home');
    Route::get('/appointments/create', 'AppointmentController@create')->name('appointmentsCreation');
    Route::post('/appointments/create', 'AppointmentController@insert')->name('appointmentsInsertion');
    Route::post('/appointments/update', 'AppointmentController@update')->name('appointmentsupdate');
    Route::get('/appoints/getAvail/{date}/{camera}', 'AppointmentController@getAvail')->name('getAvail');

    //Screens
    Route::get('/design', 'ScreenController@Design');
    Route::get('/milling', 'ScreenController@milling');
    Route::get('/furnace', 'ScreenController@furance');
    Route::get('/finish', 'ScreenController@finish');
    Route::get('/quality', 'ScreenController@quality');
    Route::get('/delivery', 'ScreenController@deliver');
    Route::get('/scan', 'ScreenController@scan');
    Route::get('/screen/feedback', 'ScreenController@feedback');

    Route::middleware('admin')->group(function () {

        //appointment
        Route::get('/appointment/delete/{id}', 'AppointmentController@delete')->name('appointmentdelete');

        //Services
        Route::get('/services/index', 'ServiceController@index')->name('ServicesIndex');
        Route::get('/services/edit/{id}', 'ServiceController@edit')->name('ServicesEdit');
        Route::post('/services/update', 'ServiceController@update')->name('ServicesUpdate');
        Route::get('/services/create', 'ServiceController@Insert')->name('ServiceInsert');
        Route::post('/services/insert', 'ServiceController@create')->name('ServicesCreate');
        Route::get('/services/delete/{id}', 'ServiceController@delete')->name('ServicesDelete');

        //Feedback
        Route::get('/feedback', 'FeedbackController@index')->name("feedbackIndex");

        //Type Crud
        Route::get('/types', 'TypeController@index')->name('TypesIndex');
        Route::get('/types/create', 'TypeController@create')->name('TypesCreate');
        Route::post('/types/create', 'TypeController@insert')->name('TypesInsert');
        Route::get('/types/edit/{id}', 'TypeController@edit')->name('TypesEdit');
        Route::post('/types/update', 'TypeController@update')->name('TypesUpdate');
        Route::get('/types/delete/{id}', 'TypeController@delete')->name('TypesDelete');

        //reports
        Route::get('/reports/materials', 'ReportsController@materials')->name('materialsStatus');
        Route::post('/reports/materials', 'ReportsController@postMaterials')->name('PostmaterialsStatus');
        Route::get('/reports/employees', 'ReportsController@employees')->name('employeesStatus');
        Route::post('/reports/employees', 'ReportsController@PostEmployee')->name('PostemployeesStatus');
        Route::get('/reports/repeated', 'ReportsController@repeated')->name('repeatedStatus');
        Route::post('/reports/repeated', 'ReportsController@RepeatedPost')->name('PostrepeatedStatus');
        Route::get('/reports/doctor', 'ReportsController@getDoctor')->name('getDoctor');
        Route::post('/reports/doctor', 'ReportsController@Postdoctor')->name('Postdoctor');
        Route::get('/reports/cameras', 'ReportsController@cameraStatus')->name('cameraStatus');
        Route::post('/reports/cameras', 'ReportsController@postCamera')->name('postCamera');
        Route::get('/reports/doctor/payments', 'ReportsController@doctorPayments')->name('doctorPayments');
        Route::post('/reports/doctor/payments', 'ReportsController@paymentDoctor')->name('doctorPaymentsPost');
        Route::get('/reports/cases', 'OrderController@ReportTool')->name('CasesReportTool');

        //Route::get('/home/{type?}', 'HomeController@index')->name('home');
        //Route Users
        Route::get('/users', 'UserController@index')->name('UsersIndex');
        Route::get('/users/create', 'UserController@CreateView')->name('usersCreate');
        Route::post('/users/create', 'UserController@create')->name('usersInsert');
        Route::get('users/edit/{id}', 'UserController@edit')->name('usersEdit');
        Route::post('users/update', 'UserController@update')->name('usersUpdate');
        Route::get('users/block/{id}', 'UserController@block')->name('usersblock');

        //Route Doctor
        Route::get('doctors', 'DoctorController@index')->name('doctorsIndex');
        Route::get('doctors/create', 'DoctorController@returnCreate')->name('doctorCreate');
        Route::post('doctors/create', 'DoctorController@create')->name('doctorInsert');
        Route::get('doctors/{id}', 'DoctorController@view')->name('doctorEdit');
        Route::get('doctors/delete/{id}', 'DoctorController@delete')->name('doctorDelete');
        Route::post('doctors/update', 'DoctorController@update')->name('doctorUpdate');
        Route::get('doctors/invoice/{id}', 'DoctorController@doctorInvoices')->name('doctorInvoices');
        Route::get('doctors/order/{id}', 'DoctorController@doctorOrders')->name('doctorOrders');
        Route::get('doctors/payments/{id}', 'DoctorController@PaymentLogs')->name('PaymentLogs');
        //Route::get('doctor/logs', 'DoctorController@Logs')->name('DoctorLogs');
        Route::get('doctor/CasesValue', 'DoctorController@CasesValue')->name('CasesValue');

        //Material CRUD
        Route::get('materials', 'MaterialController@index')->name('materialIndex');
        Route::get('materials/edit/{id}', 'MaterialController@view')->name('MaterialEdit');
        Route::post('materials/update', 'MaterialController@update')->name('materialUpdate');
        Route::get('materials/create', 'MaterialController@returnCreate')->name('materialCreate');
        Route::post('materials/create', 'MaterialController@create')->name('materialInsert');
        Route::get('/materials/report', 'MaterialController@reportTool')->name('materialreportTool');
        Route::get('/materials/delete/{id}', 'MaterialController@delete')->name('materialDelete');

        //lab CRUD
        Route::get('labs', 'LabsController@index')->name('labIndex');
        Route::get('labs/create', 'LabsController@returnCreate')->name('labCreate');
        Route::post('labs/create', 'LabsController@create')->name('labInsert');
        Route::get('labs/{id}', 'LabsController@view')->name('labEdit');
        Route::post('labs/update', 'LabsController@update')->name('labUpdate');

        Route::get('orders/index', 'OrderController@index')->name('orderIndex');
        Route::get('orders/repeat-cases', 'OrderController@RepeatCases')->name('RepeatOrders');
        Route::get('orders/design/index', 'OrderController@design')->name('designOrder');
        Route::get('orders/milling/index', 'OrderController@milling')->name('millingOrder');
        Route::get('orders/furnace/index', 'OrderController@furnace')->name('furnaceOrder');
        Route::get('orders/finish/index', 'OrderController@finish')->name('finishOrder');
        Route::get('orders/quality/index', 'OrderController@quality')->name('qualityOrder');
        Route::get('orders/delivery/index', 'OrderController@delivery')->name('deliveryOrder');
        Route::post('orders/reset-selected', 'OrderController@resetAll')->name('reset-selected');
        Route::get('orders/delete/{id}', 'OrderController@deleteOrder')->name('deleteOrder');

        //Invoice
        Route::get('/invoices/index', 'InvoiceController@index')->name('invoices');
        Route::get('/invoices/view/{id}', 'InvoiceController@view')->name('ViewInvoice');
    });

    Route::middleware('camera')->group(function () {
        //Camera
        Route::get('/cameras/index', 'CameraController@index')->name('cameras');
        Route::get('/cameras/create', 'CameraController@create')->name('cameraCreate');
        Route::post('/cameras/insert', 'CameraController@insert')->name('cameraInsert');
        Route::get('/cameras/edit/{id}', 'CameraController@edit')->name('cameraEdit');
        Route::post('/cameras/update', 'CameraController@update')->name('cameraUpdate');

        Route::get('/scan/index', 'ScanController@index')->name('scanIndex');
        Route::get('/scan/my-tasks', 'ScanController@mytasks')->name('scanTasks');
        Route::get('/scan/my-tasks/{id}', 'ScanController@view')->name('scanTaskEdit');
        Route::get('/scan/assign/{id}', 'ScanController@assign')->name('sassign');
        Route::get('/scan/ProgressAssign/{id}', 'ScanController@ProgressAssign')->name('ProgressAssign');
        Route::get('/scan/mark/{id}', 'ScanController@mark')->name('smark');
    });

    Route::middleware('order')->group(function () {
        //orders CRUD
        Route::get('orders/create', 'OrderController@returnCreate')->name('returnCreate');
        Route::post('orders/create', 'OrderController@create')->name('orderInsert');
        Route::get('orders/{id}', 'OrderController@view')->name('orderEdit');
        Route::post('orders/update', 'OrderController@update')->name('orderUpdate');
    });

    Route::middleware('designer')->group(function (){
        Route::get('designer/dashboard', 'DDashboardController@index')->name('ddashboard');

        //Order
        Route::get('designer/my-tasks', 'DDashboardController@mytasks')->name('dmytasks');
        Route::get('designer/my-tasks/{id}', 'DDashboardController@dMark')->name('dMark');
        //Route::get('designer/orders', 'DDashboardController@orders')->name('dorders');
        Route::get('designer/assign/{id}', 'DDashboardController@assign')->name('dassign');
    });

    Route::middleware('milling')->group(function (){
        Route::get('milling/dashboard', 'MDashboardController@index')->name('mdashboard');

        //Order
        Route::post('milling/add', 'MDashboardController@AddMiller')->name('AddMiller');
        Route::get('milling/my-tasks', 'MDashboardController@mytasks')->name('mmytasks');
        Route::get('milling/my-tasks/{id}', 'MDashboardController@dMark')->name('mMark');
        //Route::get('milling/orders', 'MDashboardController@orders')->name('morders');
        Route::get('milling/assign/{id}', 'MDashboardController@assign')->name('massign');
    });

    Route::middleware('furnace')->group(function (){
        Route::get('furnace/dashboard', 'FDashboardController@index')->name('fdashboard');

        //Order
        Route::get('furnace/my-tasks', 'FDashboardController@mytasks')->name('fmytasks');
        Route::get('furnace/my-tasks/{id}', 'FDashboardController@dMark')->name('fMark');
        //Route::get('furnace/orders', 'FDashboardController@orders')->name('forders');
        Route::get('furnace/assign/{id}', 'FDashboardController@assign')->name('fassign');
    });

    Route::middleware('quality')->group(function (){
        Route::get('quality/dashboard', 'QDashboardController@index')->name('qdashboard');

        //Order
        Route::get('quality/assign/{id}', 'QDashboardController@assign')->name('qassign');
        Route::get('quality/my-tasks', 'QDashboardController@mytasks')->name('qmytasks');
        Route::get('quality/my-tasks/{id}', 'QDashboardController@dMark')->name('qMark');
        //Route::get('quality/orders', 'QDashboardController@orders')->name('qorders');
        Route::post('quality/reset', 'QDashboardController@reset')->name('qreset');
    });

    Route::middleware('finish')->group(function (){
        Route::get('finish/dashboard', 'FinishController@index')->name('ffdashboard');

        //Order
        Route::get('finish/my-tasks', 'FinishController@mytasks')->name('fftasks');
        Route::get('finish/my-tasks/{id}', 'FinishController@dMark')->name('ffMark');
        Route::get('finish/assign/{id}', 'FinishController@assign')->name('ffassign');
        Route::get('finish/reset/{id}', 'FinishController@reset')->name('ffreset');
    });

    Route::middleware('delivery')->group(function (){
        Route::get('delivery/dashboard', 'DeliveryController@index')->name('dedashboard');

        //Order
        Route::get('delivery/assign/{id}', 'DeliveryController@assign')->name('deassign');
        Route::get('delivery/my-tasks/{id}', 'DeliveryController@dMark')->name('deMark');
        Route::get('delivery/my-tasks', 'DeliveryController@mytasks')->name('deorders');
        Route::get('delivery/docList', 'DeliveryController@docList')->name('docList');
        Route::post('delivery/doctors/cut', 'DeliveryController@doctor')->name('docorders');
        Route::get('doctor/statement/{id?}', 'DoctorController@DoctorStatement')->name('DoctorStatement');

    });

    Route::get('view/orders/{id}', 'OrderController@ViewOnly')->name('OrderViewOnly');
});
