<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('order_id')->nullable();
            $table->string('patient_name')->nullable();
            $table->longText('note')->nullable();
            $table->dateTime('deliver_date')->nullable();
            $table->boolean('milled')->default(0);
            $table->string('milled_by')->nullable();
            $table->string('current_status')->default(0);
            $table->string('total_status')->default(0);
            $table->boolean('hidden')->default(0);
            $table->unsignedBigInteger('made_by')->nullable();
            $table->unsignedBigInteger('doctor_id');
            $table->unsignedBigInteger('lab_id')->nullable();
            $table->unsignedBigInteger('service_id')->nullable();
            $table->unsignedBigInteger('app_id')->nullable();
            $table->unsignedBigInteger('created_by')->nullable();
            //$table->unsignedBigInteger('appointment_id')->nullable();
            //$table->unsignedBigInteger('color')->nullable();
            $table->softDeletes();
            $table->foreign('doctor_id')->references('id')->on('doctors')->onDelete('cascade');
            $table->foreign('made_by')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('lab_id')->references('id')->on('labs')->onDelete('cascade');
            $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
