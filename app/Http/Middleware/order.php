<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Cache;

class order
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth()->check()) {
            $permissions = Cache::get('user'.Auth()->user()->id);
            if (Auth()->user()->is_admin || $permissions->contains('permission_id', 1) || $permissions->contains('permission_id', 7)) {

                return $next($request);

            }else{

                return abort(404);

            }

        }else{

            return abort(404);

        }
    }
}
