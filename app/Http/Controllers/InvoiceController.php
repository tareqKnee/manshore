<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Invoice;
use App\doctor;

class InvoiceController extends Controller
{
    public function index(Request $request){
        $orders = Invoice::with('order', 'doctor');

        if($request->q){
            $orders = $orders->whereHas('order', function($q) use($request){
                $q->where('patient_name','like', '%' . $request->q . '%');
            });
        }
        if($request->doctor){
            $orders = $orders->whereHas('order', function($q) use($request){
                $q->where('doctor_id',$request->doctor);
            });
        }
        if($request->from && $request->to){
            $orders = $orders->whereBetween('created_at', [$request->from, $request->to]);
        }
        $orders = $orders->orderBy('order_id', 'DESC')->paginate(20)->appends(['q' => $request->q,'from' => $request->from, 'to' => $request->to, 'doctor' => $request->doctor]);
        $doctor = $request->doctor;
        $from = $request->from;
        $to = $request->to;
        $q = $request->q;
        $doctors = doctor::all();
        return view('invoices.index', compact('orders', 'doctor', 'from', 'to', 'q', 'doctor', 'doctors'));
    }

    public function view($id){
        $order = Invoice::with('order.jobs.material', 'order.doctors.discounts', 'order.jobs.types')->where('id', $id)->first();

        return view('invoices.view', compact('order'));
    }
}
