<?php

namespace App\Http\Controllers;

use App\appointments;
use Illuminate\Http\Request;
use App\order;
use App\doctor;
use App\Camera;

class ScanController extends Controller
{
    public function index(){
        $apps = appointments::with('doctors', 'camera')->where(['status' => 0, 'taken_by' => null])->orderBy('created_at')->paginate(20);

        return view('scan.index', compact('apps'));
    }

    public function mytasks(){
        $apps = appointments::with('doctors', 'camera')->where(['taken_by' => auth()->user()->id])->where('status', '!=', 2)->paginate(20);

        return view('scan.tasks', compact('apps'));
    }

    public function view($id){
        $app = appointments::where(['id' => $id, 'status' => 0, 'taken_by' => Auth()->user()->id])->first();
        $doctors = doctor::all();
        $cameras = Camera::all();

        return view('appointments.view', compact('app', 'doctors', 'cameras'));
    }

    public function assign($id){
        $app = appointments::findOrFail($id);
        $app->taken_by = Auth()->user()->id;
        $app->save();

        return back()->with('success', 'The task has been assigned to you!');
    }

    public function ProgressAssign($id){
        $app = appointments::where(['status' => 0, 'taken_by' => Auth()->user()->id])->findOrFail($id);
        if(!$app->camera_id || !$app->doctor_id){
            return back()->with('error', 'Please fill the required data first!');
        }
        $app->status = 1;
        $app->save();

        return back()->with('success', 'The task has been marked as in progress!');
    }

    public function mark($id){
        $app = appointments::where("taken_by", auth()->user()->id)->findOrFail($id);

        $app->status = 2;
        $app->save();
        $check = order::where('app_id', $app->id)->first();
        if(!$check){
            $order = New order();
            $order->doctor_id = $app->doctor_id;
            $order->app_id = $id;
            $order->save();
            return redirect('/orders/'.$order->id)->with('success', 'A case has been created');
        } else {
            $check->hidden = 0;
            $check->current_status = 0;
            $check->save();

            return back()->with('success', 'The appointment has been marked as done');
        }
    }
}
