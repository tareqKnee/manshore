<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Camera;

class CameraController extends Controller
{
    public function index(){
        $cameras = Camera::paginate(20);

        return view('cameras.index', compact('cameras'));
    }

    public function create(){
        return view('cameras.create');
    }

    public function insert(Request $request){
        $this->validate($request, [
            'name' => 'required|unique:cameras,name',
        ]);

        Camera::insert(['name' => $request->name]);

        return back()->with('success', 'A new camera has been added');
    }

    public function edit($id){
        $camera = Camera::where('id', $id)->first();

        return view('cameras.edit', compact('camera'));
    }

    public function update(Request $request){
        $this->validate($request, [
            'name' => 'required|unique:cameras,name,'.$request->id,
        ]);

        Camera::where('id', $request->id)->update(['name' => $request->name]);

        return back()->with('success', 'Camera has been updated');
    }
}
