<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OrderLog;
use App\doctor;
use App\order;
use App\User;
use App\Camera;
use App\repeat_cases;
use App\PaymentLog;
use App\appointments;
use DB;

class ReportsController extends Controller
{
    public function Materials(){
        $doctors = doctor::all();

        return view('material.status')->with('doctors', $doctors);
    }

    public function postMaterials(Request $request){
        if($request->from && $request->to){
            if($request->doctor == "all"){
                $materials = order::join('invoices', 'invoices.order_id', '=', 'orders.id')
                ->whereBetween('orders.created_at',array($request->from,$request->to))
                ->select(['invoices.id', DB::raw('sum(invoices.amount) as total')])
                ->groupBy('id')
                ->get();
            } else {
                $materials = order::where('orders.doctor_id', $request->doctor)
                            ->whereBetween('orders.created_at',array($request->from,$request->to))
                            ->join('invoices', 'invoices.order_id', '=', 'orders.id')
                            ->select(['invoices.id', DB::raw('sum(invoices.amount) as total')])
                            ->groupBy('id')
                            ->get();
            }
        } else {
            if($request->doctor == "all"){
                $materials = order::join('invoices', 'invoices.order_id', '=', 'orders.id')
                ->select(['invoices.id', DB::raw('sum(invoices.amount) as total')])
                ->groupBy('id')
                ->get();
            } else {
                $materials = order::where('orders.doctor_id', $request->doctor)
                ->join('invoices', 'invoices.order_id', '=', 'orders.id')
                ->select(['invoices.id', DB::raw('sum(invoices.amount) as total')])
                ->groupBy('id')
                ->get();
            }
        }

        return redirect('/reports/materials')->with('materials', $materials);
    }

    public function employees(){
        $users = User::where('is_admin', 0)->get();

        return view('users.status')->with('users', $users);
    }

    public function PostEmployee(Request $request){
        if($request->to && $request->from){
            $status = OrderLog::where(['user_id' => $request->emp, 'current_status' => $request->CS])
            ->whereBetween('created_at',array($request->from,$request->to))->count();
        } else {
            $status = OrderLog::where(['user_id' => $request->emp, 'current_status' => $request->CS])
            ->count();
        }
        if($status === 0){
            $status = -1;
        }
        return redirect('/reports/employees')->with('status', $status)->with('data', $request->all());
    }

    public function Repeated(){
        return view('orders.repeatStatus');
    }

    public function RepeatedPost(Request $request){
        if($request->from && $request->to){
            $status = repeat_cases::whereBetween('created_at',array($request->from,$request->to))->count();
        } else {
            return back()->with('error', 'Fill the from and to inputs');
        }

        return redirect('/reports/repeated')->with('status', $status)->with('data', $request->all());
    }

    public function getDoctor(){
        $doctors = doctor::all();

        return view('doctors.status')->with('doctors', $doctors);
    }

    public function Postdoctor(Request $request){
        if($request->from && $request->to && $request->doc){
            if($request->doc != 'all'){
                $status = order::whereBetween('created_at',[$request->from,$request->to])->where(['doctor_id' => $request->doc, 'total_status' => 1])->count();
            } else {
                $status = order::whereBetween('created_at',[$request->from,$request->to])->where(['total_status' => 1])->count();
            }
        }

        if($request->doc && !$request->to || $request->doc && !$request->from){
            if($request->doc != 'all'){
                $status = order::where(['doctor_id' => $request->doc, 'total_status' => 1])->count();
            } else {
                $status = order::where(['total_status' => 1])->count();
            }
        }

        return redirect('/reports/doctor')->with('status', $status)->with('data', $request->all());
    }

    public function cameraStatus(){
        $cameras = Camera::all();

        return view('appointments.status')->with('cameras', $cameras);
    }

    public function postCamera(Request $request){
        if($request->from && $request->to && $request->camera){
            if($request->camera != 'all'){
                $status = appointments::whereBetween('created_at',array($request->from,$request->to))->where(['camera_id' => $request->camera])->count();
            } else {
                $status = appointments::whereBetween('created_at',array($request->from,$request->to))->count();
            }
        }

        if($request->camera && !$request->from && !$request->to){
            if($request->camera == 'all'){
                $status = appointments::count();
            } else {
                $status = appointments::where(['camera_id' => $request->camera])->count();
            }
        }

        return redirect('/reports/cameras')->with('status', $status)->with('data', $request->all());
    }

    public function doctorPayments(){
        $doctors = doctor::all();

        return view('doctors.paymentstatus')->with('doctors', $doctors);
    }

    public function paymentDoctor(Request $request){
        if($request->from && $request->to && $request->doc){
            if($request->doc != 'all'){
                $status = PaymentLog::with('doctors', 'users')->whereBetween('created_at',array($request->from,$request->to))->where(['doctor_id' => $request->doc])->get();
            } else {
                $status = PaymentLog::with('doctors', 'users')->whereBetween('created_at',array($request->from,$request->to))->get();
            }
        }

        if($request->doc){
            if($request->doc != 'all'){
                $status = PaymentLog::with('doctors', 'users')->where(['doctor_id' => $request->doc])->get();
            } else {
                $status = PaymentLog::with('doctors', 'users')->get();
            }
        }

        return redirect('/reports/doctor/payments')->with('status', $status)->with('data', $request->all());
    }
}
