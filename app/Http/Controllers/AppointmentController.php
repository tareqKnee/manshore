<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\appointments;
use App\doctor;
use App\Camera;

class AppointmentController extends Controller
{
    public function delete($id){
        appointments::where('id', $id)->delete();

        return back()->with('success', 'Appointment has been deleted successfully');
    }

    public function index(){
        $apps = appointments::with('doctors')->withCount('order')->orderBy('id','DESC')->paginate(20);

        return view('appointments.index', compact('apps'));
    }

    public function create(){
        $doctors = doctor::all();
        $cameras = Camera::all();

        return view('appointments.create', compact('doctors', 'cameras'));
    }

    public function getAvail($date, $camera){
        $apps = appointments::where(['date' => $date, 'camera_id' => $camera])->pluck('time');

        return response()->json(['apps' => $apps], 200);
    }

    public function insert(Request $request){
        $this->validate($request, [
            'doctor_id'     => 'required|exists:doctors,id',
            'description'   => 'nullable|max:400',
            'date'          => 'required',
            'time'          => 'required',
            'camera'        => 'required|exists:cameras,id',
        ]);
        $check = appointments::where(['camera_id' => $request->camera, 'date' => $request->date, 'time' => $request->time])->first();
        if($check){
            return back()->with('error', 'Theres an appointment at the same time and date');
        }
        $app = new appointments();

        $app->doctor_id = $request->doctor_id;
        $app->description = $request->description;
        $app->date = $request->date;
        $app->time = $request->time;
        $app->camera_id = $request->camera;
        $app->created_by = Auth()->user()->id;

        $app->save();

        return back()->with('success', 'A new appointment has been created');
    }

    public function update(Request $request){
        $this->validate($request, [
            'id'            => 'required',
            'doctor_id'     => 'required|exists:doctors,id',
            'description'   => 'required|max:400',
            'date'          => 'required',
            'time'          => 'required',
        ]);

        $app = appointments::where('id', $request->id)->first();

        $app->doctor_id = $request->doctor_id;
        $app->description = $request->description;
        $app->camera_id = $request->camera;
        $app->date = $request->date;
        $app->time = $request->time;

        $app->save();

        return back()->with('success', 'The appointment has been updated');
    }
}
