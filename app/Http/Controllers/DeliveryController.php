<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\order;
use App\doctor;
use App\Invoice;
use App\lab;
use App\AccountStatement;
use App\PaymentLog;

class DeliveryController extends Controller
{
    public function index(){
        $orders = order::with('doctors')->where(['made_by' => null, 'current_status' => 5])->orderBy('created_at', 'DESC')->paginate(20);
        $count2 = order::where(['current_status' => 5])->whereNotNull('made_by')->count();

        return view('delivery.dashboard', compact('orders', 'count2'));
    }

    public function dMark($id){

        $order = order::with('doctors')->where(['id' => $id, 'current_status' => 5, 'made_by' => Auth()->user()->id])->find($id);
        $order->update(['current_status' => 6, 'made_by' => Auth()->user()->id, 'total_status' => 1]);

        if($order){
            $invoice = Invoice::where('order_id', $id)->first();
            if($invoice){
                $repeat = explode('R', $order->order_id);
                if(!isset($repeat[1])){
                    $doctor = doctor::where('id', $invoice->doctor_id)->first();
                    $doctor->balance +=  $invoice->amount;
                    $doctor->save();
                    AccountStatement::insert(['balance' => $doctor->balance, 'patient_name' => $order->patient_name, 'debit' => $invoice->amount, 'created_at' => now(), 'updated_at' => now(), 'doctor_id' => $invoice->doctor_id]);
                }
            }
            $this->sendSMS($order, $order->doctors->phone);
            return back()->with('success', 'Your order has been marked as done');
        } else {
            return back()->with('error', "Order hasn't been found");
        }
    }

    function formatMobileNumber($number){
        $mobile_number = preg_replace("/[^0-9]/", "", $number);

        if (mb_strlen($mobile_number) < 11 && substr($mobile_number, 0, 1) == "0") {
            $mobile_number = preg_replace('/^0/', '962', $number);
        }

        if (mb_strlen($mobile_number) < 10) {
            $mobile_number = '962' . $mobile_number;
        }

        return strval($mobile_number);
    }

    function sendSMS($order, $phone){
        $phone = $this->formatMobileNumber($phone);
		$phone = '+' . $phone;
		try{
			$id = config('services.twilio.sid');
			$token = config('services.twilio.token');
			$url = "https://api.twilio.com/2010-04-01/Accounts/".config('services.twilio.sid')."/Messages.json";
            $data = array(
                'Title' => 'Verification Code',
                'From' => 'MOSAIC',
                'To' => $phone,
                'Body' => 'MOSAIC شكراً لتعاملكم مع مختبر'.
               '
               '. 'من خلال الرابط التالي :' . 'http://' . request()->getHost() . '/doctor/feedback/' . $order->id);
			$post = http_build_query($data);
			$x = curl_init($url);
			curl_setopt($x, CURLOPT_POST, true);
			curl_setopt($x, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($x, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($x, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_setopt($x, CURLOPT_USERPWD, "$id:$token");
			curl_setopt($x, CURLOPT_POSTFIELDS, $post);
            $y = curl_exec($x);
            curl_close($x);

			return true;
		} catch (\Exception $e){
			return $e;
		}
    }

    public function assign($id){
        $order = order::where(['id' => $id, 'current_status' => 5, 'made_by' => null])->update(['made_by' => Auth()->user()->id]);

        if($order){
            return back()->with('success', 'The order has been assigned to you');
        } else {
            return back()->with('error', "Sorry.. Someone beat you to it!");
        }
    }

    public function orders(){
        $orders = order::with('doctors')->where(['made_by' => null, 'current_status' => 5])->orderBy('created_at', 'DESC')->paginate(20);

        return view('delivery.orders')->with('orders', $orders);
    }

    public function mytasks(){
        $tasks = order::with('doctors')->where(['made_by' => Auth()->user()->id, 'current_status' => 5])->paginate(20);
        $labs = lab::all();

        return view('delivery.tasks')->with('tasks', $tasks)->with('labs', $labs);
    }

    public function docList(Request $request){
        $search = $request->search;
        if($search){
            $users = doctor::where('name', 'like', '%' . $request->search . '%')->paginate(20)->appends(['search' => $request->search]);
        } else {
            $users = doctor::paginate(20);
        }

        return view('delivery.doctors', compact('users', 'search'));
    }

    public function doctor(Request $request){
        $this->validate($request, [
            'id'     => 'required',
            'balance' => 'required|numeric',
        ]);
        $doctor = doctor::where('id', $request->id)->first();

        if(!$doctor){
            abort(404);
        }
        $balance = $doctor->balance;
        $doctor->balance = $doctor->balance - $request->balance;
        $doctor->save();

        $log = new PaymentLog();
        $log->amount = $request->balance;
        $log->collector = Auth()->user()->id;
        $log->notes = $request->notes;
        $log->doctor_id = $doctor->id;

        $log->save();
        AccountStatement::insert(['patient_name' => "Payment", 'credit' => $request->balance, 'created_at' => now(), 'updated_at' => now(), 'doctor_id' => $request->id, 'balance' => $balance]);

        return back()->with('success', "Doctor's balance has been updated!");
    }
}
