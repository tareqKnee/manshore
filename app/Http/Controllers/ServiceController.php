<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\services;

class ServiceController extends Controller
{
    public function index(Request $request){
        $search = $request->search;
        if($request->search){
            $services = services::where('name', 'like', '%'.$request->search.'%')->paginate(10)->appends(['search' => $request->search]);
        } else {
            $services = services::paginate(10);
        }

        return view('services.index', compact('services', 'search'));
    }

    public function edit($id){
        $service = services::where('id', $id)->first();

        return view('services.edit', compact('service'));
    }

    public function update(Request $request){
        $this->validate($request, [
            'name' => 'required|min:3',
        ]);

        $serv = services::findOrFail($request->id);
        $serv->name = $request->name;

        $serv->save();

        return back()->with('success', 'Service has been updated');
    }

    public function create(Request $request){
        $this->validate($request, [
            'name' => 'required|min:3',
        ]);

        $serv = new services();
        $serv->name = $request->name;

        $serv->save();

        return back()->with('success', 'Service has been created');
    }

    public function Insert(){
        return view('services.create');
    }

    public function delete($id){
        services::where('id', $id)->delete();

        return back()->with('success', 'Deleted successfully');
    }
}
