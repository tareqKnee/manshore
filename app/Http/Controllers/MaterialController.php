<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\material;
use App\JobType;
use App\Job;
use App\doctor;

class MaterialController extends Controller
{
    public function index(Request $request)
    {
        if ($request->search) {
            $materials = material::where('name', 'like', '%' . $request->search . '%')->paginate(20)->appends(['search' => $request->search]);
        } else {
            $materials = material::paginate(20);
        }

        return view('material.index')->with('materials', $materials)->with('search', $request->search);
    }

    public function returnCreate()
    {
        return view('material.create');
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'name'     => 'required|max:30',
            'price'    => 'required|numeric|min:1',
        ]);

        $material = new material();

        try {
            $material->name = $request->name;
            $material->price = $request->price;
            $material->save();

            return back()->with('success', 'Material has been successfully created');
        } catch (\Exception $e) {
            return back()->with('error', $e);
        }
    }

    public function view($id)
    {
        $material = material::where('id', $id)->first();
        if (!$material) {
            abort(404);
        }

        return view('material.edit', compact('material'));
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'name'     => 'required|max:30',
            'price'    => 'required|numeric',
        ]);

        $material = material::where('id', $request->id)->first();
        if (!$material) {
            abort(404);
        }
        try {
            $material->name = $request->name;
            $material->price = $request->price;
            $material->save();

            return back()->with('success', 'Material has been successfully created');
        } catch (\Exception $e) {
            return back()->with('error', $e);
        }
    }

    public function delete($id)
    {
        $del = material::where('id', $id)->delete();
        if ($del) {
            return back()->with('success', 'Material has been successfully deleted');
        } else {
            abort(404);
        }
    }

    public function reportTool(Request $request)
    {
        $materials = material::all();
        $types = JobType::all();
        $doctors = doctor::all();
        $orders = Job::with('order.doctors', 'types', 'material');
        if($request->from && $request->to){
            $orders = $orders->whereBetween('created_at', [$request->from, $request->to]);
        }
        if ($request->material) {
            $orders = $orders->whereHas('material', function ($q) use ($request) {
                if ($request->material[0] != 'all') {
                    $q->whereIn('material_id', $request->material);
                }
            });
        }
        if ($request->doctor) {
            $orders = $orders->whereHas('order', function ($q) use ($request) {
                if ($request->doctor[0] != 'all') {
                    $q->whereIn('doctor_id', $request->doctor);
                }
            });
        }
        if ($request->type) {
            if ($request->type != 'all') {
                $orders = $orders->where('type', $request->type);
            }
        }
        $orders = $orders->orderBy('created_at', 'DESC')->paginate(20)->appends([
            'material' => $request->material,
            'doctor' => $request->doctor,
            'type' => $request->type,
            'from' => $request->from,
            'to' => $request->to,
        ]);

        $material = $request->material;
        $doctor = $request->doctor;
        $type = $request->type;
        $from = $request->from;
        $to = $request->to;

        $ids = $orders->pluck('id');
        $jobs_units = Job::whereIn('order_id', $ids)->get()->pluck('unit_num');
        $total_units = 0;
        foreach($jobs_units as $job){
            $nums = explode(',', $job);
            $total_units += count($nums);
        }
        return view('material.report', compact('materials', 'types', 'orders', 'doctors', 'material', 'doctor', 'type', 'from', 'to', 'total_units'));
    }

    public function reportPost(Request $request)
    {

        return back()->with('materials');
    }
}
