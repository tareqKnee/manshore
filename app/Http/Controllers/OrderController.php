<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\order;
use App\User;
use App\Job;
use App\lab;
use App\Invoice;
use App\doctor;
use App\material;
use App\repeat_cases;
use App\services;
use App\JobType;
use App\AccountStatement;
use App\OrderLog;
use DB;
use Cache;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        if ($request->q) {
            $orders = order::where('order_id', 'like', '%' . $request->q . '%')->with('doctors', 'made', 'Logs');
            if ($request->doctor && $request->doctor != "all") {
                $orders = $orders->whereHas('doctors', function ($q) use ($request) {
                    $q->where('name', 'like', '%' . $request->doctor . '%');
                });
            }
            if ($request->from && $request->to) {
                $orders = $orders->whereBetween('created_at', [$request->from, $request->to]);
            }
            $orders = $this->filterBy($orders, $request);
            if ($request->date == 'asc') {
                $orders = $orders->orderBy('created_at')->paginate(20)->appends(['q' => $request->q, 'date' => $request->date]);
            } elseif ($request->date == 'desc') {
                $orders = $orders->orderBy('created_at', 'DESC')->paginate(20)->appends(['q' => $request->q, 'date' => $request->date]);
            } else {
                $orders = $orders->paginate(20)->appends(['q' => $request->q, 'date' => $request->date, 'from' => $request->from, 'to' => $request->to, 'doctor' => $request->doctor]);
            }
        } else {
            $orders = order::with('doctors', 'made', 'Logs');
            if ($request->doctor && $request->doctor != "all") {
                $orders = $orders->where('doctor_id', $request->doctor);
            }
            $orders = $this->filterBy($orders, $request);
            if ($request->from && $request->to) {
                $orders = $orders->whereBetween('created_at', [$request->from, $request->to]);
            }
            if ($request->date == 'asc') {
                $orders = $orders->orderBy('created_at')->paginate(20)->appends(['q' => $request->q, 'date' => $request->date]);
            } elseif ($request->date == 'desc') {
                $orders = $orders->orderBy('created_at', 'DESC')->paginate(20)->appends(['q' => $request->q, 'date' => $request->date]);
            } else {
                $orders = $orders->orderBy('created_at', 'DESC')->paginate(20)->appends(['q' => $request->q, 'date' => $request->date, 'from' => $request->from, 'to' => $request->to, 'doctor' => $request->doctor]);
            }
        }
        $users = User::with('permissions');
        $designers = $users->whereHas('permissions', function ($q) {
            $q->where('permission_id', 1);
        })->get();
        $millings = $users->whereHas('permissions', function ($q) {
            $q->where('permission_id', 2);
        })->get();
        $furances = $users->whereHas('permissions', function ($q) {
            $q->where('permission_id', 3);
        })->get();
        $finishers = $users->whereHas('permissions', function ($q) {
            $q->where('permission_id', 4);
        })->get();
        $quality = $users->whereHas('permissions', function ($q) {
            $q->where('permission_id', 5);
        })->get();
        $delivery = $users->whereHas('permissions', function ($q) {
            $q->where('permission_id', 6);
        })->get();
        $doctors = doctor::all();

        return view('orders.index', compact('designers', 'millings', 'furances', 'finishers', 'quality', 'delivery', 'doctors'))->with('orders', $orders)->with('date', $request->date)->with('q', $request->q)
            ->with('from', $request->from)->with('to', $request->to)->with('doctor', $request->doctor);
    }

    function filterBy($orders, $request)
    {
        if ($request->milled_by && $request->milled_by != 'all') {
            $orders = $orders->whereHas('logs', function ($q) use ($request) {
                $q->where('current_status', 2);
                $q->where('user_id', $request->milled_by);
            });
        }
        if ($request->designed_by && $request->designed_by != 'all') {
            $orders = $orders->whereHas('logs', function ($q) use ($request) {
                $q->where('current_status', 1);
                $q->where('user_id', $request->designed_by);
            });
        }
        if ($request->furnaced_by && $request->furnaced_by != 'all') {
            $orders = $orders->whereHas('logs', function ($q) use ($request) {
                $q->where('current_status', 3);
                $q->where('user_id', $request->furnaced_by);
            });
        }
        if ($request->finished_by && $request->finished_by != 'all') {
            $orders = $orders->whereHas('logs', function ($q) use ($request) {
                $q->where('current_status', 4);
                $q->where('user_id', $request->finished_by);
            });
        }
        if ($request->quality_by && $request->quality_by != 'all') {
            $orders = $orders->whereHas('logs', function ($q) use ($request) {
                $q->where('current_status', 5);
                $q->where('user_id', $request->quality_by);
            });
        }
        if ($request->delivered_by && $request->delivered_by != 'all') {
            $orders = $orders->whereHas('logs', function ($q) use ($request) {
                $q->where('current_status', 6);
                $q->where('user_id', $request->delivered_by);
            });
        }

        return $orders;
    }

    public function ReportTool(Request $request){
            $orders = order::with('jobs','invoice','Logs.user','doctors','services');
            if($request->q){
                $orders = $orders->where('order_id', 'like', '%' . $request->q . '%');
            }
            if($request->status == 'completed'){
                $orders = $orders->where('total_status', 1);
            }
            if($request->status == 'not'){
                $orders = $orders->where('total_status', 0);
            }
            if($request->impression_id && $request->impression_id != 'all'){
                $orders = $orders->where('service_id', $request->impression_id);
            }
            if($request->material_id){
                $orders = $orders->whereHas('Jobs.material', function($q) use ($request){
                    $q->where('material_id', $request->material_id);
                });
            }
            if($request->job){
                $orders = $orders->whereHas('Jobs', function($q) use ($request){
                    $q->where('type', $request->job);
                });
            }
            if ($request->doctor && $request->doctor != "all") {
                $orders = $orders->where('doctor_id', $request->doctor);
            }
            if ($request->from && $request->to) {
                $orders = $orders->whereBetween('created_at', [$request->from, $request->to]);
            }
            $orders = $this->filterBy($orders, $request);
            $ids = $orders->get()->pluck('id');
            if ($request->date == 'asc') {
                $orders = $orders->orderBy('created_at')->paginate(20)->appends(['q' => $request->q, 'date' => $request->date, 'from' => $request->from, 'to' => $request->to, 'doctor' => $request->doctor,
                'material_id' => $request->material_id,
                'job' => $request->job,
                'impression_id' => $request->impression_id,
                'status' => $request->status]);
            } elseif ($request->date == 'desc') {
                $orders = $orders->orderBy('created_at', 'DESC')->paginate(20)->appends(['q' => $request->q, 'date' => $request->date, 'from' => $request->from, 'to' => $request->to, 'doctor' => $request->doctor,
                'material_id' => $request->material_id,
                'job' => $request->job,
                'impression_id' => $request->impression_id,
                'status' => $request->status]);
            } else {
                $orders = $orders->paginate(20)->appends(['q' => $request->q, 'date' => $request->date, 'from' => $request->from, 'to' => $request->to, 'doctor' => $request->doctor,
                'material_id' => $request->material_id,
                'job' => $request->job,
                'impression_id' => $request->impression_id,
                'status' => $request->status]);
            }

            $designers = User::whereHas('permissions', function ($q) {
                $q->where('permission_id', 1);
            })->get();
            $millings = User::whereHas('permissions', function ($q) {
                $q->where('permission_id', 2);
            })->get();
            $furances = User::whereHas('permissions', function ($q) {
                $q->where('permission_id', 3);
            })->get();
            $finishers = User::whereHas('permissions', function ($q) {
                $q->where('permission_id', 4);
            })->get();
            $quality = User::whereHas('permissions', function ($q) {
                $q->where('permission_id', 5);
            })->get();

            $delivery = User::whereHas('permissions', function ($q) {
                $q->where('permission_id', 6);
            })->get();
        $materials = Material::all();
        $jobs = JobType::all();
        $impression = services::all();
        $doctors = doctor::all();
        $total_amount = Invoice::whereIn('order_id', $ids)->sum('amount');
        $jobs_units = Job::whereIn('order_id', $ids)->get()->pluck('unit_num');
        $total_units = 0;
        foreach($jobs_units as $job){
            $nums = explode(',', $job);
            $total_units += count($nums);
        }
        return view('orders.report', compact('total_units', 'designers', 'millings', 'furances', 'finishers', 'quality', 'delivery', 'materials', 'jobs', 'impression', 'doctors', 'total_amount'))->with('orders', $orders)->with('date', $request->date)->with('q', $request->q)
            ->with('from', $request->from)->with('to', $request->to)->with('doctor', $request->doctor);
    }

    public function design(Request $request)
    {
        if ($request->q) {
            $orders = order::where('order_id', 'like', '%' . $request->q . '%')->where('current_status', 0)->with('doctors', 'made')->orderBy('created_at', 'DESC')->paginate(20)->appends(['q' => $request->q]);;
        } else {
            $orders = order::with('doctors', 'made')->where('current_status', 0)->orderBy('created_at', 'DESC')->paginate(20);
        }

        return view('orders.index')->with('orders', $orders)->with('date', $request->date)->with('q', $request->q);
    }

    public function milling(Request $request)
    {
        if ($request->q) {
            $orders = order::where('order_id', 'like', '%' . $request->q . '%')->where('current_status', 1)->with('doctors', 'made')->orderBy('created_at', 'DESC')->paginate(20)->appends(['q' => $request->q]);;
        } else {
            $orders = order::with('doctors', 'made')->where('current_status', 1)->orderBy('created_at', 'DESC')->paginate(20);
        }

        return view('orders.index')->with('orders', $orders)->with('date', $request->date)->with('q', $request->q);
    }

    public function furnace(Request $request)
    {
        if ($request->q) {
            $orders = order::where('order_id', 'like', '%' . $request->q . '%')->where('current_status', 2)->with('doctors', 'made')->orderBy('created_at', 'DESC')->paginate(20)->appends(['q' => $request->q]);;
        } else {
            $orders = order::with('doctors', 'made')->where('current_status', 2)->orderBy('created_at', 'DESC')->paginate(20);
        }

        return view('orders.index')->with('orders', $orders)->with('date', $request->date)->with('q', $request->q);
    }

    public function finish(Request $request)
    {
        if ($request->q) {
            $orders = order::where('order_id', 'like', '%' . $request->q . '%')->where('current_status', 3)->with('doctors', 'made')->orderBy('created_at', 'DESC')->paginate(20)->appends(['q' => $request->q]);;
        } else {
            $orders = order::with('doctors', 'made')->where('current_status', 3)->orderBy('created_at', 'DESC')->paginate(20);
        }

        return view('orders.index')->with('orders', $orders)->with('date', $request->date)->with('q', $request->q);
    }

    public function quality(Request $request)
    {
        if ($request->q) {
            $orders = order::where('order_id', 'like', '%' . $request->q . '%')->where('current_status', 4)->with('doctors', 'made')->orderBy('created_at', 'DESC')->paginate(20)->appends(['q' => $request->q]);;
        } else {
            $orders = order::with('doctors', 'made')->where('current_status', 4)->orderBy('created_at', 'DESC')->paginate(20);
        }

        return view('orders.index')->with('orders', $orders)->with('date', $request->date)->with('q', $request->q);
    }

    public function delivery(Request $request)
    {
        if ($request->q) {
            $orders = order::where('order_id', 'like', '%' . $request->q . '%')->where('current_status', 5)->with('doctors', 'made')->orderBy('created_at', 'DESC')->paginate(20)->appends(['q' => $request->q]);;
        } else {
            $orders = order::with('doctors', 'made')->where('current_status', 5)->orderBy('created_at', 'DESC')->paginate(20);
        }

        return view('orders.index')->with('orders', $orders)->with('date', $request->date)->with('q', $request->q);
    }

    public function returnCreate()
    {
        $doctors = doctor::all();
        $materials = material::all();
        $labs = lab::all();
        $services = services::all();
        $types = JobType::all();

        return view('orders.create', compact('doctors', 'materials', 'labs', 'types', 'services'));
    }

    public function resetAll(Request $request)
    {
        $this->validate($request, [
            'id'   => 'required',
            'id.*' => 'required|exists:orders,id',
        ]);

        $update = order::whereIn('id', $request->id)->update(['made_by' => null, 'current_status' => $request->status, 'note' => $request->note]);
        if ($update) {
            foreach ($request->id as $id) {
                repeat_cases::insert(['user_id' => Auth()->user()->id, 'order_id' => $id, 'created_at' => now(), 'updated_at' => now()]);
            }
            return response()->json(['status' => true], 200);
        } else {
            return response()->json(['status' => false], 422);
        }
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'order_id'          => 'required|unique:orders,order_id',
            'patient_name' => 'required',
            'doctor_id'   => 'required',
            'lab_id'      => 'required_if:milled,1|exists:labs,id|nullable',
            'service_id'  => 'required|exists:services,id',
            'milled'      => 'boolean',
            'milled_by'   => 'required_if:milled,0|nullable',
            'delivery_date' => 'required|date|after:yesterday'
        ]);

        $doctor = doctor::with('discounts')->where('id', $request->doctor_id)->first();
        if (!$doctor) {
            abort(404);
        }
        $transaction = DB::transaction(function () use ($request, $doctor) {
            $order = new order();
            $order->order_id = $request->order_id;
            $order->patient_name = $request->patient_name;
            $order->doctor_id = $request->doctor_id;
            if ($request->milled == 1) {
                $request->milled = 1;
                $order->lab_id = $request->lab_id;
            } else {
                $order->milled = 0;
                $order->milled_by = $request->milled_by;
            }
            $order->deliver_date = $request->delivery_date;
            $order->service_id = $request->service_id;
            $order->note = $request->note;
            $order->created_by = Auth()->user()->id;
            $order->save();
            $data = array();
            if ($request->repeat) {
                $sum = 0;
                foreach ($request->repeat as $unit) {
                    if(!isset($unit['type'])){
                        return -1;
                    }
                    if (isset($unit['unit_num'])) {
                        $unit_num = implode(", ", $unit['unit_num']);
                    } else {
                        $unit_num = null;
                    }
                    $job = new Job();
                    $job->unit_num = $unit_num;
                    $job->type = $unit['type'];
                    $job->color = $unit['color'];
                    $job->style = $unit['style'];
                    $job->material_id = $unit['material_id'];
                    $job->order_id = $order->id;
                    $job->save();
                    array_push($data, $unit['material_id']);
                    $d_type = $doctor->discounts->where('material_id', $unit['material_id'])->first();
                    if ($d_type && $d_type->type == 0) {
                        $materials = material::where('id', $unit['material_id'])->sum('price') - $d_type->discount;
                    } elseif ($d_type) {
                        $materials = material::where('id', $unit['material_id'])->sum('price') - (material::where('id', $unit['material_id'])->sum('price') * $d_type->discount / 100);
                    } else {
                        $materials = material::where('id', $unit['material_id'])->sum('price');
                    }
                    if (isset($unit['unit_num'])) {
                        $sum = $sum + (count($unit['unit_num']) * $materials);
                        // if($job->type == 9){
                        //     $sum = $sum + 1 * $materials;
                        // } else {
                        //     $sum = $sum + (count($unit['unit_num']) * $materials);
                        // }
                    }
                }
            }

            if (isset($sum)) {
                $Invoice = new Invoice();
                $Invoice->amount = $sum;
                $Invoice->order_id = $order->id;
                $Invoice->doctor_id = $request->doctor_id;
                $Invoice->save();
            }

            return true;
        });

        if ($transaction) {
            return back()->with('success', 'New case has been placed');
        } else {
            return back()->with('error', 'Something went wrong');
        }
    }

    public function view($id)
    {
        $order = order::with('jobs', 'CreatedBy')->where('id', $id)->first();
        if(!$order){
            abort(404);
        }
        $explode = explode('_', $order->order_id);
        if(count($explode) && isset($explode[count($explode) - 1][0]) && $explode[count($explode) - 1][0] == 'R'){
            return back()->with('error', 'You cant edit repeated cases');
        }
        $doctors = doctor::all();
        $materials = material::all();
        $labs = lab::all();
        $types = JobType::all();
        $services = services::all();

        return view('orders.edit', compact('doctors', 'materials', 'order', 'labs', 'types', 'services'));
    }

    public function RepeatCases(Request $request, repeat_cases $rep)
    {
        $rep = $rep->whereHas('order.made')->with('order.doctors', 'order.made', 'repeater');
        if ($request->doctor && $request->doctor != "all") {
            $rep->whereHas('order', function ($q) use ($request) {
                $q->where('doctor_id', $request->doctor);
            });
        }
        if ($request->patient) {
            $rep->whereHas('order', function ($q) use ($request) {
                $q->where('patient_name', 'like', '%' . $request->patient . '%');
            });
        }
        if ($request->date == 'asc') {
            $rep->orderBy('created_at');
        }
        if ($request->date == 'desc') {
            $rep->orderBy('created_at', 'desc');
        }

        $orders = $rep->paginate(20)->appends(['doctor', 'patient', 'date']);
        $doctor = $request->doctor;
        $patient = $request->patient;
        $date = $request->date;
        $doctors = doctor::all();

        return view('orders.repeat', compact('orders', 'doctor', 'patient', 'date', 'doctors'));
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'order_id'          => 'unique:orders,order_id,' . $request->id,
            'patient_name' => 'required',
            'doctor_id'   => 'required',
            'lab_id'      => 'required_if:milled,1|exists:labs,id|nullable',
            'service_id'  => 'required|exists:services,id',
            'milled'      => 'boolean',
            'milled_by'   => 'required_if:milled,0|nullable',
            'delivery_date' => 'required|date|after:yesterday'
        ]);
        $permissions = Cache::get('user'.Auth()->user()->id);
        $transaction = DB::transaction(function () use ($request, $permissions) {
            $order = order::where('id', $request->id)->first();
            if (!$order) {
                return false;
            }
            if($permissions->contains('permission_id', 7) && count($permissions) > 1 || Auth()->user()->is_admin == 1){
                $order->order_id = $request->order_id;
            }
            if($order->milled){
                $order->milled = $request->milled;
            }
            $order->patient_name = $request->patient_name;
            $order->doctor_id = $request->doctor_id;
            if ($request->milled == 1) {
                $order->lab_id = $request->lab_id;
                $order->milled_by = null;
            } else {
                if ($order->milled != null) {
                    $order->milled_by = $request->milled_by;
                    $order->lab_id = null;
                }
            }
            $order->deliver_date = $request->delivery_date;
            $order->service_id = $request->service_id;
            $order->note = $request->note;
            // if (!Auth()->user()->is_admin && $permissions->contains('permission_id', 7) && count($permissions) > 1) {
            //     $order->made_by = Auth()->user()->id;
            // }
            $order->save();
            $sum = 0;
            if ($request->job_ids) {
                $i = 0;
                foreach ($request->job_ids as $id) {
                    $num_num = 'old_unit_num_' . $id;
                    $color = 'old_color_' . $id;
                    if(!$request->$num_num){
                        return false;
                    }
                    if (is_array($request->old_unit_num[$i])) {
                        $unit_num = implode(", ", $request->$num_num[$i]);
                    } else {
                        $unit_num = implode(", ", $request->$num_num);
                    }
                    $mat = 'old_material_id_' . $id;
                    if (count($request->job_ids) == 1) {
                        $style = 'old_style_' . $id;
                        $type = 'old_type_' . $id;
                        Job::where('id', $id)->update([
                            'unit_num' => $unit_num, 'type' => $request->$type,
                            'color' => $request->$color, 'style' => $request->$style, 'material_id' =>  $request->$mat
                        ]);
                        $materials = material::where('id',  $request->$mat)->sum('price');
                        $sum = $sum + (count($request->$num_num) * $materials);
                        $i++;
                    } else {
                        $style = 'old_style_' . $id;
                        $type = 'old_type_' . $id;
                        $color = 'old_color_' . $id;
                        Job::where('id', $id)->update([
                            'unit_num' => $unit_num, 'type' => $request->$type,
                            'color' => $request->$color, 'style' => $request->$style, 'material_id' => $request->$mat
                        ]);
                        $materials = material::where('id',  $request->$mat)->sum('price');
                        // if($request->old_type == 9){
                        //     $sum = $sum + 1 * $materials;
                        // } else {
                        //     $sum = $sum + (count($request->$num_num) * $materials);
                        // }
                        $sum = $sum + (count($request->$num_num) * $materials);
                        $i++;
                    }
                }
            }
            if ($request->repeat) {
                foreach ($request->repeat as $unit) {
                    if (!isset($unit['unit_num'])) {
                        continue;
                    }
                    $unit_num = implode(", ", $unit['unit_num']);
                    if(!isset($unit['type'])){
                        return -1;
                    }
                    $job = new Job();
                    $job->unit_num = $unit_num;
                    $job->type = $unit['type'];
                    $job->color = $unit['color'];
                    $job->style = $unit['style'];
                    $job->material_id = $unit['material_id'];
                    $job->order_id = $order->id;
                    $job->save();
                    $materials = material::where('id', $unit['material_id'])->sum('price');
                    if($request->old_type == 9){
                        $sum = $sum + 1 * $materials;
                    } else {
                        $sum = $sum + (count($unit['unit_num']) * $materials);
                    }
                }
            }

            if (isset($sum)) {
                $update = Invoice::where(['order_id' => $order->id])->update(['amount' => $sum]);
                if(!$update){
                    $Invoice = new Invoice();
                    $Invoice->amount = $sum;
                    $Invoice->order_id = $order->id;
                    $Invoice->doctor_id = $request->doctor_id;
                    $Invoice->save();
                }
            }

            return true;
        });
        if ($transaction) {
            return back()->with('success', 'Case has been updated');
        } else {
            return back()->with('error', 'Something went wrong');
        }
    }

    public function delJob($id)
    {
        $job = Job::where('id', $id)->first();
        $job->delete();

        return back()->with('success', 'Job deleted');
    }

    public function deleteOrder($id){
        $order = order::where('id', $id)->first();
        $log = OrderLog::where('id', $order->id)->where('current_status', 6)->first();
        $invoice = Invoice::where('order_id', $id)->first();
        if($log){
            doctor::where('id', $order->doctor_id)->update(['balance' => DB::raw('balance-'.$invoice->amount)]);
            AccountStatement::where(['doctor_id' => $order->doctor_id, 'debit' => $invoice->amount])->delete();
        }
        if($invoice){
            $invoice->delete();
        }
        $order->delete();

        return back()->with('success', 'Order has been successfully deleted');
    }

    public function ViewOnly($id)
    {
        $order = order::with('jobs.material', 'doctors', 'lab')->where('id', $id)->first();
        $types = JobType::all();
        $services = services::all();
        $materials = material::all();
        $labs = lab::all();

        return view('orders.viewonly', compact('order', 'services', 'types', 'labs', 'materials'));
    }
}
