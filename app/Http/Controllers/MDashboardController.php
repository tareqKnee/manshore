<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\order;
use App\lab;

class MDashboardController extends Controller
{
    public function index(){
        $orders = order::with('doctors')->where(['made_by' => null, 'current_status' => 1])->orderBy('created_at', 'DESC')->paginate(20);
        $count2 = order::where(['current_status' => 1])->whereNotNull('made_by')->count();
        $count3 = order::where(['current_status' => 2, 'made_by' => null])->count();

        return view('milling.dashboard', compact('orders', 'count2', 'count3'));
    }

    public function dMark($id){
        $order = order::where(['id' => $id, 'current_status' => 1, 'made_by' => auth()->user()->id])->find($id);
        $order->update(['current_status' => 2, 'made_by' => null]);

        if($order){
            return back()->with('success', 'Your order has been marked as done');
        } else {
            return back()->with('error', "Order hasn't been found");
        }
    }

    public function mytasks(){
        $tasks = order::with('doctors')->where(['made_by' => Auth()->user()->id, 'current_status' => 1])->paginate(20);
        $labs = lab::all();

        return view('milling.tasks')->with('tasks', $tasks)->with('labs', $labs);
    }

    public function assign($id){
        $order = order::where(['id' => $id, 'current_status' => 1, 'made_by' => null])->update(['made_by' => Auth()->user()->id]);

        if($order){
            return back()->with('success', 'The order has been assigned to you');
        } else {
            return back()->with('error', "Sorry.. Someone beat you to it!");
        }
    }

    public function AddMiller(Request $request){
        $this->validate($request, [
            'lab_id'      => 'required_if:milled,1|exists:labs,id|nullable',
            //'service_id'  => 'required|exists:services,id',
            'milled'      => 'boolean',
            'milled_by'   => 'required_if:milled,0|nullable',
        ]);
        $order = order::where(['id' => $request->id, 'current_status' => 1, 'made_by' => Auth()->user()->id])->first();

        if($order){
            $order->milled = $request->milled;
            if ($request->milled == 1) {
                $order->lab_id = $request->lab_id;
                $order->milled_by = null;
            } else {
                $order->milled_by = $request->milled_by;
                $order->lab_id = null;
            }
            $order->save();
            return back()->with('success', 'The order has been updated');
        } else {
            return back()->with('error', "Sorry.. Someone beat you to it!");
        }
    }

    public function orders(){
        $orders = order::with('doctors')->where(['made_by' => null, 'current_status' => 1])->orderBy('created_at', 'DESC')->paginate(20);

        return view('milling.order')->with('orders', $orders);
    }
}
