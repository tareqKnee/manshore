<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\order;
use App\appointments;
use App\Camera;
use DB;
use Illuminate\Support\Facades\Cache;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($type = 'design', $camera = null)
    {
        if(Auth()->user()->is_admin){
            if ($type == 'design') {
                $design = order::with('doctors', 'made')->where('current_status', 0)->whereNull('deleted_at')->paginate(6);
                $j = 0;
                $group = DB::table('orders')->whereNull('deleted_at')->select('current_status', DB::raw('count(*) as total'))
                    ->groupBy('current_status')->get();
            } elseif ($type == "milling") {
                $milling = order::with('doctors', 'made')->where('current_status', 1)->whereNull('deleted_at')->paginate(6);
                $j = 1;
                $group = DB::table('orders')->whereNull('deleted_at')->select('current_status', DB::raw('count(*) as total'))
                    ->groupBy('current_status')->get();
            } elseif ($type == "furnace") {
                $furnace = order::with('doctors', 'made')->where('current_status', 2)->whereNull('deleted_at')->paginate(6);
                $j = 2;
                $group = DB::table('orders')->whereNull('deleted_at')->select('current_status', DB::raw('count(*) as total'))
                    ->groupBy('current_status')->get();
            } elseif ($type == "finish") {
                $finish = order::with('doctors', 'made')->whereNull('deleted_at')->where('current_status', 3)->paginate(6);
                $j = 3;
                $group = DB::table('orders')->whereNull('deleted_at')->select('current_status', DB::raw('count(*) as total'))
                    ->groupBy('current_status')->get();
            } elseif ($type == "quality") {
                $group = DB::table('orders')->whereNull('deleted_at')->select('current_status', DB::raw('count(*) as total'))
                    ->groupBy('current_status')->get();
                $j = 4;
                $quality = order::with('doctors', 'made')->whereNull('deleted_at')->where('current_status', 4)->paginate(6);
            } elseif ($type == "delivery") {
                $group = DB::table('orders')->whereNull('deleted_at')->select('current_status', DB::raw('count(*) as total'))
                    ->groupBy('current_status')->get();
                $j = 5;
                $delivery = order::with('doctors', 'made')->whereNull('deleted_at')->where('current_status', 5)->paginate(6);
            }
            $dates = collect();
            foreach (range(-6, 0) as $i) {
                $date = now()->addDays($i)->format('Y-m-d');
                $dates->put($date, 0);
            }
            $orders = order::where('created_at', '>=', $dates->keys()->first())
                ->groupBy('date')
                ->orderBy('date')
                ->get([
                    DB::raw('DATE( created_at ) as date'),
                    DB::raw('COUNT( * ) as "count"')
                ])
                ->pluck('count', 'date');
            $dates = $dates->merge($orders);
            if($camera && $camera != "all"){
                $appointments = appointments::with('camera', 'doctors')->where('camera_id', $camera)->where('date', '>=', now()->format('Y-m-d'))->get();
            } else {
                $appointments = appointments::with('camera', 'doctors')->where('date', '>=', now()->format('Y-m-d'))->get();
            }
            $cameras = Camera::all();
            $graph2 = DB::table('orders')->where('current_status', $j)->select('current_status', DB::raw('count(*) as total'))
            ->groupBy('current_status')->get();

            $count_made = order::where(['current_status' => $j])->whereNotNull('made_by')->count();
            return view('index', compact('design', 'milling', 'furnace', 'quality', 'delivery', 'finish', 'group', 'dates', 'appointments', 'count_made', 'graph2', 'cameras'));
        }
        $permissions = Cache::get('user'.Auth()->user()->id);
        if($permissions->contains('permission_id', 1)){
            return redirect('designer/dashboard');
        } elseif($permissions->contains('permission_id', 2)){
            return redirect('milling/dashboard');
        } elseif($permissions->contains('permission_id', 3)){
            return redirect('furnace/dashboard');
        }elseif($permissions->contains('permission_id', 4)){
            return redirect('finish/dashboard');
        }elseif($permissions->contains('permission_id', 5)){
            return redirect('quality/dashboard');
        }elseif($permissions->contains('permission_id', 6)){
            return redirect('delivery/dashboard');
        }elseif($permissions->contains('permission_id', 7)){
            return redirect('scan/index');
        }
    }
}
