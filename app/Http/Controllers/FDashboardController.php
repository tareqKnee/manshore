<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\order;

use function GuzzleHttp\Promise\task;

class FDashboardController extends Controller
{
    public function index(){
        $orders = order::with('doctors')->where(['made_by' => null, 'current_status' => 2])->orderBy('created_at', 'DESC')->paginate(20);
        $count2 = order::where(['current_status' => 2])->whereNotNull('made_by')->count();
        $count3 = order::where(['current_status' => 3, 'made_by' => null])->count();

        return view('furnace.dashboard', compact('orders', 'count2', 'count3'));
    }

    public function dMark($id){
        $order = order::where(['id' => $id, 'current_status' => 2, 'made_by' => auth()->user()->id])->find($id);
        $order->update(['current_status' => 3, 'made_by' => null]);

        if($order){
            return back()->with('success', 'Your order has been marked as done');
        } else {
            return back()->with('error', "Order hasn't been found");
        }
    }

    public function mytasks(){
        $tasks = order::with('doctors')->where(['made_by' => Auth()->user()->id, 'current_status' => 2])->paginate(20);

        return view('furnace.tasks')->with('tasks', $tasks);
    }

    public function assign($id){
        $order = order::where(['id' => $id, 'current_status' => 2, 'made_by' => null])->update(['made_by' => Auth()->user()->id]);

        if($order){
            return back()->with('success', 'The order has been assigned to you');
        } else {
            return back()->with('error', "Sorry.. Someone beat you to it!");
        }
    }

    public function orders(){
        $orders = order::with('doctors')->where(['made_by' => null, 'current_status' => 2])->orderBy('created_at', 'DESC')->paginate(20);

        return view('furnace.order')->with('orders', $orders);
    }
}
