<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\lab;

class LabsController extends Controller
{
    public function index(Request $request){
        $labs = lab::where('name', 'like', '%' . $request->search . '%')->paginate(20)->appends(['search' => $request->search]);

        return view('labs.index')->with('labs', $labs);
    }

    public function returnCreate(){
        return view('labs.create');
    }

    public function create(Request $request){
        $this->validate($request, [
            'name'     => 'required|max:60',
            'phone'    => 'required',
            'address'  => 'required|max:100',
        ]);

        $lab = New lab();

        try{
            $lab->name = $request->name;
            $lab->phone = $request->phone;
            $lab->address = $request->address;
            $lab->save();

            return back()->with('success', 'Lab has been successfully created');
        } catch(\Exception $e){
            return back()->with('error', $e);
        }
    }

    public function view($id){
        $lab = lab::where('id', $id)->first();

        return view('labs.edit')->with('lab', $lab);
    }

    public function update(Request $request){
        $this->validate($request, [
            'id'       => 'required',
            'name'     => 'required|max:60',
            'phone'    => 'required',
            'address'  => 'required|max:100',
        ]);

        $lab = lab::where('id', $request->id)->first();
        if(!$lab){
            abort(404);
        }

        try{
            $lab->name = $request->name;
            $lab->phone = $request->phone;
            $lab->address = $request->address;
            $lab->save();

            return back()->with('success', 'Lab has been successfully created');
        } catch(\Exception $e){
            return back()->with('error', $e);
        }
    }
}
