<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Permission;

class PermissionController extends Controller
{
    public function index(){
        $permissions = Permission::paginate(20);

        return $permissions;
    }

    public function create(Request $request){
        $this->validate($request, [
            'name'     => 'required|max:30',
        ]);

        Permission::create(['name' => $request->name, 'created_at' => now(), 'updated_at' => now()]);

        return true;
    }

    public function delete(Request $request){
        $this->validate($request, [
            'id'     => 'required',
        ]);

        $permission = Permission::where('id', $request->id)->first();
        if($permission){
            $permission->delete();

            return true;
        }

        abort(404);
    }
}
