<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\order;

class DDashboardController extends Controller
{
    public function index(){
        $orders = order::with('doctors')->where(['made_by' => null, 'current_status' => 0])->orderBy('created_at', 'DESC')->paginate(20);
        $count2 = order::where(['current_status' => 0])->whereNotNull('made_by')->count();
        $count3 = order::where(['current_status' => 1, 'made_by' => null])->count();

        return view('designer.dashboard', compact('orders', 'count2', 'count3'));
    }

    public function mytasks(){
        $tasks = order::with('doctors')->where(['made_by' => Auth()->user()->id, 'current_status' => 0])->paginate(20);

        return view('designer.tasks')->with('tasks', $tasks);
    }

    public function dMark($id){
        $order = order::where(['id' => $id, 'current_status' => 0, 'made_by' => auth()->user()->id])->find($id);
        if(!$order->patient_name || !$order->deliver_date || !$order->service_id){
            return back()->with('error', 'Please fill the required data first!');
        }
        $order->update(['current_status' => 1, 'made_by' => null]);

        if($order){
            return back()->with('success', 'Your order has been marked as done');
        } else {
            return back()->with('error', "Order hasn't been found");
        }
    }

    public function orders(){
        $orders = order::with('doctors')->where(['made_by' => null, 'current_status' => 0, 'hidden' => 0])->orderBy('created_at', 'DESC')->paginate(20);

        return view('designer.orders')->with('orders', $orders);
    }

    public function assign($id){
        $order = order::where(['id' => $id, 'current_status' => 0, 'made_by' => null])->update(['made_by' => Auth()->user()->id]);

        $o = order::where(['id' => $id, 'current_status' => 0])->first();
        if(!$o->order_id){
            $o->order_id = Auth()->user()->id.'_'.now()->format('Y').now()->format('m').now()->format('d').'_';
            if(!$o->created_by){
                $o->created_by = Auth()->user()->id;
            }
            $o->save();
        }
        if($order){
            return back()->with('success', 'The order has been assigned to you');
        } else {
            return back()->with('error', "Sorry.. Someone beat you to it!");
        }
    }
}
