<?php

namespace App\Http\Controllers;

use App\appointments;
use Illuminate\Http\Request;
use App\order;
use App\repeat_cases;

class QDashboardController extends Controller
{
    public function index(){
        $orders = order::with('doctors')->where(['made_by' => null, 'current_status' => 4])->orderBy('created_at', 'DESC')->paginate(20);
        $count2 = order::where(['current_status' => 4])->whereNotNull('made_by')->count();
        $count3 = order::where(['current_status' => 5, 'made_by' => null])->count();

        return view('quality.dashboard', compact('orders', 'count2', 'count3'));
    }

    public function dMark($id){
        $order = order::where(['id' => $id, 'current_status' => 4, 'made_by' => Auth()->user()->id])->find($id);
        $order->update(['current_status' => 5, 'made_by' => null]);

        if($order){
            /*
            $invoice = Invoice::where('order_id', $id)->first();
            $doctor = doctor::where('id', $invoice->doctor_id)->first();
            if($doctor->type == 'percentage'){
                $doctor->balance = $doctor->balance - ($invoice->amount - ($invoice->amount * $doctor->discount/100));
                $doctor->save();
            } else {
                $doctor->balance = $doctor->balance - ($invoice->amount - $doctor->discount);
                $doctor->save();
            }*/
            return back()->with('success', 'Your order has been marked as done');
        } else {
            return back()->with('error', "Order hasn't been found");
        }
    }

    public function mytasks(){
        $tasks = order::with('doctors')->where(['made_by' => Auth()->user()->id, 'current_status' => 4])->paginate(20);

        return view('quality.tasks')->with('tasks', $tasks);
    }

    public function assign($id){
        $order = order::where(['id' => $id, 'current_status' => 4, 'made_by' => null])->update(['made_by' => Auth()->user()->id]);

        if($order){
            return back()->with('success', 'The order has been assigned to you');
        } else {
            return back()->with('error', "Sorry.. Someone beat you to it!");
        }
    }

    public function reset(Request $request){
        if(Auth()->user()->is_admin){
            if($request->note){
                if($request->status == "scan"){
                    $app = order::where('id', $request->id)->first();
                    $not = $app->note;
                    $order = order::where(['id' => $request->id ])->update(['made_by' =>null, 'current_status' => 0, 'note' => $not.'<br>'.$request->note, 'hidden' => 1]);
                    if($app->app_id){
                        appointments::where('id', $app->app_id)->update(['status' => 0, 'taken_by' => null, 'date' => null, 'time' => null, 'camera_id' => null,
                        'created_by' => null]);
                    } else {
                        $id = appointments::insertGetId(['status' => 0, 'taken_by' => null, 'date' => null, 'time' => null, 'camera_id' => null,
                        'created_by' => null, 'doctor_id' => $app->doctor_id]);
                        $app->app_id = $id;
                        $app->save();
                    }
                    if($app->total_status == 1){
                        $repeat = explode('R', $app->order_id);
                        if(isset($repeat[1])){
                            $num = (int) $repeat[1];
                            $app->order_id = $repeat[0]."R".($num + 1);
                            $app->total_status = 0;
                            $app->save();
                        } else {
                            $app->order_id = $app->order_id."_R1";
                            $app->total_status = 0;
                            $app->save();
                        }
                    }
                } else {
                    $app = order::where('id', $request->id)->first();
                    $not = $app->note;
                    $order = order::where(['id' => $request->id ])->update(['made_by' =>null, 'current_status' => $request->status, 'note' => $not.'<br>'.$request->note]);
                    if($app->total_status == 1){
                        $repeat = explode('R', $app->order_id);
                        if(isset($repeat[1])){
                            $num = (int) $repeat[1];
                            $app->order_id = $repeat[0]."R".($num + 1);
                            $app->total_status = 0;
                            $app->save();
                        } else {
                            $app->order_id = $app->order_id."_R1";
                            $app->total_status = 0;
                            $app->save();
                        }
                    }
                }
            }
            else {
                if($request->status == "scan"){
                    $app = order::where('id', $request->id)->first();
                    $not = $app->note;
                    $order = order::where(['id' => $request->id ])->update(['made_by' =>null, 'current_status' => 0, 'note' => $not.'<br>'.$request->note, 'hidden' => 1]);
                    if($app->app_id){
                        appointments::where('id', $app->app_id)->update(['status' => 0, 'taken_by' => null, 'date' => null, 'time' => null, 'camera_id' => null,
                        'created_by' => null]);
                    } else {
                        $id = appointments::insertGetId(['status' => 0, 'taken_by' => null, 'date' => null, 'time' => null, 'camera_id' => null,
                        'created_by' => null, 'doctor_id' => $app->doctor_id]);
                        $app->app_id = $id;
                        $app->save();
                    }
                    if($app->total_status == 1){
                        $repeat = explode('R', $app->order_id);
                        if(isset($repeat[1])){
                            $num = (int) $repeat[1];
                            $app->order_id = $repeat[0]."R".($num + 1);
                            $app->total_status = 0;
                            $app->save();
                        } else {
                            $app->order_id = $app->order_id."_R1";
                            $app->total_status = 0;
                            $app->save();
                        }
                    }
                } else {
                    $app = order::where('id', $request->id)->first();
                    $not = $app->note;
                    $order = order::where(['id' => $request->id ])->update(['made_by' =>null, 'current_status' => $request->status, 'note' => $not.'<br>'.$request->note]);
                    if($app->total_status == 1){
                        $repeat = explode('R', $app->order_id);
                        if(isset($repeat[1])){
                            $num = (int) $repeat[1];
                            $app->order_id = $repeat[0]."R".($num + 1);
                            $app->total_status = 0;
                            $app->save();
                        } else {
                            $app->order_id = $app->order_id."_R1";
                            $app->total_status = 0;
                            $app->save();
                        }
                    }
                }
            }
        } else {
            if($request->note){
                if($request->status == "scan"){
                    $app = order::where('id', $request->id)->first();
                    $not = $app->note;
                    $order = order::where(['id' => $request->id ])->update(['made_by' =>null, 'current_status' => 0, 'note' => $not.'<br>'.$request->note, 'hidden' => 1]);
                    if($app->app_id){
                        appointments::where('id', $app->app_id)->update(['status' => 0, 'taken_by' => null, 'date' => null, 'time' => null, 'camera_id' => null,
                        'created_by' => null]);
                    } else {
                        $id = appointments::insertGetId(['status' => 0, 'taken_by' => null, 'date' => null, 'time' => null, 'camera_id' => null,
                        'created_by' => null, 'doctor_id' => $app->doctor_id]);
                        $app->app_id = $id;
                        $app->save();
                    }
                } else {
                    $order = order::where(['id' => $request->id, 'current_status' => 4, 'made_by' => Auth()->user()->id])->update(['made_by' =>null, 'current_status' => $request->status, 'note' => $request->note]);
                }
            } else {
                if($request->status == "scan"){
                    $app = order::where('id', $request->id)->first();
                    $not = $app->note;
                    $order = order::where(['id' => $request->id ])->update(['made_by' =>null, 'current_status' => 0, 'note' => $not.'<br>'.$request->note, 'hidden' => 1]);
                    if($app->app_id){
                        appointments::where('id', $app->app_id)->update(['status' => 0, 'taken_by' => null, 'date' => null, 'time' => null, 'camera_id' => null,
                        'created_by' => null]);
                    } else {
                        $id = appointments::insertGetId(['status' => 0, 'taken_by' => null, 'date' => null, 'time' => null, 'camera_id' => null,
                        'created_by' => null, 'doctor_id' => $app->doctor_id]);
                        $app->app_id = $id;
                        $app->save();
                    }
                } else {
                    $order = order::where(['id' => $request->id, 'current_status' => 4, 'made_by' => Auth()->user()->id])->update(['made_by' =>null, 'current_status' => $request->status]);
                }
            }
        }

        if($order){
            repeat_cases::insert(['user_id' => Auth()->user()->id, 'order_id' => $request->id, 'created_at' => now(), 'updated_at' => now()]);
            return back()->with('success', 'The order has been reset');
        } else {
            return back()->with('error', "Sorry.. something went wrong!");
        }
    }

    public function orders(){
        $orders = order::with('doctors')->where(['made_by' => null, 'current_status' => 4])->orderBy('created_at', 'DESC')->paginate(20);

        return view('quality.orders')->with('orders', $orders);
    }

    public function repeatCases(){
        $orders = order::with('doctors')->where(['made_by' => null, 'current_status' => 4])->orderBy('created_at', 'DESC')->paginate(20);

        return view('quality.orders')->with('orders', $orders);
    }
}
