<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\doctor;
use App\Invoice;
use App\order;
use App\PaymentLog;
use App\discount;
use App\material;
use App\AccountStatement;

class DoctorController extends Controller
{
    public function delete($id){
        doctor::where('id', $id)->delete();

        return back()->with('success', 'Doctor has been deleted successfully');
    }

    public function Logs(Request $request)
    {
        $orders = Invoice::with('order', 'doctor')->paginate(10);
        $orders2 = PaymentLog::with('doctors')->paginate(10);
        $total = $orders->merge($orders2);
        $total = $total->sortBy(function ($post) {
            return $post->created_at;
        });
        $total = $total->forPage(isset($_GET['page']) ? $_GET['page'] : '1', 20);

        return view('doctors.logs', compact('total'));
    }

    public function index(Request $request, doctor $doc)
    {
        if ($request->search) {
            $users = $doc->where('name', 'like', '%' . $request->search . '%');
            if ($request->amount == 'asc') {
                $users = $users->orderBy('balance');
            } elseif ($request->amount == 'desc') {
                $users = $users->orderBy('balance', 'DESC');
            }
            $users = $users->paginate(20)->appends(['search' => $request->search, 'amount' => $request->amount]);
        } else {
            $users = $doc;
            if ($request->amount == 'asc') {
                $users = $users->orderBy('balance');
            } elseif ($request->amount == 'desc') {
                $users = $users->orderBy('balance', 'DESC');
            }
            $users = $users->withCount('order')->paginate(20)->appends(['search' => $request->search, 'amount' => $request->amount]);
        }

        return view('doctors.index')->with('users', $users)->with('search', $request->search)->with('amount', $request->amount);
    }

    public function view($id)
    {
        $user = doctor::with('discounts')->where('id', $id)->first();
        if (!$user) {
            abort(404);
        }
        $materials = material::all();
        return view('doctors.edit')->with('user', $user)->with('materials', $materials);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'name'     => 'required|max:30',
            'phone'    => 'required',
            'address'  => 'required',
            //'discount' => 'required|numeric|min:1',
            //'type'     => 'required',
        ]);

        $doctor = doctor::where('id', $request->id)->first();
        if (!$doctor) {
            abort(404);
        }

        //update.
        $doctor->name = $request->name;
        $doctor->phone = $request->phone;
        $doctor->address = $request->address;
        // $doctor->discount = $request->discount;
        // $doctor->type = $request->type;
        $doctor->save();
        discount::where('doctor_id', $request->id)->delete();
        if (is_array($request->ids)) {
            foreach ($request->ids as $mat) {
                $discount = new discount();
                $o_type = "old_type_".$mat;
                $o_discount = "old_discount_".$mat;
                $o_material = "old_material_".$mat;
                $discount->type = $request->$o_type[0];
                $discount->discount = $request->$o_discount[0];
                $discount->material_id = $request->$o_material[0];
                $discount->doctor_id = $request->id;
                $discount->save();
            }
        }
        if(is_array($request->repeat)){
            foreach ($request->repeat as $rep) {
                if(!isset($rep["type"]) || !isset($rep['discount']) || !isset($rep['material'])){
                    continue;
                }
                $discount = new discount();
                $discount->type = $rep['type'];
                $discount->discount = $rep['discount'];
                $discount->material_id = $rep['material'];
                $discount->doctor_id = $doctor->id;
                $discount->save();
            }
        }
        return back()->with('success', 'Doctor has been successfully updated');
    }

    public function returnCreate()
    {
        $materials = material::all();

        return view('doctors.create', compact('materials'));
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'name'     => 'required|max:30',
            'phone'    => 'required',
            'address'  => 'required',
            //'discount' => 'required|numeric|min:0',
            //'type'     => 'required',
        ]);

        $doctor = new doctor();
        $doctor->name = $request->name;
        $doctor->phone = $request->phone;
        $doctor->address = $request->address;
        $doctor->discount = 30;
        $doctor->type = "fixed";
        $doctor->save();
        foreach ($request->repeat as $rep) {
            if(isset($rep['discount'])){
                $discount = new discount();
                $discount->type = $rep['type'];
                $discount->discount = $rep['discount'];
                $discount->material_id = $rep['material'];
                $discount->doctor_id = $doctor->id;
                $discount->save();
            }
        }
        return back()->with('success', 'Doctor has been successfully created');
    }

    public function doctorInvoices($id)
    {
        $orders = Invoice::with('order')->where('doctor_id', $id)->paginate(20);
        $from = null;
        $to = null;
        $doctor = null;
        $q = null;

        $doctors = doctor::all();
        return view('invoices.index', compact('orders', 'q', 'from', 'to', 'doctor', 'doctors'));
    }

    public function doctorOrders($id, Request $request)
    {
        $q = $request->q;
        $date = $request->date;
        if ($request->q) {
            $orders = order::where('order_id', 'like', '%' . $request->q . '%')->where('doctor_id', $id)->with('doctors', 'made');
            if ($request->date == 'asc') {
                $orders = $orders->orderBy('created_at')->paginate(20)->appends(['q' => $request->q, 'date' => $request->date]);
            } elseif ($request->date == 'desc') {
                $orders = $orders->orderBy('created_at', 'DESC')->paginate(20)->appends(['q' => $request->q, 'date' => $request->date]);
            } else {
                $orders = $orders->paginate(20)->appends(['q' => $request->q, 'date' => $request->date]);
            }
        } else {
            $orders = order::with('doctors', 'made')->where('doctor_id', $id);
            if ($request->date == 'asc') {
                $orders = $orders->orderBy('created_at')->paginate(20)->appends(['q' => $request->q, 'date' => $request->date]);
            } elseif ($request->date == 'desc') {
                $orders = $orders->orderBy('created_at', 'DESC')->paginate(20)->appends(['q' => $request->q, 'date' => $request->date]);
            } else {
                $orders = $orders->paginate(20)->appends(['q' => $request->q, 'date' => $request->date]);
            }
        }
        $from = null;
        $to = null;
        $doctor = null;

        return view('orders.index', compact('orders', 'date', 'q', 'from', 'to', 'doctor'));
    }

    public function PaymentLogs($id)
    {
        $payments = PaymentLog::with('doctors', 'users')->where('doctor_id', $id)->orderBy('created_at', 'DESC')->paginate(20);

        return view('payments.index', compact('payments'));
    }

    public function CasesValue(Request $request){
        $orders = Invoice::with('order', 'doctor');
        $doctors = doctor::all();

        $doctor = $request->doc;
        $from = $request->from;
        $to = $request->to;
        if($request->doc != "all"){
            $orders = $orders->whereHas('order', function($q) use($doctor){
                $q->where('doctor_id', $doctor);
            });
        }
        if($request->from && $request->to){
            $orders = $orders->whereBetween('created_at', [$request->from, $request->to]);
        }

        $orders = $orders->sum('amount');
        $data = $request->all();

        return view('doctors.total', compact('orders','doctors', 'doctor', 'from', 'to', 'data'));
    }

    public function DoctorStatement($id = null, Request $request){
        $id  = $id == null ? $request->id : $id;
        $from = $request->from;
        $to = $request->to;
        if($request->from && $request->to){
            $stats = AccountStatement::where('doctor_id', $id)->whereBetween('created_at', [$request->from, $request->to])->orderBy('created_at')->get();
            $before = AccountStatement::where('doctor_id', $id)->where('created_at', '<', $request->from)->orderBy('created_at', 'DESC')->sum('balance');
        } else {
            $stats = AccountStatement::where('doctor_id', $id)->whereBetween('created_at', [now()->subDays(30), now()])->orderBy('created_at')->get();
            $before = null;
        }

        return view('doctors.statement', compact('stats', 'from', 'to', 'id', 'before'));
    }
}
