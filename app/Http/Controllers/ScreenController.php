<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\order;
use App\appointments;
use App\feedback;

class ScreenController extends Controller
{
    public function Design(){
        $orders = order::with('doctors', 'made')->where('current_status', 0)->paginate(7);
        $title = "Design";
        return view('screen', compact('orders', 'title'));
    }

    public function milling(){
        $orders = order::with('doctors', 'made')->where('current_status', 1)->paginate(7);
        $title = "Milling";
        return view('screen', compact('orders', 'title'));
    }

    public function furance(){
        $orders = order::with('doctors', 'made')->where('current_status', 2)->paginate(7);
        $title = "Furance";
        return view('screen', compact('orders', 'title'));
    }

    public function finish(){
        $orders = order::with('doctors', 'made')->where('current_status', 3)->paginate(7);
        $title = "Finish and build up";
        return view('screen', compact('orders', 'title'));
    }

    public function quality(){
        $orders = order::with('doctors', 'made')->where('current_status', 4)->paginate(7);
        $title = "Quality control";
        return view('screen', compact('orders', 'title'));
    }

    public function deliver(){
        $orders = order::with('doctors', 'made')->where('current_status', 5)->paginate(7);
        $title = "Delivery";
        return view('screen', compact('orders', 'title'));
    }

    public function scan(){
        $orders = appointments::with('doctors')->orderBy('id','DESC')->paginate(7);
        $title = "Scan";

        return view('appointments', compact('orders', 'title'));
    }

    public function feedback(){
        $orders = feedback::with('doctor', 'order')->paginate(7);
        $title = "Feedback";

        return view('feedbackscreen', compact('orders', 'title'));
    }
}
