<?php

namespace App\Http\Controllers;

use App\Job;
use App\JobType;
use Illuminate\Http\Request;

class TypeController extends Controller
{
    public function index(){
        $types = JobType::paginate(20);

        return view('types.index', compact('types'));
    }

    public function edit($id){
        $type = JobType::where('id', $id)->first();
        if(!$type){
            abort(404);
        }

        return view('types.edit', compact('type'));
    }

    public function create(){
        return view('types.create');
    }

    public function update(Request $request){
        $this->validate($request, [
            'name'   => 'required|string'
        ]);

        $type = JobType::where('id', $request->id)->first();
        if(!$type){
            abort(404);
        }

        $type->name = $request->name;
        $type->save();

        return back()->with('success', 'The job type has been updated');
    }

    public function insert(Request $request){
        $this->validate($request, [
            'name'   => 'required|string'
        ]);

        $type = new JobType();

        $type->name = $request->name;
        $type->save();

        return back()->with('success', 'The job type has been created');
    }

    public function delete($id){
        $type = JobType::where('id', $id)->delete();
        if($type){
            return back()->with('success', 'The job type has been deleted');
        }
    }
}
