<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\feedback;
use App\order;

class FeedbackController extends Controller
{
    public function doctorView($order_id){
        $order = order::with('doctors')->where('id', $order_id)->first();
        $check = feedback::where('order_id', $order_id)->first();
        if($check){
            abort(404);
        }
        if(!$order){
            abort(404);
        }

        return view('feedbacks.doctorView', compact('order'));
    }

    public function insert(Request $request){
        $this->validate($request, [
            'rate' => 'required',
            'note' => 'required',
        ]);
        $feed = new feedback();
        $feed->doctor_id = $request->doctor_id;
        $feed->patient_name = $request->patient_name;
        $feed->order_id = $request->order_id;
        $feed->note = $request->note;
        $feed->rate = $request->rate;
        $feed->save();

        return redirect('/feedback/thank-you')->with('success', 'Thank you so much for your feedback!');
    }

    public function index(Request $request){
        $feeds = feedback::with('doctor', 'order');

        if($request->patient_name){
            $feeds = $feeds->where('patient_name', 'like', '%' . $request->search . '%' );
        }
        if($request->doctor_id){
            $feeds = $feeds->where('doctor_id', $request->doctor_id);
        }
        if($request->order_id){
            $feeds = $feeds->where('order_id', $request->order_id);
        }
        if($request->rate){
            $feeds = $feeds->where('rate', $request->rate);
        }

        $feeds = $feeds->paginate(20);
        $doctor = $request->doctor_id;
        $patient_name = $request->patient_name;
        $rate = $request->rate;
        $order_id = $request->order_id;
        return view('feedbacks.index', compact('feeds', 'doctor', 'rate', 'patient_name', 'order_id'));
    }
}
