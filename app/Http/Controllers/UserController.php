<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\UserPermission;
use App\Permission;
use DB;
use Hash;
use Illuminate\Support\Facades\Cache;

class UserController extends Controller
{
    public function index(Request $request, User $usrs)
    {
        $users = $usrs;
        if ($request->search) {
            $users = $users->where('email', 'like', '%' . $request->search . '%');
        }
        if($request->status == 1) {
            $users = $users->where('status', 1);
        }
        if($request->status == "disabled"){
            $users = $users->where('status', 0);
        }
        $users = $users->paginate(20)->appends(['status' => $request->status, 'search' => $request->search]);

        return view('users.index')->with('users', $users)->with('status', $request->status)->with('search', $request->search);
    }

    public function CreateView()
    {
        $permissions = Permission::all();
        return view('users.create')->with('permissions', $permissions);
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'email'    => 'required|email|unique:users,email',
            'username' => 'required|unique:users,username',
            'first_name'     => 'required',
            'last_name'    => 'required',
            'password' => 'required|confirmed|min:6',
            'password_confirmation' => 'required',
            'phone'    => 'required',
            'permission' => 'required_if:is_admin,null',
            'permission.*' => 'exists:permissions,id',
        ]);
        $transaction = DB::transaction(function ()  use ($request) {
            $admin = $request->is_admin ? 1 : 0;
            $users = User::create([
                'first_name' => $request->first_name, 'last_name' => $request->last_name, 'email' => $request->email, 'password' => Hash::make($request->password), 'phone' => $request->phone, 'username' => $request->username,
                'is_admin' => $admin
            ]);
            if (!$request->is_admin && $request->permission) {
                foreach ($request->permission as $permission) {
                    $perm = new UserPermission();
                    $perm->user_id = $users->id;
                    $perm->permission_id = $permission;
                    $perm->save();
                }
            }
            return $users;
        });
        if ($transaction == true) {
            return back()->with('success', 'The user has been successfully created');
        } else {
            return back()->with('error', 'Something went wrong!');
        }
    }

    public function edit($id)
    {
        $user = User::with('permissions')->where('id', $id)->first();
        if (!$user) {
            abort(404);
        }
        $permissions = Permission::all();

        return view('users.edit')->with('user', $user)->with('permissions', $permissions);
    }

    public function block($id){
        $user = User::where('id', $id)->first();
        if (!$user) {
            abort(404);
        }
        if($user->status){
            $user->status = 0;
        } else {
            $user->status = 1;
        }

        $user->save();
        return back()->with('success', 'User has been updated');
    }

    public function update(Request $request)
    {
        $user = User::where('id', $request->id)->first();
        if (!$user) {
            abort(404);
        }
        $this->validate($request, [
            'id'    => 'required',
            'email'    => 'required|email|unique:users,email,' . $user->id,
            'first_name'     => 'required',
            'last_name'     => 'required',
            'phone' => 'required',
            'permission' => 'required_if:is_admin,null',
            'permission.*' => 'exists:permissions,id',
            'status' => 'nullable',
            'password_confirmation' => 'min:6|max:200|nullable',
        ]);
        $transaction = DB::transaction(function ()  use ($request, $user) {
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->phone = $request->phone;
            $user->email = $request->email;
            if ($request->permission) {
                UserPermission::where('user_id', $request->id)->delete();
                if (!$request->is_admin && $request->permission) {
                    foreach ($request->permission as $permission) {
                        $perm = new UserPermission();
                        $perm->user_id = $request->id;
                        $perm->permission_id = $permission;
                        $perm->save();
                    }
                    $permissions =  UserPermission::where('user_id', $request->id)->get();
                    Cache::forget('user'.$request->id);
                    Cache::put('user'.$user->id,$permissions,60*24*30);
                }
            }
			$new_password      = $request->get('password_confirmation');
            if ($new_password) {
					User::where('id', $request->id)->update([
						'password' => Hash::make($new_password)
					]);
			}
            $user->status = $request->status == 'on' ? 1 : 0;
            if ($request->is_admin) {
                UserPermission::where('user_id', $request->id)->delete();
                $user->is_admin = true;
            }
            return $user->save();
        });
        if ($transaction == true) {
            return back()->with('success', 'The user has been updated successfully');
        } else {
            return back()->with('error', 'Something went wrong!');
        }
    }
}
