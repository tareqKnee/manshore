<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class appointments extends Model
{
    protected $guarded = ['id'];

    public function doctors(){
        return $this->belongsTo('App\doctor', 'doctor_id', 'id');
    }

    public function camera(){
        return $this->belongsTo('App\Camera', 'camera_id', 'id');
    }

    public function order(){
        return $this->hasOne('App\order', 'id', 'app_id');
    }
}
