<?php

namespace App\Observers;

use App\order;
use App\OrderLog;
use Cache;

class OrderObserver
{
    /**
     * Handle the order "created" event.
     *
     * @param  \App\order  $order
     * @return void
     */
    public function created(order $order)
    {
        //
    }

    /**
     * Handle the order "updated" event.
     *
     * @param  \App\order  $order
     * @return void
     */
    public function updated(order $order)
    {
        // $permissions = Cache::get('user'.Auth()->user()->id);
        // && !$permissions->contains('permission_id', 7)
        if (!Auth()->user()->is_admin) {
            OrderLog::insert([
                'user_id' => auth()->user()->id, 'order_id' => $order->id, 'created_at' => now(),
                'updated_at' => now(), 'current_status' => $order->current_status
            ]);
        }
    }

    /**
     * Handle the order "deleted" event.
     *
     * @param  \App\order  $order
     * @return void
     */
    public function deleted(order $order)
    {
        //
    }

    /**
     * Handle the order "restored" event.
     *
     * @param  \App\order  $order
     * @return void
     */
    public function restored(order $order)
    {
        //
    }

    /**
     * Handle the order "force deleted" event.
     *
     * @param  \App\order  $order
     * @return void
     */
    public function forceDeleted(order $order)
    {
        //
    }
}
