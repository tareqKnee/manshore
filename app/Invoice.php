<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    public function order(){
        return $this->belongsTo('App\order', 'order_id', 'id')->withTrashed();
    }

    public function doctor(){
        return $this->belongsTo('App\doctor', 'doctor_id', 'id');
    }
}
