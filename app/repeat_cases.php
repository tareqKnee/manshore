<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class repeat_cases extends Model
{
    public function order(){
        return $this->belongsTo('App\order', 'order_id', 'id');
    }

    public function repeater(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
