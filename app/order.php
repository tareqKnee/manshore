<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class order extends Model
{
    use SoftDeletes;
    protected $guarded = ['id'];

    public function Logs(){
        return $this->hasMany('App\OrderLog', 'order_id', 'id');
    }

    public function Jobs()
    {
        return $this->hasMany('App\Job', 'order_id', 'id');
    }

    public function doctors()
    {
        return $this->belongsTo('App\doctor', 'doctor_id', 'id');
    }

    public function lab()
    {
        return $this->belongsTo('App\lab', 'lab_id', 'id');
    }

    public function made()
    {
        return $this->belongsTo('App\User', 'made_by', 'id');
    }

    public function CreatedBy(){
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function invoice()
    {
        return $this->hasOne('App\Invoice', 'order_id', 'id');
    }

    public function services()
    {
        return $this->belongsTo('App\services', 'service_id', 'id');
    }

    public function getDeliverDateAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d\TH:i');
    }
}
