<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentLog extends Model
{
    public function doctors(){
        return $this->belongsTo('App\doctor', 'doctor_id', 'id');
    }

    public function users(){
        return $this->belongsTo('App\User', 'collector', 'id');
    }
}
