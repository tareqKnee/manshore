<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class doctor extends Model
{
    public function PaymentLog(){
        return $this->hasMany('App\PaymentLog', 'doctor_id', 'id');
    }

    public function invoice(){
        return $this->hasMany('App\PaymentLog', 'doctor_id', 'id');
    }

    public function discounts(){
        return $this->hasMany('App\discount', 'doctor_id', 'id');
    }

    public function order(){
        return $this->hasMany('App\order', 'doctor_id', 'id');
    }
}
