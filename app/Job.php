<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    public function material(){
        return $this->belongsTo('App\material', 'material_id', 'id')->withTrashed();
    }

    public function order(){
        return $this->belongsTo('App\order', 'order_id', 'id')->withTrashed();
    }

    public function types(){
        return $this->belongsTo('App\JobType', 'type', 'id')->withTrashed();
    }
}
