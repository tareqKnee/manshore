<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class feedback extends Model
{
    public function doctor(){
        return $this->belongsTo('App\doctor', 'doctor_id', 'id');
    }

    public function order(){
        return $this->belongsTo('App\order', 'order_id', 'id');
    }
}
