@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Feedback</div>

                <div class="card-body">
                        <div class="mb-5 text-center">
                            <img src="/uploads/login-logo.png" class="w-25">
                        </div>
                        <div class="mb-5 text-center">
                            <h2 class="text-center mt-5">شكرا لكم</h2>
                <h4 class="text-center mt-3">تقييمكم و ملاحظتكم تهمنا و تساعدا على ضبط الجودة و تحسين الأداء</h4>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
