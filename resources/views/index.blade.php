@extends('layout/layout')

@section('head')
@if(Auth()->user()->is_admin)
<title>Admin Dashboard</title>
@else
<title>Dashboard</title>
@endif
@endsection

@section('body-header')
	<h3 class="kt-subheader__title">Admin Dashboard</h3>
	<span class="kt-subheader__separator kt-subheader__separator--v"></span>
	<div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
		<input type="text" class="form-control" placeholder="Search order..." id="generalSearch">
		<span class="kt-input-icon__icon kt-input-icon__icon--right">
			<span><i class="flaticon2-search-1"></i></span>
		</span>
    </div>
    <style>
        .active-tab {
            box-shadow: 1px solid black;box-shadow: 0px 6px 5px 0px rgba(0, 0, 0, 0.21);flex: 0;
            color: royalblue !important;
        }
        .outside {
            border-style: outset;
            color: white;
  text-decoration: none;
        }
        .fc-icon {
    font-family: "LineAwesome" !important;
}
        .v-divider{
 margin-left: auto;
 margin-right: auto;
 width:10px;
 height:80%;
 border-left:1px solid gray;
}
.events-container {
    overflow-y: scroll;
    height: 100%;
    float: right;
    margin: 0px auto;
    font: 13px Helvetica, Arial, sans-serif;
    display: inline-block;
    padding: 0 10px;
    border-bottom-right-radius: 3px;
    border-top-right-radius: 3px;
}
.events-container:after{
    clear:both;
}
.event-card {
    padding: 20px 0;
    width: 350px;
    margin: 20px auto;
    display: block;
    background: #fff;
    border-left: 10px solid #52A0FD;
    border-radius: 3px;
    box-shadow: 3px 8px 16px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);
    -moz-box-shadow: 3px 8px 16px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);
    -webkit-box-shadow: 3px 8px 16px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);
}
.event-count, .event-name, .event-cancelled {
    display: inline;
    padding: 0 10px;
    font-size: 1rem;
}
.event-count {
    color: #52A0FD;
    text-align: right;
}
.event-name {
    padding-right:0;
    text-align: left;
}
.event-cancelled {
    color: #FF1744;
    text-align: right;
}

/*  Calendar wrapper */
.calendar-container  {
    float: left;
    position: relative;
    margin: 0px auto;
    height: 100%;
    background: #fff;
    font: 13px Helvetica, Arial, san-serif;
    display: inline-block;
    border-bottom-left-radius: 3px;
    border-top-left-radius: 3px;
}
.calendar-container:after{
    clear:both;
}
.calendar {
    display: table;
}

/* Calendar Header */
.year-header {
    background: #52A0FD;
    background: -moz-linear-gradient(left,  #52A0FD 0%, #00C9FB 80%, #00C9FB 100%);
    background: -webkit-linear-gradient(left,  #52A0FD 0%, #00C9FB 80%, #00C9FB 100%);
    background: linear-gradient(to right,  #52A0FD 0%, #00C9FB 80%, #00C9FB 100%);
    font-family: Helvetica;
    box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
    -moz-box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
    -webkit-box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
    height: 40px;
    text-align: center;
    position: relative;
    color:#fff;
    border-top-left-radius: 3px;
}
.year-header span {
    display:inline-block;
    font-size: 20px;
    line-height:40px;
}
.left-button, .right-button {
    cursor: pointer;
    width:28px;
    text-align:center;
    position:absolute;
}
.left-button {
    left:0;
    -webkit-border-top-left-radius: 5px;
    -moz-border-radius-topleft: 5px;
    border-top-left-radius: 5px;
}
.right-button {
    right:0;
    top:0;
    -webkit-border-top-right-radius: 5px;
    -moz-border-radius-topright: 5px;
    border-top-right-radius: 5px;
}
.left-button:hover {
    background: #3FADFF;
}
.right-button:hover {
    background: #00C1FF;
}

/* Buttons */
.button{
    cursor: pointer;
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    outline: none;
    font-size: 1rem;
    border-radius: 25px;
    padding: 0.65rem 1.9rem;
    transition: .2s ease all;
    color: white;
    border: none;
    box-shadow: -1px 10px 20px #9BC6FD;
    background: #52A0FD;
    background: -moz-linear-gradient(left,  #52A0FD 0%, #00C9FB 80%, #00C9FB 100%);
    background: -webkit-linear-gradient(left,  #52A0FD 0%, #00C9FB 80%, #00C9FB 100%);
    background: linear-gradient(to right,  #52A0FD 0%, #00C9FB 80%, #00C9FB 100%);
}
#cancel-button {
    box-shadow: -1px 10px 20px #FF7DAE;
    background: #FF1744;
    background: -moz-linear-gradient(left,  #FF1744 0%, #FF5D95 80%, #FF5D95 100%);
    background: -webkit-linear-gradient(left,  #FF1744 0%, #FF5D95 80%, #FF5D95 100%);
    background: linear-gradient(to right,  #FF1744 0%, #FF5D95 80%, #FF5D95 100%);
}
#add-button {
    display: block;
    position: absolute;
    right:20px;
    bottom: 20px;
}
#add-button:hover, #ok-button:hover, #cancel-button:hover {
    transform: scale(1.03);
}
#add-button:active, #ok-button:active, #cancel-button:active {
    transform: translateY(3px) scale(.97);
}

/* Days/months tables */
.days-table, .dates-table, .months-table {
    border-collapse:separate;
    text-align: center;
}
.day {
    height: 26px;
    width: 26px;
    padding: 0 10px;
    line-height: 26px;
    border: 2px solid transparent;
    text-transform:uppercase;
    font-size:90%;
    color:#9e9e9e;
}
.month {
    cursor: default;
    height: 26px;
    width: 26px;
    padding: 0 2px;
    padding-top:10px;
    line-height: 26px;
    text-transform:uppercase;
    font-size: 11px;
    color:#9e9e9e;
    transition: all 250ms;
}
.active-month {
    font-weight: bold;
    font-size: 14px;
    color: #FF1744;
    text-shadow: 0 1px 4px RGBA(255, 50, 120, .8);
}
.month:hover {
    color: #FF1744;
    text-shadow: 0 1px 4px RGBA(255, 50, 120, .8);
}

/*  Dates table */
.table-date {
    cursor: default;
    color:#2b2b2b;
    height:26px;
    width: 26px;
    font-size: 15px;
    padding: 10px;
    line-height:26px;
    text-align:center;
    border-radius: 50%;
    border: 2px solid transparent;
    transition: all 250ms;
}
.table-date:not(.nil):hover {
    border-color: #FF1744;
    box-shadow: 0 2px 6px RGBA(255, 50, 120, .9);
}
.event-date {
    border-color:#52A0FD;
    box-shadow: 0 2px 8px RGBA(130, 180, 255, .9);
}
.active-date{
    background: #FF1744;
    box-shadow: 0 2px 8px RGBA(255, 50, 120, .9);
    color: #fff;
}
.event-date.active-date {
    background: #52A0FD;
    box-shadow: 0 2px 8px RGBA(130, 180, 255, .9);
}

/* input dialog */
.dialog{
    z-index: 5;
    background: #fff;
    position:absolute;
    width:415px;
    height: 500px;
    left:387px;
    border-top-right-radius:3px;
    border-bottom-right-radius: 3px;
    display:none;
    border-left: 1px #aaa solid;
}
.dialog-header {
    margin: 20px;
    color:#333;
    text-align: center;
}
.form-container {
    margin-top:25%;
}
.form-label {
    color:#333;
}
.input {
    border:none;
    background: none;
    border-bottom: 1px #aaa solid;
    display:block;
    margin-bottom:50px;
    width: 200px;
    height: 20px;
    text-align: center;
    transition: border-color 250ms;
}
.input:focus {
    outline:none;
    border-color: #00C9FB;
}
.error-input {
    border-color: #FF1744;
}

/* Tablets and smaller */
@media only screen and (max-width: 780px) {
    .content {
        overflow: visible;
        position:relative;
        max-width: 100%;
        width: 370px;
        height: 100%;
        background: #52A0FD;
        background: -moz-linear-gradient(left,  #52A0FD 0%, #00C9FB 80%, #00C9FB 100%);
        background: -webkit-linear-gradient(left,  #52A0FD 0%, #00C9FB 80%, #00C9FB 100%);
        background: linear-gradient(to right,  #52A0FD 0%, #00C9FB 80%, #00C9FB 100%);
    }
    .dialog {
        width:370px;
        height: 450px;
        border-radius: 3px;
        top:0;
        left:0;
    }
    .events-container {
        float:none;
        overflow: visible;
        margin: 0 auto;
        padding: 0;
        display: block;
        left: 0;
        border-radius: 3px;
    }

    .calendar-container {
        float: none;
        padding: 0;
        margin: 0 auto;
        margin-right: 0;
        display: block;
        left: 0;
        border-radius: 3px;
        box-shadow: 3px 8px 16px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);
        -moz-box-shadow: 3px 8px 16px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);
        -webkit-box-shadow: 3px 8px 16px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);
    }
}

/* Small phone screens */
@media only screen and (max-width: 400px) {
    .content, .events-container, .year-header, .calendar-container {
        width: 320px;
    }
    .dialog {
        width: 320px;
    }
    .months-table {
        display: block;
        margin: 0 auto;
        width: 320px;
    }
    .event-card {
        width: 300px;
    }
    .day {
        padding: 0 7px;
    }
    .month {
        display: inline-block;
        padding: 10px 10px;
        font-size: .8rem;
    }
    .table-date {
        width: 20px;
        height: 20px;
        line-height: 20px;
    }
    .event-name, .event-count, .event-cancelled {
        font-size: .8rem;
    }
    .add-button{
        bottom: 10px;
        right: 10px;
        padding: 0.5rem 1.5rem;
    }
}
        </style>
@endsection
@section('body')
    <div class="row w-100 bg-white">
        <div class="col-md-3 mb-2">
            <a href="/home/design">
                <div class="{{Request::segment(2) == 'design' || Request::segment(2) == null ? 'active-tab' : 'outside'}}" style="padding: 12px;border: 1px solid #e0e0e0;border-top:0">
                    <span style="display:inline-block;font-size: 18px;color: #888;font-weight: 300; color: #777">Design</span>
                    <span style="display:inline-block;float: right;font-size: 21px;color: #555;">{{ $group->where('current_status', 0)->first() ?
                            $group->where('current_status', 0)->first()->total : '0' }}</span>
                    <span style="display:block; color: #999">Total tasks</span>
                </div>
            </a>
            <a href="/home/milling">
                <div class="{{Request::segment(2) == 'milling' ? 'active-tab' : 'outside'}}" style="padding: 12px;border: 1px solid #e0e0e0;border-top:0">
                    <span style="display:inline-block;font-size: 18px;color: #888;font-weight: 300; color: #777">Milling</span>
                    <span style="display:inline-block;float: right;font-size: 21px;color: #555;">{{ $group->where('current_status', 1)->first() ?
                            $group->where('current_status', 1)->first()->total : '0' }}</span>
                    <span style="display:block; color: #999">Total tasks</span>
                </div>
            </a>
            <a href="/home/furnace">
                <div class="{{Request::segment(2) == 'furnace' ? 'active-tab' : 'outside'}}" style="padding: 12px;border: 1px solid #e0e0e0;border-top:0">
                    <span style="display:inline-block;font-size: 18px;color: #888;font-weight: 300; color: #777">Sintering furnace</span>
                    <span style="display:inline-block;float: right;font-size: 21px;color: #555;">{{ $group->where('current_status', 2)->first() ?
                    $group->where('current_status', 2)->first()->total : '0'}}</span>
                    <span style="display:block; color: #999">Total tasks</span>
                </div>
            </a>
            <a href="/home/finish">
                <div class="{{Request::segment(2) == 'finish' ? 'active-tab' : 'outside'}}" style="padding: 12px;border: 1px solid #e0e0e0;border-top:0">
                    <span style="display:inline-block;font-size: 18px;color: #888;font-weight: 300; color: #777">Finishing and build up</span>
                    <span style="display:inline-block;float: right;font-size: 21px;color: #555;">{{  $group->where('current_status', 3)->first() ?
                            $group->where('current_status', 3)->first()->total : '0' }}</span>
                    <span style="display:block; color: #999">Total tasks</span>
                </div>
            </a>
            <a href="/home/quality">
                <div class="{{Request::segment(2) == 'quality' ? 'active-tab' : 'outside'}}" style="padding: 12px;border: 1px solid #e0e0e0;border-top:0">
                    <span style="display:inline-block;font-size: 18px;color: #888;font-weight: 300; color: #777">Quality control</span>
                    <span style="display:inline-block;float: right;font-size: 21px;color: #555;">{{ $group->where('current_status', 4)->first() ?
                            $group->where('current_status', 4)->first()->total : '0' }}</span>
                    <span style="display:block; color: #999">Total tasks</span>
                </div>
            </a>
            <a href="/home/delivery">
                <div class="{{Request::segment(2) == 'delivery' ? 'active-tab' : 'outside'}}" style="padding: 12px;border: 1px solid #e0e0e0;border-top:0">
                    <span style="display:inline-block;font-size: 18px;color: #888;font-weight: 300; color: #777">Delivery</span>
                    <span style="display:inline-block;float: right;font-size: 21px;color: #555;">{{ $group->where('current_status', 5)->first() ?
                            $group->where('current_status', 5)->first()->total : '0' }}</span>
                    <span style="display:block; color: #999">Total tasks</span>
                </div>
            </a>
        </div>
        <div class="col-md-5 mb-2 pt-4 mb-3">
            <h4>Weekly Tasks</h4>
            <p class="mb-4">Check out each column for more details</p>
            <canvas class="barChart" id="myChart" style="width: 50%; height: 180px"></canvas>
        </div>
        <div class="col-md-4 mb-2 pt-4 mb-3">
            <h4>Chart</h4>
            <p class="mb-4">Chart between tasks per week</p>
            <canvas id="doughnutChart" style="width: 50%; height: 180px"></canvas>
        </div>
    </div>
    <br>
        <div class="container vertical-divider" style="margin-top: 30px; margin-right: 0;margin-left: 0; background-color:white; max-width: 100%; overflow: auto;">
            <br>
            <h4 style="text-transform: capitalize; color:#434349"> {{Request::segment(2) != null ? Request::segment(2) : 'Design'}} tasks </h4>
                <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" id="kt_apps_user_list_datatable" style="">
                        <table class="kt-datatable__table">
                            <thead class="kt-datatable__head">
                                <tr class="kt-datatable__row" >
                                    <th class="kt-datatable__cell"><span style="width: 200px; margin: auto; text-align: center">Orders ID</span></th>
                                    <th class="kt-datatable__cell"><span style="width: 150px; margin: auto; text-align: center">Assigned to</span></th>
                                    <th class="kt-datatable__cell"><span style="width: 180px; margin: auto; text-align: center">Doctor Name</span></th>
                                    <th class="kt-datatable__cell"><span style="width: 150px; margin: auto; text-align: center">Patient Name</span></th>
                                    <th class="kt-datatable__cell"><span style="width: 120px; margin: auto; text-align: center">Date created</span></th>
                                    <th class="kt-datatable__cell"><span style="width: 120px; margin: auto; text-align: center">State</span></th>
                                    <th class="kt-datatable__cell"><span style="width: 80px; margin: auto; text-align: center">Actions</span></th>
                                </tr>
                            </thead>
                            <tbody style="" class="kt-datatable__body">
                                @if(Request::segment(2) == 'design' || Request::segment(2) == null)
                                @php
                                    $orders = $design;
                                @endphp
                                @elseif(Request::segment(2) == 'milling')
                                @php
                                $orders = $milling;
                                @endphp
                                @elseif(Request::segment(2) == 'furnace')
                                @php
                                $orders = $furnace;
                            @endphp
                                @elseif(Request::segment(2) == 'finish')
                                @php
                                $orders = $finish;
                            @endphp
                                @elseif(Request::segment(2) == 'quality')
                                @php
                                $orders = $quality;
                            @endphp
                                @else
                                @php
                                $orders = $delivery;
                            @endphp
                                @endif
                                @foreach($orders as $order)
                                @php
                                if($order->made_by == 0){
                                    $status = "Waiting";
                                }else{
                                    $status = "Active";
                                }
                                @endphp
                                <tr data-row="{{$order->id}}" class="kt-datatable__row" >
                                    <td class="kt-datatable__cell--sorted kt-datatable__cell"><span style="width: 200px; margin: auto; text-align: center;">
                                         {{$order->order_id}}
                                        </span>
                                    </td>
                                    <th class="kt-datatable__cell"><span style="width: 150px; margin: auto; text-align: center">{{$order->made_by ? $order->made->first_name . ' ' . $order->made->last_name : 'Unassigned'}}</span></th>
                                    <td class="kt-datatable__cell"><span style="width: 180px; margin: auto; text-align: center">{{$order->doctors->name}}</span></td>
                                    <td class="kt-datatable__cell"><span style="width: 150px; margin: auto; text-align: center">{{$order->patient_name}}</span></td>
                                    <td class="kt-datatable__cell" style=""><span style="width: 120px; margin: auto; text-align: center">{{$order->created_at->format('d/m/Y')}}</span></td>
                                    <td class="kt-datatable__cell">
                                        @if($order->current_status == 6)
                                        <span style="width: 120px; margin: auto; text-align: center" class="badge badge-success">Completed</span>
                                        @elseif($status == "Active")
                                        <span style="width: 120px; margin: auto; text-align: center" class="badge badge-primary">{{$status}}</span>
                                        @else
                                        <span style="width: 120px; margin: auto; text-align: center" class="badge badge-danger">{{$status}}</span>
                                        @endif</td>
                                    <td data-field="Actions" data-autohide-disabled="false" class="kt-datatable__cell"><span style="overflow: visible; position: relative; width: 80px; margin: auto; text-align: center">
                                            <div class="dropdown"> <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md"> <i class="flaticon-more-1"></i> </a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <ul class="kt-nav">
                                                        <li class="kt-nav__item"> <a class="kt-nav__link" href="{{route('orderEdit', $order->id)}}"> <i class="kt-nav__link-icon flaticon2-contract"></i> <span class="kt-nav__link-text">Edit</span> </a> </li>
                                                        @if($order->current_status != 6)
                                                        <li class="kt-nav__item" data-toggle="modal" data-target="#myModal{{$order->id}}"> <a class="kt-nav__link"><i class="kt-nav__link-icon flaticon2-contract"></i> <span class="kt-nav__link-text">Repeat this task</span> </a> </li>
                                                        @endif
                                                    </ul>
                                                </div>
                                            </div>
                                        </span></td>
                                </tr>
                                @if($order->current_status != 6)
                                <div class="modal" tabindex="-1" role="dialog" id="myModal{{$order->id}}">
                                        <form action="{{route('qreset')}}" method="POST">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$order->id}}">
                                            <div class="modal-dialog" role="document">
                                              <div class="modal-content">
                                                <div class="modal-header">
                                                  <h5 class="modal-title">Repeat Case</h5>
                                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                  </button>
                                                </div>
                                                <div class="modal-body">
                                                        <strong style="color: black"><h6>Fill the fields below to be able to repeat the case</h6></strong>
                                                        <div class="form-group">
                                                                <div class="form-group">
                                                                        <label for="milled">Required modifications: </label>
                                                                        <div class="form-group form-group-last">
                                                                                <textarea class="form-control" name="note" id="exampleTextarea" rows="3">{{old('note')}}</textarea>
                                                                            </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label for="status">Choose Phase: </label>
                                                                        <select class="form-control" name="status">
                                                                            <option value="scan" {{old('status') == 'scan' ? 'selected' : ''}}>Scan</option>
                                                                            <option value="0" {{old('status') == 0 ? 'selected' : ''}}>Design</option>
                                                                                <option value="1" {{old('status') == 1 ? 'selected' : ''}}>Milling</option>
                                                                                <option value="2" {{old('status') == 2 ? 'selected' : ''}}>Sintering furnace</option>
                                                                                <option value="3" {{old('status') == 3 ? 'selected' : ''}}>Finishing and build up</option>
                                                                                <option selected hidden disabled>Select the phase</option>
                                                                            </select>
                                                                    </div>
                                                        </div>
                                                </div>
                                                <div class="modal-footer">
                                                  <button type="submit" class="btn btn-primary">Save changes</button>
                                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                </div>
                                              </div>
                                            </div>
                                        </form>
                                          </div>
                                          @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="kt-portlet">
                            <div class="kt-portlet__body">
                                <div class="kt-pagination kt-pagination--brand">
                                    {{$orders->links()}}
                                </div>
                            </div>
                        </div>
        </div>

        <div class="container vertical-divider" style="padding-top: 10px;margin-top: 30px; margin-right: 0;margin-left: 0; background-color:white; max-width: 100%;">
            <span style="float: right;">
                <div class="dropdown"> <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md"> <i class="flaticon-more-1"></i> </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <ul class="kt-nav">
                            <li class="kt-nav__item"> <a class="kt-nav__link" href="{{route('home', ['type' => Request::segment(2) ? Request::segment(2) : "design", 'camera' => 'all'])}}">All cameras
                            </a>
                        </li>
                            @foreach($cameras as $cam)
                            <li class="kt-nav__item {{ Request::segment(3) == $cam->id ? 'active' : ''}}"> <a class="kt-nav__link" href="{{route('home', ['type' => Request::segment(2) ? Request::segment(2) : "design", 'camera' => $cam->id])}}"> {{$cam->name}}
                            </a>
                        </li>
                        @endforeach
                        </ul>
                    </div>
                </div>
            </span>
            <div id='calendar'></div>
              </div>
        @php
            $i = 0;
            $j = 7
        @endphp
        @foreach($dates as $key => $date)
        <input type="hidden"  class="hidden" value="{{$key}}" id="{{$i}}">
        <input type="hidden" class="hidden" value="{{$date}}" id="{{$j}}">
        @php
            $i++;
            $j++;
        @endphp
        @endforeach
        <div class="modal" tabindex="-1" role="dialog" id="Appointment">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title">Appointment information</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                             <label for="time">Time: </label>
                                <input type="text" class="form-control" id="time" disabled>
                                <label for="time">Doctor: </label>
                                <input type="text" class="form-control" id="doctor" disabled>
                                <label for="time">Camera: </label>
                                <input type="text" class="form-control" id="camera" disabled>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                      </div>
                    </div>
                  </div>
@endsection

@section('scripts')
<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.js'></script>
<script>
    $(document).ready(function() {
        @foreach($appointments as $app)
            getTimePeriod('{{$app->time}}');
        @endforeach
        // page is now ready, initialize the calendar...
        $('#calendar').fullCalendar({
            header: {
				center: 'agendaDay,agendaWeek,month',
                right: 'prev,next today'
			},
            defaultView: 'agendaWeek',
            scrollTime: '09:00:00',
            // put your options and callbacks here
            events : [
                @foreach($appointments as $app)
                {
                    title : '{{$app->doctors->name}} - {{ $app->camera->name }}',
                    start : '{{ $app->date }}' + 'T' + getTimePeriod('{{$app->time}}')[0],
                    camera : '{{ $app->camera->name }}',
                    doctor : '{{ $app->doctors->name }}',
                    time : '{{ $app->time }}',
                    end : '{{ $app->date }}' + 'T' + getTimePeriod('{{$app->time}}')[1],
                    color : getBackgroundColor('{{ $app->status }}'),
                    borderColor : getBorderColor('{{ $app->status }}'),
                },
                @endforeach
            ],
            eventClick: function(info) {
                $('#time').val(info.time);
                $('#doctor').val(info.doctor);
                $('#camera').val(info.camera);

                $('#Appointment').modal('show');
            }
        })
        function getBackgroundColor(status) {
            if(status == 0) {
                return '#f9abad';
            }
            else if(status == 1) {
                return '#b8c9ff';
            }
            else if(status == 2) {
                return '#caffca';
            }
            else {
                return '#fff';
            }
        }
        function getBorderColor(status) {
            if(status == 0) {
                return '#f7464a';
            }
            else if(status == 1) {
                return '#5578eb';
            }
            else if(status == 2) {
                return '#008000';
            }
            else {
                return '#fff';
            }
        }
        function getTimePeriod(time) {
            finalTime = [];
            newTime = time.replace(' - ',' ');
            newTime = newTime.split(' ');
            newTime.forEach(element => {
                if(element.includes('AM')) {
                    element = element.replace('AM','');
                    element = parseInt(element);
                    if(element < 10){
                        element = '0' + element;
                    }
                    if(element == 12){
                        element = '00';
                    }
                    element = element+':00:00';
                }
                else if(element.includes('PM')){
                    element = element.replace('PM','');
                    element = parseInt(element);
                    if(element < 12){
                        element = element + 12;
                    }
                    element = element + ':00:00';
                }
                finalTime.push(element);
            });
            return finalTime;
        }
    });


</script>
<script>

var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [$('#0').val(), $('#1').val(), $('#2').val(), $('#3').val(), $('#4').val(), $('#5').val(), $('#6').val()],
        datasets: [{
            label: 'Cases',
            data: [$('#7').val(), $('#8').val(), $('#9').val(), $('#10').val(), $('#11').val(), $('#12').val(), $('#13').val()],
            backgroundColor: [
                'rgb(110, 79, 245)',
                'rgb(110, 79, 245)',
                'rgb(110, 79, 245)',
                'rgb(110, 79, 245)',
                'rgb(110, 79, 245)',
                'rgb(110, 79, 245)',
                'rgb(110, 79, 245)'
            ],
            borderWidth: 2
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
var ctxD = document.getElementById("doughnutChart").getContext('2d');
var myLineChart = new Chart(ctxD, {
type: 'doughnut',
data: {
labels: ["Waiting", "Completed", "Active"],
datasets: [{
data: ["{{isset($graph2[0]->total) ? ($graph2[0]->total - $count_made < 0 ? 0 : $graph2[0]->total - $count_made) : 0}}", "{{isset($graph2[0]->total) ? $graph2[0]->total : 0}}", "{{$count_made}}"],
backgroundColor: ["#F7464A", "#008000", "#5578eb"],
hoverBackgroundColor: ["#FF5A5E", "#7FFF00", "#00FFFF"]
}]
},
options: {
responsive: true
}
});
</script>
@endsection
