@extends('layout/layout')

@section('head')
<title>View Job types</title>
@endsection

@section('body-header')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Job Types
            </h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__group" id="kt_subheader_search">
                <span class="kt-subheader__desc" id="kt_subheader_total">
                    {{$types->total()}} Total </span>
            </div>
        </div>
    </div>
</div>
@endsection

@section('body')
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="text-right mb-3">
        <a class="btn btn-brand btn-elevate" href="{{route('TypesCreate')}}"><i class="fa fa-plus mx-2"></i>Add a Job types</a>
    </div>
    @if(session('success'))
    <div class="alert alert-success" role="alert">
        <div class="alert-icon"><i class="flaticon-success"></i></div>
        <div class="alert-text">{{session("success")}}</div>
    </div>
    @endif
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body kt-portlet__body--fit">
            <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" id="kt_apps_user_list_datatable" style="">
                <table class="kt-datatable__table" style="display: block;">
                    <thead class="kt-datatable__head">
                        <tr class="kt-datatable__row" style="left: 0px;">
                            <th class="kt-datatable__cell"><span style="width: 200px;">Type ID</span></th>
                            <th class="kt-datatable__cell"><span style="width: 150px;">Type Name</span></th>
                            <th class="kt-datatable__cell"><span class="middle" style="width: 80px;">Actions</span></th>
                        </tr>
                    </thead>
                    <tbody style="" class="kt-datatable__body">
                        @foreach($types as $key=>$type)
                        <tr data-row="{{$type->id}}" class="kt-datatable__row" style="left: 0px;">
                            <td class="kt-datatable__cell--sorted kt-datatable__cell" data-field="AgentName"><span style="width: 200px;">
                                    <div class="kt-user-card-v2">
                                        <div class="kt-user-card-v2__details"> <span class="kt-user-card-v2__name" href="#">{{$type->id}}</span></div>
                                    </div>
                                </span>
                            </td>
                            <td data-field="Country" class="kt-datatable__cell"><span style="width: 150px;">{{$type->name}}</span></td>
                            <td data-field="Actions" data-autohide-disabled="false" class="kt-datatable__cell"><span style="overflow: visible; position: relative; width: 80px; margin: auto; text-align: center">
                                    <div class="dropdown"> <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md"> <i class="flaticon-more-1"></i> </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <ul class="kt-nav">
                                                <li class="kt-nav__item"> <a class="kt-nav__link" href="{{route('TypesEdit', $type->id)}}"> <i class="kt-nav__link-icon flaticon2-contract"></i> <span class="kt-nav__link-text">Edit</span> </a> </li>
                                                <li class="kt-nav__item"> <a class="kt-nav__link" href="{{route('TypesDelete', $type->id)}}"> <i class="kt-nav__link-icon flaticon2-contract"></i> <span class="kt-nav__link-text">Delete</span> </a> </li>
                                            </ul>
                                        </div>
                                    </div>
                                </span></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="kt-portlet">
        <div class="kt-portlet__body">
            <div class="kt-pagination kt-pagination--brand">
                {{$types->links()}}
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    function sweetalert() {
        swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {

            if (result.value) {
                swalWithBootstrapButtons.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                )
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                swalWithBootstrapButtons.fire(
                    'Cancelled',
                    'Your imaginary file is safe :)',
                    'error'
                )
            }
        })
    }
</script>
@endsection
