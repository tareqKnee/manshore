@if ($paginator->hasPages())
<ul class="kt-pagination__links">
    {{-- Previous Page Link --}}
    @if (!$paginator->onFirstPage())

    <li class="kt-pagination__link--first">
        <a href="?page=1"><i class="fa fa-angle-double-left kt-font-brand"></i></a>
    </li>
    <li class="kt-pagination__link--next">
        <a href="{{ $paginator->previousPageUrl() }}"><i class="fa fa-angle-left kt-font-brand"></i></a>
    </li>
    @endif

    {{-- Pagination Elements --}}
    @foreach ($elements as $element)
    {{-- "Three Dots" Separator --}}
    @if (is_string($element))
    <li class="page-item disabled"><span class="page-link">{{ $element }}</span></li>
    @endif

    {{-- Array Of Links --}}
    @if (is_array($element))
    @foreach ($element as $page => $url)
    @if ($page == $paginator->currentPage())
    <li class="kt-pagination__link--active">
        <a href="#">
            {{ $page }}
        </a>
    </li>
    @else
    <li><a href="{{ $url }}">{{ $page }}</a></li>
    @endif
    @endforeach
    @endif
    @endforeach

    {{-- Next Page Link --}}
    @if ($paginator->hasMorePages())
    <li class="kt-pagination__link--prev">
        <a href="{{ $paginator->nextPageUrl() }}"><i class="fa fa-angle-right kt-font-brand"></i></a>
    </li>
    <li class="kt-pagination__link--last">
        <a href="?page={{$paginator->lastPage()}}"><i class="fa fa-angle-double-right kt-font-brand"></i></a>
    </li>
    @else
    <li class="kt-pagination__link--prev" style="display: none">
        <a href="#"><i class="fa fa-angle-right kt-font-brand"></i></a>
    </li>
    <li class="kt-pagination__link--last" style="display: none">
        <a href="#"><i class="fa fa-angle-double-right kt-font-brand"></i></a>
    </li>
    @endif
</ul>
@endif