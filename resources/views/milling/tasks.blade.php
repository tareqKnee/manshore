@extends('layout/layout')

@section('head')
<title>View Tasks</title>
@endsection

@section('body-header')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                My tasks
            </h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
        </div>
    </div>
</div>
@endsection

@section('body')
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body kt-portlet__body--fit">
            <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" id="kt_apps_user_list_datatable" style="">
                <table class="kt-datatable__table" style="display: block;">
                    <thead class="kt-datatable__head">
                        <tr class="kt-datatable__row" style="left: 0px;">
                            <th class="kt-datatable__cell"><span style="width: 200px;">Orders ID</span></th>
                            <th class="kt-datatable__cell"><span style="width: 150px;">Patient Name</span></th>
                            <th class="kt-datatable__cell"><span style="width: 180px;">Doctor Name</span></th>
                            <th class="kt-datatable__cell"><span style="width: 120px;">Date created</span></th>
                            <th class="kt-datatable__cell"><span style="width: 120px;">State</span></th>
                            <th class="kt-datatable__cell"><span style="width: 80px;">Actions</span></th>
                        </tr>
                    </thead>
                    <tbody style="" class="kt-datatable__body">
                        @foreach($tasks as $key=>$order)
                        @php
                        if($order->made_by == 0){
                            $status = "Waiting";
                        }else{
                            $status = "Active";
                        }
                        @endphp
                        <tr data-row="{{$order->id}}" class="kt-datatable__row" style="left: 0px;">
                            <td class="kt-datatable__cell--sorted kt-datatable__cell" data-field="AgentName"><span style="width: 200px;">
                                    <div class="kt-user-card-v2">
                                        <div class="kt-user-card-v2__details"> <span class="kt-user-card-v2__name" href="#">{{$order->order_id}}</span></div>
                                    </div>
                                </span>
                            </td>
                            <td data-field="Country" class="kt-datatable__cell"><span style="width: 150px;">{{$order->patient_name}}</span></td>
                            <td data-field="Country" class="kt-datatable__cell"><span style="width: 180px;">{{$order->doctors->name}}</span></td>
                            <td class="kt-datatable__cell" style=""><span style="width: 120px;">{{$order->created_at->format('d/m/Y')}}</span></td>
                            <td class="kt-datatable__cell" style="">@if($status == "Active")
                                    <span style="width: 120px;" class="badge badge-primary">{{$status}}</span>
                                    @else
                                    <span style="width: 120px;" class="badge badge-danger">{{$status}}</span>
                                    @endif</td>
                            <td data-field="Actions" data-autohide-disabled="false" class="kt-datatable__cell"><span style="overflow: visible; position: relative; width: 80px;">
                                    <div class="dropdown"> <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md"> <i class="flaticon-more-1"></i> </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <ul class="kt-nav">
                                                    <li class="kt-nav__item"> <a class="kt-nav__link" href="{{route('OrderViewOnly', $order->id)}}"> <i class="kt-nav__link-icon flaticon2-contract"></i> <span class="kt-nav__link-text">View Order</span> </a> </li>
                                                <li class="kt-nav__item"> <a class="kt-nav__link" href="{{route('mMark', $order->id)}}"><i class="kt-nav__link-icon flaticon2-contract"></i> <span class="kt-nav__link-text">Mark as done</span> </a> </li>
                                                <li class="kt-nav__item" data-toggle="modal" data-target="#myModal{{$order->id}}"> <a class="kt-nav__link"><i class="kt-nav__link-icon flaticon2-contract"></i> <span class="kt-nav__link-text">Edit milling options</span> </a> </li>
                                            </ul>
                                        </div>
                                    </div>
                                </span></td>
                        </tr>
                        <div class="modal" tabindex="-1" role="dialog" id="myModal{{$order->id}}">
                                <form action="{{route('AddMiller')}}" method="POST">
                                    @csrf
                                    <input type="hidden" name="id" value="{{$order->id}}">
                                    <div class="modal-dialog" role="document">
                                      <div class="modal-content">
                                        <div class="modal-header">
                                          <h5 class="modal-title">Order milling information</h5>
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                          </button>
                                        </div>
                                        <div class="modal-body">
                                                <div class="form-group row">
                                                        <div class="form-group col-6">
                                                                <label for="milled">Milled: </label>
                                                                <select class="form-control milled" id="milled" name="milled">
                                                                    <option selected hidden disabled>Select your lab</option>
                                                                        <option value="1" {{$order->milled == 1 ? 'selected' : ''}}>Externally</option>
                                                                        <option value="0" {{$order->milled == 0 ? 'selected' : ''}}>Internally</option>
                                                                    </select>
                                                            </div>

                                                            <div class="form-group col-6 milled_by">
                                                                <label for="milled_by">Milled by: </label>
                                                                <input id="milled_by" class="form-control input-group-lg reg_name" type="text" name="milled_by"
                                                                       title="Write the person name"
                                                                       placeholder="Write the person name" value="{{$order->milled_by}}"/>
                                                            </div>
                                                            <div class="form-group col-6 lab_id">
                                                                    <label for="lab_id">Lab name: </label>
                                                                    <select class="form-control" id="lab_id" name="lab_id">
                                                                            <option selected hidden disabled>Select your lab</option>
                                                                            @foreach($labs as $lab)
                                                                            <option value="{{$lab->id}}" {{$order->lab_id == $lab->id ? 'selected' : ''}}>{{$lab->name}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                </div>
                                                </div>
                                        </div>
                                        <div class="modal-footer">
                                          <button type="submit" class="btn btn-primary">Save changes</button>
                                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        </div>
                                      </div>
                                    </div>
                                </form>
                                  </div>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="kt-portlet">
        <div class="kt-portlet__body">
            <div class="kt-pagination kt-pagination--brand">
                {{$tasks->links()}}
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
        $(document).ready(function() {
            if($('.milled').val() == 1){
                $('.milled_by').hide();
            }
            else if($('select.milled').val() == 0) {
                $('.lab_id').hide();
            }
            $('.milled').on('change', function() {
                if($(this).val() == 1){
                    $(this).parent().parent().find('.lab_id').css('display', 'inline-block');
                    $(this).parent().parent().find('.milled_by').hide();
                } else {
                    $(this).parent().parent().find('.lab_id').hide();
                    $(this).parent().parent().find('.milled_by').css('display', 'inline-block');
                }
            });
    });

    function sweetalert() {
        swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, mark it as done!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {

            if (result.value) {
                swalWithBootstrapButtons.fire(
                    'Marked as done!',
                    'Your task has been marked as done.',
                    'success'
                )
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                swalWithBootstrapButtons.fire(
                    'Cancelled',
                    'Your imaginary task is safe :)',
                    'error'
                )
            }
        })
    }
</script>
@endsection
