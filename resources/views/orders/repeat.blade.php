@extends('layout/layout')

@section('head')
<title>View Cases</title>
@endsection

@section('body-header')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Cases
            </h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__group" id="kt_subheader_search">
                <span class="kt-subheader__desc" id="kt_subheader_total">
                    {{$orders->total()}} Total </span>
                <form class="kt-margin-l-20" id="kt_subheader_search_form" method="GET" action="{{route('RepeatOrders')}}">
                        <strong style="color:black"> Sort by: </strong>
                        <div class="kt-subheader__search" style="">
                                    <select name="date" id="date" class="form-control">
                                        <option value="desc" {{$date == 'desc' ? 'selected' : ''}}>Date (Descending)</option>
                                        <option value="asc" {{$date == 'asc' ? 'selected' : ''}}>Date (Ascending)</option>
                                    </select>
                                </div>
                                <strong style="color:black"> Doctor: </strong>
                                <div class="kt-subheader__search" style="">
                                            <select name="doctor" id="doctor" class="form-control">
                                                @foreach($doctors as $doc)
                                                <option value="{{$doc->id}}" {{$doctor == $doc->id ? 'selected' : ''}}>{{$doc->name}}</option>
                                                @endforeach
                                                @if($doctor == null)
                                                <option selected disabled hidden>Choose your doctor</option>
                                                @endif
                                                <option value="all" {{$doctor == "all" ? 'selected' : ''}}>All</option>
                                            </select>
                                        </div>
                    <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                            <input type="text" class="form-control" placeholder="Search..." name="patient" id="generalSearch" value="{{$patient}}">
                            <span class="kt-input-icon__icon kt-input-icon__icon--right">
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="0" y="0" width="24" height="24" />
                                            <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                            <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero" />
                                        </g>
                                    </svg>
                                    <!--<i class="flaticon2-search-1"></i>-->
                                </span>
                            </span>
                        </div>
                        <button type="submit" class="btn btn-primary">Search</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('body')
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" style="width: 200%;">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body kt-portlet__body--fit">
            <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" id="kt_apps_user_list_datatable" style="">
                <table class="kt-datatable__table">
                    <thead class="kt-datatable__head">
                        <tr class="kt-datatable__row" >
                            <th class="kt-datatable__cell"><span style="width: 200px; margin: auto; text-align: center">Cases ID</span></th>
                            <th class="kt-datatable__cell"><span style="width: 150px; margin: auto; text-align: center">Assigned to</span></th>
                            <th class="kt-datatable__cell"><span style="width: 180px; margin: auto; text-align: center">Doctor Name</span></th>
                            <th class="kt-datatable__cell"><span style="width: 150px; margin: auto; text-align: center">Patient Name</span></th>
                            <th class="kt-datatable__cell"><span style="width: 150px; margin: auto; text-align: center">Repeater Name</span></th>
                            <th class="kt-datatable__cell"><span style="width: 120px; margin: auto; text-align: center">Date created</span></th>
                            <th class="kt-datatable__cell"><span style="width: 100px; margin: auto; text-align: center">State</span></th>
                            <th class="kt-datatable__cell"><span style="width: 80px; margin: auto; text-align: center">Actions</span></th>
                        </tr>
                    </thead>
                    <tbody style="" class="kt-datatable__body">
                        @foreach($orders as $key=>$order)
                        @php
                        if($order->order->made_by == 0){
                            $status = "Waiting in";
                        }else{
                            $status = "Active in";
                        }
                        if($order->order->current_status == '0'){
                            $phase = 'Design';
                        } elseif($order->order->current_status == '1'){
                            $phase = 'Milling';
                        } elseif($order->order->current_status == '2'){
                            $phase = 'Furnace';
                        } elseif($order->order->current_status == '3'){
                            $phase = 'Finish and building up';
                        } elseif($order->order->current_status == '4'){
                            $phase = 'Quality assurance';
                        } else {
                            $phase = 'Delivery';
                        }
                        @endphp
                        <tr data-row="{{$order->order->id}}" class="kt-datatable__row" >
                            <td class="kt-datatable__cell--sorted kt-datatable__cell"><span style="width: 200px; text-align: center; margin: auto;">
                                 {{$order->order->order_id}}
                                </span>
                            </td>
                            <th class="kt-datatable__cell"><span style="width: 150px; margin: auto; text-align: center">{{ $order->order->made_by ? (isset($order->order->made->first_name) ? $order->order->made->first_name . ' ' . $order->order->made->last_name : 'Unassigned') : 'Unassigned'}}</span></th>
                            <td class="kt-datatable__cell"><span style="width: 180px; margin: auto; text-align: center">{{$order->order->doctors->name}}</span></td>
                            <td class="kt-datatable__cell"><span style="width: 150px; margin: auto; text-align: center">{{$order->order->patient_name}}</span></td>
                            <td class="kt-datatable__cell"><span style="width: 150px; margin: auto; text-align: center">{{$order->repeater->first_name}} {{$order->repeater->last_name}}</span></td>
                            <td class="kt-datatable__cell" style=""><span style="width: 120px; margin: auto; text-align: center">{{$order->created_at->format('d/m/Y')}}</span></td>
                            <td class="kt-datatable__cell">
                                @if($order->order->current_status == 6)
                                <span style="width: 100px; margin: auto; text-align: center" class="badge badge-success middle">Completed</span>
                                @elseif($status == "Active in" || $status == "Active")
                                <span style="width: 100px; margin: auto; text-align: center" class="badge badge-primary middle">{{$status}} {{$phase}}</span>
                                @else
                                <span style="width: 100px; margin: auto; text-align: center" class="badge badge-danger middle">{{$status}} {{$phase}}</span>
                                @endif</td>
                            <td data-field="Actions" data-autohide-disabled="false" class="kt-datatable__cell"><span style="overflow: visible; position: relative; width: 80px; margin: auto; text-align: center">
                                    <div class="dropdown"> <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md"> <i class="flaticon-more-1"></i> </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <ul class="kt-nav">
                                                @php
                                                $explode = explode('_', $order->order_id);
                                    @endphp
                                    @if(count($explode) && $explode[count($explode) - 1][0] == 'R')
                                    <li class="kt-nav__item"> <a class="kt-nav__link" href="{{route('orderEdit', $order->id)}}"> <i class="kt-nav__link-icon flaticon2-contract"></i> <span class="kt-nav__link-text">Edit</span> </a> </li>
                                    @endif
                                                <li class="kt-nav__item" data-toggle="modal" data-target="#myModal{{$order->order->id}}"> <a class="kt-nav__link"><i class="kt-nav__link-icon flaticon2-contract"></i> <span class="kt-nav__link-text">Repeat this task</span> </a> </li>
                                            </ul>
                                        </div>
                                    </div>
                                </span></td>
                        </tr>
                        <div class="modal" tabindex="-1" role="dialog" id="myModal{{$order->order->id}}">
                                <form action="{{route('qreset')}}" method="POST">
                                    @csrf
                                    <input type="hidden" name="id" value="{{$order->order->id}}">
                                    <div class="modal-dialog" role="document">
                                      <div class="modal-content">
                                        <div class="modal-header">
                                          <h5 class="modal-title">Repeat Case</h5>
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                          </button>
                                        </div>
                                        <div class="modal-body">
                                                <strong style="color: black"><h6>Fill the fields below to be able to repeat the case</h6></strong>
                                                <div class="form-group">
                                                        <div class="form-group">
                                                                <label for="milled">Required modifications: </label>
                                                                <div class="form-group form-group-last">
                                                                        <textarea class="form-control" name="note" id="exampleTextarea" rows="3">{{old('note')}}</textarea>
                                                                    </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="status">Choose Phase: </label>
                                                                <select class="form-control" name="status">
                                                                    <option value="scan" {{old('status') == 'scan' ? 'selected' : ''}}>Scan</option>
                                                                    <option value="0" {{old('status') == 0 ? 'selected' : ''}}>Design</option>
                                                                        <option value="1" {{old('status') == 1 ? 'selected' : ''}}>Milling</option>
                                                                        <option value="2" {{old('status') == 2 ? 'selected' : ''}}>Sintering furnace</option>
                                                                        <option value="3" {{old('status') == 3 ? 'selected' : ''}}>Finishing and build up</option>
                                                                        <option selected hidden disabled>Select the phase</option>
                                                                    </select>
                                                            </div>
                                                </div>
                                        </div>
                                        <div class="modal-footer">
                                          <button type="submit" class="btn btn-primary">Save changes</button>
                                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        </div>
                                      </div>
                                    </div>
                                </form>
                                  </div>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="kt-portlet">
        <div class="kt-portlet__body">
            <div class="kt-pagination kt-pagination--brand">
                {{$orders->links()}}
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
                $("#date").on('change', function(){
        $("#kt_subheader_search_form").submit();
    });
    $("#doctor").on('change', function(){
        $("#kt_subheader_search_form").submit();
    });
    function sweetalert() {
        swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {

            if (result.value) {
                swalWithBootstrapButtons.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                )
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                swalWithBootstrapButtons.fire(
                    'Cancelled',
                    'Your imaginary file is safe :)',
                    'error'
                )
            }
        })
    }
</script>
@endsection
