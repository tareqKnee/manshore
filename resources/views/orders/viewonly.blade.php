@extends('layout/layout')

@section('head')
<title>View order</title>
@endsection

@section('body-header')
<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">
				View Order
			</h3>
			<span class="kt-subheader__separator kt-subheader__separator--v"></span>
			<div class="kt-subheader__group" id="kt_subheader_search">
				<span class="kt-subheader__desc" id="kt_subheader_total">
                        View order details </span>
			</div>
		</div>
	</div>
</div>
<!-- end:: Content Head -->
@endsection
@section('body')

									<!--begin::Portlet-->
									<div class="kt-portlet">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
                                                        View order
												</h3>
											</div>
										</div>

										<!--begin::Form-->
                                        <form class="kt-form">
											<div class="kt-portlet__body">
                                                    <div class="form-group row">
                                                            <label class="col-2 col-form-label">Case ID : </label>
                                                            <div class="input-group input-group-sm col-2">

                                                                    <input type="text" class="form-control" id="unremovable" name="order_id" aria-describedby="basic-addon2" value="{{$order->order_id}}">
                                                            </div>
                                                            <div class="input-group input-group-sm col-2">
                                                                    Date: {{$order->created_at->format('m/d/Y')}}
                                                            </div>
                                                        </div>
												<div class="form-group row">
                                                        <div class="form-group col-xs-10" >
                                                                <label for="doctor_id">Doctor name: </label>
                                                                <br>
                                                                <select class="form-control" name="doctor_id">
                                                                    <option>{{$order->doctors->name}}</option>
                                                                </select>
                                                            </div>

                                                            <div class="form-group col-xs-6" style="margin-left: 5%">
                                                                <label for="patient_name">Patient name: </label>
                                                                <input id="patient_name" class="form-control input-group-lg reg_name" type="text" name="patient_name"
                                                                       title="Enter patient name"
                                                                       placeholder="Patient name" value="{{$order->patient_name}}"/>
                                                            </div>
                                                            <div class="form-group col-xs-6" style="margin-left: 5%">
                                                                <label for="service_id">Impression: </label>
                                                                <select class="form-control" id="service_id" name="service_id">
                                                                    @foreach($services as $serv)
                                                                    <option value="{{$serv->id}}" {{$order->service_id == $serv->id ? 'selected' : ''}}>{{$serv->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                                <div class="form-group col-xs-6" style="margin-left: 5%">
                                                                        <label for="delivery_date">Delivery date: </label>
                                                                        <input id="delivery_date" class="form-control input-group-lg reg_name" type="datetime-local" name="delivery_date"
                                                                               title="Enter delivery date"
                                                                               placeholder="delivery date" value="{{$order->deliver_date}}"/>
                                                                    </div>
                                                </div>
                                                        <div class="kt-portlet__head">
                                                            <div class="kt-portlet__head-label">
                                                                <h3 class="kt-portlet__head-title">
                                                                        <i class="fa fa-gavel"></i> Milling
                                                                </h3>
                                                            </div>
                                                        </div>
                                                        <br>

                                                        <div class="form-group row">
                                                            <div class="form-group col-xs-6" >
                                                                    <label for="milled">Milled: </label>
                                                                    <select class="form-control" id="milled" name="milled">
                                                                            <option value="1" {{$order->milled == 1 ? 'selected' : ''}}>Externally</option>
                                                                            <option value="0" {{$order->milled == 0 ? 'selected' : ''}}>Internally</option>
                                                                        </select>
                                                                </div>

                                                                <div class="form-group col-xs-6" style="margin-left: 5%">
                                                                    <label for="milled_by">Milled by: </label>
                                                                    <input id="milled_by" class="form-control input-group-lg reg_name" type="text" name="milled_by"
                                                                           title="Write the person name"
                                                                           placeholder="Write the person name" value="{{$order->milled_by}}"/>
                                                                </div>
                                                                <div class="form-group col-xs-6" style="margin-left: 5%">
                                                                        <label for="lab_id">Lab name: </label>
                                                                        <select class="form-control" name="lab_id">
                                                                                <option selected hidden disabled>Select your lab</option>
                                                                                @foreach($labs as $lab)
                                                                                <option value="{{$lab->id}}" {{$order->lab_id == $lab->id ? 'selected' : ''}}>{{$lab->name}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                    </div>
                                                    </div>

                                                        <div class="kt-portlet__head">
                                                                <div class="kt-portlet__head-label">
                                                                    <h3 class="kt-portlet__head-title">
                                                                            <i class="fa fa-briefcase"></i> Job information
                                                                    </h3>
                                                                </div>
                                                            </div>
                                                            <br>
                                                            <div class="form-group">
                                                                @foreach($order->jobs as $job)
                                                                @php
                                                                    $unit = explode(', ',$job->unit_num);
                                                                @endphp
                                                                <input name="job_ids[]" value="{{$job->id}}" type="hidden">
                                                                    <div>
                                                                            <div class="form-group form-group-last row">
                                                                                <div class="col-lg-10">
                                                                                    <div class="form-group row align-items-center">
                                                                                        <div class="col-2">
                                                                                            <div class="kt-form__group--inline">
                                                                                                <div class="kt-form__label">
                                                                                                    <label>Unit number:</label>
                                                                                                </div>
                                                                                                <div class="kt-form__control">
                                                                                                    <select class="form-control selectpicker" multiple name="old_unit_num_{{$job->id}}[]">
                                                                                                        @for($i = 1; $i < 33; $i++)
                                                                                                        <option value="{{$i}}" {{in_array($i, $unit) ? 'selected' : ''}}>{{$i}}</option>
                                                                                                        @endfor
                                                                                                    </select>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="d-md-none kt-margin-b-10"></div>
                                                                                        </div>

                                                                                        <div class="col-2">
                                                                                            <div class="kt-form__group--inline">
                                                                                                <div class="kt-form__label">
                                                                                                    <label class="kt-label m-label--single">Job type:</label>
                                                                                                </div>
                                                                                                <div class="kt-form__control">
                                                                                                        <select class="form-control" id="type" name="old_type">
                                                                                                                @foreach($types as $type)
                                                                                                                <option value="{{$type->id}}" {{$type->id == $job->type ? 'selected' : ''}}>{{$type->name}}</option>
                                                                                                            @endforeach
                                                                                                        </select>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="d-md-none kt-margin-b-10"></div>
                                                                                        </div>
                                                                                        <div class="col-2">
                                                                                                <div class="kt-form__group--inline">
                                                                                                    <div class="kt-form__label">
                                                                                                        <label>Material:</label>
                                                                                                    </div>
                                                                                                    <div class="kt-form__control">
                                                                                                            <select class="form-control" id="material_id" name="old_material_id_{{$job->id}}">
                                                                                                                    @foreach($materials as $m)
                                                                                                                    <option value="{{$m->id}}" {{$job->material_id == $m->id ? 'selected' : ''}}>
                                                                                                                        {{$m->name}}
                                                                                                                    </option>
                                                                                                                    @endforeach
                                                                                                            </select>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="d-md-none kt-margin-b-10"></div>
                                                                                            </div>
                                                                                            <div class="col-2">
                                                                                                    <div class="kt-form__group--inline">
                                                                                                        <div class="kt-form__label">
                                                                                                            <label>Color:</label>
                                                                                                        </div>
                                                                                                        <div class="kt-form__control">
                                                                                                                <select class="form-control" id="color" name="old_color">
                                                                                                                        <option value="A1">A1</option>
                                                                                                                        <option value="A2">A2</option>
                                                                                                                        <option value="A3">A3</option>
                                                                                                                        <option value="A3.5">A3.5</option>
                                                                                                                        <option value="A4">A4</option>
                                                                                                                        <option value="B1">B1</option>
                                                                                                                        <option value="B2">B2</option>
                                                                                                                        <option value="B3">B3</option>
                                                                                                                        <option value="B4">B4</option>
                                                                                                                        <option value="C1">C1</option>
                                                                                                                        <option value="C2">C2</option>
                                                                                                                        <option value="C3">C3</option>
                                                                                                                        <option value="C4">C4</option>
                                                                                                                        <option value="D2">D2</option>
                                                                                                                        <option value="D3">D3</option>
                                                                                                                        <option value="D4">D4</option>
                                                                                                                        <option value="BL1">BL1</option>
                                                                                                                        <option value="BL2">BL2</option>
                                                                                                                        <option value="BL3">BL3</option>
                                                                                                                        <option value="BL4">BL4</option>
                                                                                                                </select>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                                                                </div>
                                                                                                <div class="col-2">
                                                                                                        <div class="kt-form__group--inline">
                                                                                                                <div class="kt-form__label">
                                                                                                        <label>Style:</label>
                                                                                                                </div>
                                                                                                            <div class="kt-radio-inline">
                                                                                                                <label class="kt-radio">
                                                                                                                    <input type="radio" value="Bridge" name="old_style_{{$job->id}}" {{$job->style == 'Bridge' ? 'checked' : ''}}> Bridge
                                                                                                                    <span></span>
                                                                                                                </label>
                                                                                                                <label class="kt-radio">
                                                                                                                    <input type="radio" value="Single" name="old_style_{{$job->id}}" {{$job->style == 'Single' ? 'checked' : ''}}> Single
                                                                                                                    <span></span>
                                                                                                                </label>
                                                                                                            </div>
                                                                                                </div>
                                                                                            </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        @endforeach
                                                            </div>

                                                            <div class="kt-portlet__head">
                                                                    <div class="kt-portlet__head-label">
                                                                        <h3 class="kt-portlet__head-title">
                                                                                <i class="fa fa-sticky-note"></i> Additional information
                                                                        </h3>
                                                                    </div>
                                                                </div>
                                                                <br>
                                                                <div class="form-group form-group-last">
                                                                        <label for="exampleTextarea">Note</label>
                                                                        @php
                                                                            if($order->note){
                                                                                $break = explode('<br>', $order->note);
                                                                            }
                                                                        @endphp
                                                                        <textarea class="form-control" name="note" id="exampleTextarea" rows="3">@if(isset($break))@foreach($break as $b){!!$b!!}&#13;&#10;@endforeach @endif</textarea>
                                                                    </div>
											</div>
										</form>

										<!--end::Form-->
									</div>

<!-- end:: Content -->
@endsection
@section('scripts')
<script src="/js/jquery.repeater.min.js" defer></script>
<script>
    $(":input").prop("disabled", true);
    $(":select").prop("disabled", true);
   // $('#addLoL').selectpicker('refresh');
	$('select[name="position"]').on('change', function() {
		var selected = $(this).find('option:selected');
		var extra = selected.data('content');
		if (extra == 'B') {
			$('#TypeB').removeAttr('hidden')
		} else {
			$('#TypeB').prop('hidden', true)
		}
	})
    $("#unremovable").keydown(function(e) {
    var oldvalue=$(this).val();
    var field=this;
    setTimeout(function () {
        if(field.value.indexOf("{{Auth()->user()->id.'_'.now()->format('Y').now()->format('m').now()->format('d').'_'}}") !== 0) {
            $(field).val(oldvalue);
        }
    }, 1);
});
</script>
@stop
