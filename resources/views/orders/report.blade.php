@extends('layout/layout')

@section('head')
<title>View Cases</title>
@endsection

@section('body-header')
<div class="kt-subheader   kt-grid__item" id="kt_subheader" style="height:auto">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <div class="kt-subheader__group" id="kt_subheader_search">
                <h3 class="kt-subheader__title">
                    Cases
                </h3>
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                <span class="kt-subheader__desc" id="kt_subheader_total">
                    {{$orders->total()}} Total </span>
            </div>
        </div>
    </div>
</div>
@endsection

@section('body')
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__body">
                <form id="kt_subheader_search_form" method="GET" action="{{route('CasesReportTool')}}">
                    <div class="form-row">
                        <div class="col-lg-3 col-md-4 col-6 mb-3">
                            <div class="kt-subheader__search" style="">
                                <label>From:</label>
                                <input type="date" class="form-control" name="from" value="{{$from}}">
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-6 mb-3">
                            <div class="kt-subheader__search" style="">
                               <label>To:</label>
                                <input type="date" class="form-control" name="to" value="{{$to}}">
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-6 mb-3">
                            <div class="kt-subheader__search" style="">
                                <label>Doctor:</label>
                                <select class="form-control" name="doctor">
                                    <option selected hidden disabled>Select the doctor</option>
                                    @foreach($doctors as $d)
                                    <option value="{{$d->id}}" {{$doctor == $d->id ? 'selected' : ''}}>{{$d->name}}</option>
                                    @endforeach
                                    <option value="all" {{$doctor == "all" ? 'selected' : ''}}>All</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-6 mb-3">
                            <div class="kt-subheader__search" style="">
                                <label>Select Job type:</label>
                                <select class="form-control" name="job">
                                    <option selected hidden disabled>Select the job type</option>
                                    @foreach($jobs as $job)
                                    <option value="{{$job->id}}" {{isset($_GET['job']) && $_GET['job'] == $job->id ? 'selected' : ''}}>{{$job->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-6 mb-3">
                            <div class="kt-subheader__search" style="">
                                <label>Select Material:</label>
                                <select class="form-control" name="material_id">
                                    <option selected hidden disabled>Select the material</option>
                                    <option> All materials </option>
                                    @foreach($materials as $mat)
                                    <option value="{{$mat->id}}" {{isset($_GET['material_id']) && $_GET['material_id'] == $mat->id ? 'selected' : ''}}>{{$mat->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-6 mb-3">
                            <div class="kt-subheader__search" style="">
                                <label>Select Status:</label>
                                <select class="form-control" name="status">
                                    <option selected hidden disabled>Select the Status</option>
                                    <option value="completed" {{isset($_GET['status']) && $_GET['status'] == 'completed' ? 'selected' : ''}}>Completed</option>
                                    <option value="not" {{isset($_GET['status']) && $_GET['status'] == 'not' ? 'selected' : ''}}>Not completed</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-6 mb-3">
                            <div class="kt-subheader__search" style="">
                                <label>Select Impression type:</label>
                                <select class="form-control" name="impression_id">
                                    <option selected hidden disabled>Select Impress type</option>
                                    @foreach($impression as $imp)
                                    <option value="{{$imp->id}}" {{isset($_GET['impression_id']) && $_GET['impression_id'] == $imp->id ? 'selected' : ''}}>{{$imp->name}}</option>
                                    @endforeach
                                    <option value="all" {{isset($_GET['impression_id']) && $_GET['impression_id'] == "all" ? 'selected' : ''}}>All</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-6 mb-3">
                            <div class="kt-subheader__search" style="">
                                <label>Select Designer:</label>
                                <select class="form-control" name="designed_by">
                                    <option selected hidden disabled>Select the designer</option>
                                    @foreach($designers as $des)
                                    <option value="{{$des->id}}" {{isset($_GET['designed_by']) && $_GET['designed_by'] == $des->id ? 'selected' : ''}}>{{$des->first_name}} {{$des->last_name}}</option>
                                    @endforeach
                                    <option value="all" {{isset($_GET['designed_by']) && $_GET['designed_by'] == "all" ? 'selected' : ''}}>All</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-6 mb-3">
                            <div class="kt-subheader__search" style="">
                                <label>Select miller:</label>
                                <select class="form-control" name="milled_by">
                                    <option selected hidden disabled>Select the miller</option>
                                    @foreach($millings as $des)
                                    <option value="{{$des->id}}" {{isset($_GET['milled_by']) && $_GET['milled_by'] == $des->id ? 'selected' : ''}}>{{$des->first_name}} {{$des->last_name}}</option>
                                    @endforeach
                                    <option value="all" {{isset($_GET['milled_by']) && $_GET['milled_by'] == "all" ? 'selected' : ''}}>All</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-6 mb-3">
                            <div class="kt-subheader__search" style="">
                                <label>Select the furnace worker:</label>
                                <select class="form-control" name="furnaced_by">
                                    <option selected hidden disabled>Select the furnace worker</option>
                                    @foreach($furances as $des)
                                    <option value="{{$des->id}}" {{isset($_GET['furnaced_by']) && $_GET['furnaced_by'] == $des->id ? 'selected' : ''}}>{{$des->first_name}} {{$des->last_name}}</option>
                                    @endforeach
                                    <option value="all" {{isset($_GET['furnaced_by']) && $_GET['furnaced_by'] == "all" ? 'selected' : ''}}>All</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-6 mb-3">
                            <div class="kt-subheader__search" style="">
                                <label>Select the finisher:</label>
                                <select class="form-control" name="finished_by">
                                    <option selected hidden disabled>Select the finisher</option>
                                    @foreach($finishers as $des)
                                    <option value="{{$des->id}}" {{isset($_GET['finished_by']) && $_GET['finished_by'] == $des->id ? 'selected' : ''}}>{{$des->first_name}} {{$des->last_name}}</option>
                                    @endforeach
                                    <option value="all" {{isset($_GET['finished_by']) && $_GET['finished_by'] == "all" ? 'selected' : ''}}>All</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-6 mb-3">
                            <div class="kt-subheader__search" style="">
                                <label>Select the QA:</label>
                                <select class="form-control" name="quality_by">
                                    <option selected hidden disabled>Select the QA</option>
                                    @foreach($quality as $des)
                                    <option value="{{$des->id}}" {{isset($_GET['quality_by']) && $_GET['quality_by'] == $des->id ? 'selected' : ''}}>{{$des->first_name}} {{$des->last_name}}</option>
                                    @endforeach
                                    <option value="all" {{isset($_GET['quality_by']) && $_GET['quality_by'] == "all" ? 'selected' : ''}}>All</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-6 mb-3">
                            <div class="kt-subheader__search" style="">
                                <label>Select the delivery person:</label>
                                <select class="form-control" name="delivered_by">
                                    <option selected hidden disabled>Select the delivery person</option>
                                    @foreach($delivery as $des)
                                    <option value="{{$des->id}}" {{isset($_GET['delivered_by']) && $_GET['delivered_by'] == $des->id ? 'selected' : ''}}>{{$des->first_name}} {{$des->last_name}}</option>
                                    @endforeach
                                    <option value="all" {{isset($_GET['delivered_by']) && $_GET['delivered_by'] == "all" ? 'selected' : ''}}>All</option>
                                </select>
                            </div>
                        </div>
                    </div>
                <button type="submit" class="btn btn-primary">Search</button>
            </form>
        </div>
    </div>
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body kt-portlet__body--fit">
            <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" id="kt_apps_user_list_datatable" style="">
                <table class="kt-datatable__table">
                    <thead class="kt-datatable__head">
                        <tr class="kt-datatable__row" >
                            <th class="kt-datatable__cell"><span style="width: 200px;">Cases ID</span></th>
                            <th class="kt-datatable__cell"><span style="width: 160px; margin: auto; text-align: center">Doctor Name</span></th>
                            <th class="kt-datatable__cell"><span style="width: 160px; margin: auto; text-align: center">Impression type</span></th>
                            <th class="kt-datatable__cell"><span style="width: 160px; margin: auto; text-align: center">Design by</span></th>
                            <th class="kt-datatable__cell"><span style="width: 160px; margin: auto; text-align: center">Milling by</span></th>
                            <th class="kt-datatable__cell"><span style="width: 160px; margin: auto; text-align: center">Furnace by</span></th>
                            <th class="kt-datatable__cell"><span style="width: 160px; margin: auto; text-align: center">Finishing by</span></th>
                            <th class="kt-datatable__cell"><span style="width: 160px; margin: auto; text-align: center">QC by</span></th>
                            <th class="kt-datatable__cell"><span style="width: 160px; margin: auto; text-align: center">Delivered by</span></th>
                            <th class="kt-datatable__cell"><span style="width: 160px; margin: auto; text-align: center">Status</span></th>
                            <th class="kt-datatable__cell"><span style="width: 160px; margin: auto; text-align: center">Units</span></th>
                            <th class="kt-datatable__cell"><span style="width: 150px; margin: auto; text-align: center">Amount</span></th>
                        </tr>
                    </thead>
                    @php
                    $units = 0;
                    $invoice = 0;
                    @endphp
                    <tbody style="" class="kt-datatable__body">
                        @foreach($orders as $key=>$order)
                        @php
                        $invoice += isset($order->invoice->amount) ? $order->invoice->amount : 0;
                        $now_units = 0;
                        foreach($order->Jobs as $job){
                            $array = explode(',', $job->unit_num);
                            $now_units += count($array);
                        }
                        $now_amount = isset($order->invoice->amount) ? $order->invoice->amount : 0;
                        @endphp
                        <tr data-row="{{$order->id}}" class="kt-datatable__row" >
                            <td class="kt-datatable__cell--sorted kt-datatable__cell"><span style="width: 200px;">
                                 {{$order->order_id}}
                                </span>
                            </td>
                            <td class="kt-datatable__cell"><span style="width: 160px; margin: auto; text-align: center;margin: auto;">{{$order->doctors->name}}</span></td>
                            @if(isset($order->services->name))
                            <td class="kt-datatable__cell"><span style="width: 160px; margin: auto; text-align: center;margin: auto;">{{$order->services->name}}</span></td>
                            @else
                            <td class="kt-datatable__cell"><span style="width: 160px; margin: auto; text-align: center;margin: auto;">N/A</span></td>
                            @endif
                            @php
                              $name = $order->Logs->where('current_status', 1)->first();
                              if($name){
                                $name = $name->user->first_name . ' ' . $name->user->last_name;
                              } else {
                                $name = "Not picked yet";
                              }
                            @endphp
                            <td class="kt-datatable__cell"><span style="width: 160px; margin: auto; text-align: center;margin: auto;">{{$name}}</span></td>
                            @php
                              $name = $order->Logs->where('current_status', 2)->first();
                              if($name){
                                $name = $name->user->first_name . ' ' . $name->user->last_name;
                              } else {
                                  $name = "Not picked yet";
                              }
                            @endphp
                          <td class="kt-datatable__cell"><span style="width: 160px; margin: auto; text-align: center;margin: auto;">{{$name}}</span></td>
                          @php
                          $name = $order->Logs->where('current_status', 3)->first();
                          if($name){
                            $name = $name->user->first_name . ' ' . $name->user->last_name;
                          } else {
                              $name = "Not picked yet";
                          }
                        @endphp
                        <td class="kt-datatable__cell"><span style="width: 160px; margin: auto; text-align: center;margin: auto;">{{$name}}</span></td>
                        @php
                        $name = $order->Logs->where('current_status', 4)->first();
                        if($name){
                            $name = $name->user->first_name . ' ' . $name->user->last_name;
                        } else {
                            $name = "Not picked yet";
                        }
                      @endphp
                      <td class="kt-datatable__cell"><span style="width: 160px; margin: auto; text-align: center;margin: auto;">{{$name}}</span></td>
                      @php
                      $name = $order->Logs->where('current_status', 5)->first();
                      if($name){
                        $name = $name->user->first_name . ' ' . $name->user->last_name;
                      } else {
                          $name = "Not picked yet";
                      }
                    @endphp
                    <td class="kt-datatable__cell"><span style="width: 160px; margin: auto; text-align: center;margin: auto;">{{$name}}</span></td>
                    @php
                    $name = $order->Logs->where('current_status', 7)->first();
                    if($name){
                      $name = $name->user->first_name . ' ' . $name->user->last_name;
                    } else {
                        $name = "Not picked yet";
                    }
                  @endphp
                  <td class="kt-datatable__cell"><span style="width: 160px; margin: auto; text-align: center;margin: auto;">{{$name}}</span></td>
                  <td class="kt-datatable__cell"><span style="width: 160px; margin: auto; text-align: center;margin: auto;">{{$order->total_status ? 'Completed' : 'Not completed'}}</span></td>
                  <td class="kt-datatable__cell"><span style="width: 160px; margin: auto; text-align: center;margin: auto;">{{$now_units}}</span></td>
                  <td class="kt-datatable__cell"><span style="width: 150px; margin: auto; text-align: center;margin: auto;">{{$now_amount}}</span></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <strong>Total money: {{$total_amount}}, Total units : {{$total_units}}</strong>
    <div class="kt-portlet">
        <div class="kt-portlet__body">
            <div class="kt-pagination kt-pagination--brand">
                {{$orders->appends($_GET)->links()}}
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
                $("#date").on('change', function(){
        $("#kt_subheader_search_form").submit();
    });
    function sweetalert() {
        swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {

            if (result.value) {
                swalWithBootstrapButtons.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                )
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                swalWithBootstrapButtons.fire(
                    'Cancelled',
                    'Your imaginary file is safe :)',
                    'error'
                )
            }
        })
    }
</script>
@endsection
