@extends('layout/layout')

@section('head')
<title>Edit order</title>
@endsection

@section('body-header')
<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">
				Edit Order
			</h3>
			<span class="kt-subheader__separator kt-subheader__separator--v"></span>
			<div class="kt-subheader__group" id="kt_subheader_search">
				<span class="kt-subheader__desc" id="kt_subheader_total">
					Enter order details and submit </span>
			</div>
		</div>
	</div>
</div>
<!-- end:: Content Head -->
@endsection
@section('body')

									<!--begin::Portlet-->
									<div class="kt-portlet">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Edit order
												</h3>
											</div>
										</div>

										<!--begin::Form-->
                                        <form class="kt-form" method="POST" action="{{route('orderUpdate')}}">
                                            @csrf
                                            <input name="id" value="{{$order->id}}" type="hidden">
                                            <input name="old_id" value="{{$order->order_id}}" type="hidden">
											<div class="kt-portlet__body">
                                                    <div class="form-group row">
                                                            <label class="col-2 col-form-label">Case ID : </label>
                                                            <div class="input-group input-group-sm col-5">

                                                                    <input type="text" class="form-control" id="unremovable" name="order_id" aria-describedby="basic-addon2" value="{{$order->order_id ?
                                                                    $order->order_id : Auth()->user()->id.'_'.now()->format('Y').now()->format('m').now()->format('d').'_'}}">
                                                            </div>
                                                            @if(isset($order->CreatedBy->is_admin))
                                                            <div class="input-group input-group-sm col-2">
                                                                @if($order->CreatedBy->is_admin) Created by: @else Desgined by: @endif<font color="blue"> {{$order->CreatedBy->first_name . ' ' . $order->CreatedBy->last_name}} </font>
                                                        </div>
                                                        @endif
                                                            <div class="input-group input-group-sm col-5">
                                                                    Date: {{now()}}
                                                            </div>
                                                        </div>
												<div class="form-group row">
                                                        <div class="form-group col-xs-10" >
                                                                <label for="doctor_id">Doctor name: </label>
                                                                <br>
                                                                <select class="form-control" name="doctor_id">
                                                                    <option selected hidden disabled>Select your doctor</option>
                                                                    @foreach($doctors as $doctor)
                                                                    <option value="{{$doctor->id}}" {{$order->doctor_id == $doctor->id ? 'selected' : ''}}>{{$doctor->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>

                                                            <div class="form-group col-xs-6" style="margin-left: 5%">
                                                                <label for="patient_name">Patient name: </label>
                                                                <input id="patient_name" class="form-control input-group-lg reg_name" type="text" name="patient_name"
                                                                       title="Enter patient name"
                                                                       placeholder="Patient name" value="{{$order->patient_name}}"/>
                                                            </div>
                                                            <div class="form-group col-xs-6" style="margin-left: 5%">
                                                                    <label for="service_id">Impression: </label>
                                                                    <select class="form-control" id="service_id" name="service_id">
                                                                        @foreach($services as $serv)
                                                                        <option value="{{$serv->id}}" {{$order->service_id == $serv->id ? 'selected' : ''}}>{{$serv->name}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="form-group col-xs-6" style="margin-left: 5%">
                                                                        <label for="delivery_date">Delivery date: </label>
                                                                        <input id="delivery_date" class="form-control input-group-lg reg_name" type="datetime-local" name="delivery_date"
                                                                               title="Enter delivery date"
                                                                               placeholder="delivery date" value="{{$order->deliver_date}}"/>
                                                                    </div>
                                                </div>
                                                @if(auth()->user()->is_admin)
                                                        <div class="kt-portlet__head">
                                                            <div class="kt-portlet__head-label">
                                                                <h3 class="kt-portlet__head-title">
                                                                        <i class="fa fa-gavel"></i> Milling
                                                                </h3>
                                                            </div>
                                                        </div>
                                                        <br>
                                                        <div class="form-group row">
                                                                <div class="form-group col-xs-6" >
                                                                        <label for="milled">Milled: </label>
                                                                        <select class="form-control" id="milled" name="milled">
                                                                                <option value="1" {{$order->milled == 1 ? 'selected' : ''}}>Externally</option>
                                                                                <option value="0" {{$order->milled == 0 ? 'selected' : ''}}>Internally</option>
                                                                                @if(!$order->milled_by && !$order->lab_id)
                                                                                    <option selected hidden disabled>Select your lab</option>
                                                                                @endif
                                                                            </select>
                                                                    </div>

                                                                    <div class="form-group col-xs-6" style="margin-left: 5%">
                                                                        <label for="milled_by">Milled by: </label>
                                                                        <input id="milled_by" class="form-control input-group-lg reg_name" type="text" name="milled_by"
                                                                               title="Write the person name"
                                                                               placeholder="Write the person name" value="{{$order->milled_by}}"/>
                                                                    </div>
                                                                    <div class="form-group col-xs-6" style="margin-left: 5%">
                                                                            <label for="lab_id">Lab name: </label>
                                                                            <select class="form-control" name="lab_id">
                                                                                    <option selected hidden disabled>Select your lab</option>
                                                                                    @foreach($labs as $lab)
                                                                                    <option value="{{$lab->id}}" {{$order->lab_id == $lab->id ? 'selected' : ''}}>{{$lab->name}}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                        </div>
                                                        </div>
                                                        @endif
                                                        <div class="kt-portlet__head">
                                                                <div class="kt-portlet__head-label">
                                                                    <h3 class="kt-portlet__head-title">
                                                                            <i class="fa fa-briefcase"></i> Job information
                                                                    </h3>
                                                                </div>
                                                            </div>
                                                            <br>
                                                            <div class="form-group">
                                                                @foreach($order->jobs as $job)
                                                                @php
                                                                    $unit = explode(', ',$job->unit_num);
                                                                @endphp
                                                                <input name="job_ids[]" value="{{$job->id}}" type="hidden">
                                                                    <div>
                                                                            <div class="form-group form-group-last row">
                                                                                <div class="col-lg-10">
                                                                                    <div class="form-group row align-items-center">
                                                                                        <div class="col-2">
                                                                                            <div class="kt-form__group--inline">
                                                                                                <div class="kt-form__label">
                                                                                                    <label>Unit number:</label>
                                                                                                </div>
                                                                                                <div class="kt-form__control">
                                                                                                    <select class="form-control selectpicker" multiple name="old_unit_num_{{$job->id}}[]">
                                                                                                        @for($i = 1; $i < 33; $i++)
                                                                                                        <option value="{{$i}}" {{in_array($i, $unit) ? 'selected' : ''}}>{{$i}}</option>
                                                                                                        @endfor
                                                                                                    </select>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="d-md-none kt-margin-b-10"></div>
                                                                                        </div>

                                                                                        <div class="col-2">
                                                                                            <div class="kt-form__group--inline">
                                                                                                <div class="kt-form__label">
                                                                                                    <label class="kt-label m-label--single">Job type:</label>
                                                                                                </div>
                                                                                                <div class="kt-form__control">
                                                                                                        <select class="form-control" id="type" name="old_type_{{$job->id}}">
                                                                                                                @foreach($types as $type)
                                                                                                                <option value="{{$type->id}}" {{$type->id == $job->type ? 'selected' : ''}}>{{$type->name}}</option>
                                                                                                            @endforeach
                                                                                                        </select>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="d-md-none kt-margin-b-10"></div>
                                                                                        </div>
                                                                                        <div class="col-2">
                                                                                                <div class="kt-form__group--inline">
                                                                                                    <div class="kt-form__label">
                                                                                                        <label>Material:</label>
                                                                                                    </div>
                                                                                                    <div class="kt-form__control">
                                                                                                            <select class="form-control" id="material_id" name="old_material_id_{{$job->id}}">
                                                                                                                    @foreach($materials as $m)
                                                                                                                    <option value="{{$m->id}}" {{$job->material_id == $m->id ? 'selected' : ''}}>
                                                                                                                        {{$m->name}}
                                                                                                                    </option>
                                                                                                                    @endforeach
                                                                                                            </select>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="d-md-none kt-margin-b-10"></div>
                                                                                            </div>
                                                                                            <div class="col-2">
                                                                                                    <div class="kt-form__group--inline">
                                                                                                        <div class="kt-form__label">
                                                                                                            <label>Color:</label>
                                                                                                        </div>
                                                                                                        <div class="kt-form__control">
                                                                                                                <select class="form-control" id="color" name="old_color_{{$job->id}}">
                                                                                                                        <option value="A1" {{$job->color == 'A1' ? 'selected' : ''}}>A1</option>
                                                                                                                        <option value="A2" {{$job->color == 'A2' ? 'selected' : ''}}>A2</option>
                                                                                                                        <option value="A3" {{$job->color == 'A3' ? 'selected' : ''}}>A3</option>
                                                                                                                        <option value="A3.5" {{$job->color == 'A3.5' ? 'selected' : ''}}>A3.5</option>
                                                                                                                        <option value="A4" {{$job->color == 'A4' ? 'selected' : ''}}>A4</option>
                                                                                                                        <option value="B1" {{$job->color == 'B1' ? 'selected' : ''}}>B1</option>
                                                                                                                        <option value="B2" {{$job->color == 'B2' ? 'selected' : ''}}>B2</option>
                                                                                                                        <option value="B3" {{$job->color == 'B3' ? 'selected' : ''}}>B3</option>
                                                                                                                        <option value="B4" {{$job->color == 'B4' ? 'selected' : ''}}>B4</option>
                                                                                                                        <option value="C1" {{$job->color == 'C1' ? 'selected' : ''}}>C1</option>
                                                                                                                        <option value="C2" {{$job->color == 'C2' ? 'selected' : ''}}>C2</option>
                                                                                                                        <option value="C3" {{$job->color == 'C3' ? 'selected' : ''}}>C3</option>
                                                                                                                        <option value="C4" {{$job->color == 'C4' ? 'selected' : ''}}>C4</option>
                                                                                                                        <option value="D2" {{$job->color == 'D2' ? 'selected' : ''}}>D2</option>
                                                                                                                        <option value="D3" {{$job->color == 'D3' ? 'selected' : ''}}>D3</option>
                                                                                                                        <option value="D4" {{$job->color == 'D4' ? 'selected' : ''}}>D4</option>
                                                                                                                        <option value="BL1" {{$job->color == 'BL1' ? 'selected' : ''}}>BL1</option>
                                                                                                                        <option value="BL2" {{$job->color == 'BL2' ? 'selected' : ''}}>BL2</option>
                                                                                                                        <option value="BL3" {{$job->color == 'BL3' ? 'selected' : ''}}>BL3</option>
                                                                                                                        <option value="BL4" {{$job->color == 'BL4' ? 'selected' : ''}}>BL4</option>
                                                                                                                </select>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                                                                </div>
                                                                                                <div class="col-2">
                                                                                                        <div class="kt-form__group--inline">
                                                                                                                <div class="kt-form__label">
                                                                                                        <label>Style:</label>
                                                                                                                </div>
                                                                                                            <div class="kt-radio-inline">
                                                                                                                <label class="kt-radio">
                                                                                                                    <input type="radio" value="Bridge" name="old_style_{{$job->id}}" {{$job->style == 'Bridge' ? 'checked' : ''}}> Bridge
                                                                                                                    <span></span>
                                                                                                                </label>
                                                                                                                <label class="kt-radio">
                                                                                                                    <input type="radio" value="Single" name="old_style_{{$job->id}}" {{$job->style == 'Single' ? 'checked' : ''}}> Single
                                                                                                                    <span></span>
                                                                                                                </label>
                                                                                                            </div>
                                                                                                </div>
                                                                                            </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        @endforeach
                                                            </div>
                                                            <div class="form-group">
                                                                    <div id="kt_repeater_1">
                                                                            <div class="form-group form-group-last row" id="kt_repeater_1">
                                                                                <div data-repeater-list="repeat" class="col-lg-10">
                                                                                    <div data-repeater-item class="form-group row align-items-center">
                                                                                        <div class="col-2">
                                                                                            <div class="kt-form__group--inline">
                                                                                                <div class="kt-form__label">
                                                                                                    <label>Unit number:</label>
                                                                                                </div>
                                                                                                <div class="kt-form__control">
                                                                                                    <select class="form-control selectpicker" multiple name="unit_num">
                                                                                                        @for($i = 1; $i < 33; $i++)
                                                                                                        <option value="{{$i}}">{{$i}}</option>
                                                                                                        @endfor
                                                                                                    </select>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="d-md-none kt-margin-b-10"></div>
                                                                                        </div>

                                                                                        <div class="col-2">
                                                                                            <div class="kt-form__group--inline">
                                                                                                <div class="kt-form__label">
                                                                                                    <label class="kt-label m-label--single">Job type:</label>
                                                                                                </div>
                                                                                                <div class="kt-form__control">
                                                                                                        <select class="form-control" id="type" name="type">
                                                                                                                @foreach($types as $type)
                                                                                                                <option value="{{$type->id}}">{{$type->name}}</option>
                                                                                                            @endforeach
                                                                                                        </select>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="d-md-none kt-margin-b-10"></div>
                                                                                        </div>
                                                                                        <div class="col-2">
                                                                                                <div class="kt-form__group--inline">
                                                                                                    <div class="kt-form__label">
                                                                                                        <label>Material:</label>
                                                                                                    </div>
                                                                                                    <div class="kt-form__control">
                                                                                                            <select class="form-control" id="material_id" name="material_id">
                                                                                                                    @foreach($materials as $m)
                                                                                                                    <option value="{{$m->id}}">
                                                                                                                        {{$m->name}}
                                                                                                                    </option>
                                                                                                                    @endforeach
                                                                                                            </select>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="d-md-none kt-margin-b-10"></div>
                                                                                            </div>
                                                                                            <div class="col-2">
                                                                                                    <div class="kt-form__group--inline">
                                                                                                        <div class="kt-form__label">
                                                                                                            <label>Color:</label>
                                                                                                        </div>
                                                                                                        <div class="kt-form__control">
                                                                                                                <select class="form-control" id="color" name="color">
                                                                                                                        <option value="A1">A1</option>
                                                                                                                        <option value="A2">A2</option>
                                                                                                                        <option value="A3">A3</option>
                                                                                                                        <option value="A3.5">A3.5</option>
                                                                                                                        <option value="A4">A4</option>
                                                                                                                        <option value="B1">B1</option>
                                                                                                                        <option value="B2">B2</option>
                                                                                                                        <option value="B3">B3</option>
                                                                                                                        <option value="B4">B4</option>
                                                                                                                        <option value="C1">C1</option>
                                                                                                                        <option value="C2">C2</option>
                                                                                                                        <option value="C3">C3</option>
                                                                                                                        <option value="C4">C4</option>
                                                                                                                        <option value="D2">D2</option>
                                                                                                                        <option value="D3">D3</option>
                                                                                                                        <option value="D4">D4</option>
                                                                                                                        <option value="BL1">BL1</option>
                                                                                                                        <option value="BL2">BL2</option>
                                                                                                                        <option value="BL3">BL3</option>
                                                                                                                        <option value="BL4">BL4</option>
                                                                                                                </select>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                                                                </div>
                                                                                                <div class="col-2">
                                                                                                        <div class="kt-form__group--inline">
                                                                                                                <div class="kt-form__label">
                                                                                                        <label>Style:</label>
                                                                                                                </div>
                                                                                                            <div class="kt-radio-inline">
                                                                                                                <label class="kt-radio">
                                                                                                                    <input type="radio" name="style" value="Bridge"> Bridge
                                                                                                                    <span></span>
                                                                                                                </label>
                                                                                                                <label class="kt-radio">
                                                                                                                    <input type="radio" checked="checked" name="style" value="Single"> Single
                                                                                                                    <span></span>
                                                                                                                </label>
                                                                                                            </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        <div class="col-2">
                                                                                            <a href="javascript:;" data-repeater-delete="" class="btn-sm btn btn-label-danger btn-bold">
                                                                                                <i class="la la-trash-o"></i>
                                                                                                Delete
                                                                                            </a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group form-group-last row">
                                                                                <label class="col-lg-2 col-form-label"></label>
                                                                                <div class="col-lg-4">
                                                                                    <a href="javascript:;" id="addLoL" data-repeater-create="" class="btn btn-bold btn-sm btn-label-brand">
                                                                                        <i class="la la-plus"></i> Add
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                            </div>

                                                            <div class="kt-portlet__head">
                                                                    <div class="kt-portlet__head-label">
                                                                        <h3 class="kt-portlet__head-title">
                                                                                <i class="fa fa-sticky-note"></i> Additional information
                                                                        </h3>
                                                                    </div>
                                                                </div>
                                                                <br>
                                                                <div class="form-group form-group-last">
                                                                        <label for="exampleTextarea">Note</label>
                                                                        @php
                                                                            if($order->note){
                                                                                $break = explode('<br>', $order->note);
                                                                            }
                                                                        @endphp
                                                                        <textarea class="form-control" name="note" id="exampleTextarea" rows="3">@if(isset($break))@foreach($break as $b){!!$b!!}&#13;&#10;@endforeach @endif</textarea>
                                                                    </div>
											</div>
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<button type="submit" class="btn btn-primary">Submit</button>
													<button type="reset" class="btn btn-danger">Reset</button>
												</div>
											</div>
										</form>

										<!--end::Form-->
									</div>

<!-- end:: Content -->
@endsection
@section('scripts')
@php
    $order_id = explode('_', $order->order_id);
@endphp
<script src="/js/jquery.repeater.min.js" defer></script>
<script>
        $('#addLoL').on('click', function() {
        var matches =  $(".btn-light").length;
            setTimeout(() => {
        $('.selectpicker').selectpicker('refresh');
        $('.btn-light').eq(matches+1).remove();
    }, 500);
    });
	$('select[name="position"]').on('change', function() {
		var selected = $(this).find('option:selected');
		var extra = selected.data('content');
		if (extra == 'B') {
			$('#TypeB').removeAttr('hidden')
		} else {
			$('#TypeB').prop('hidden', true)
		}
	})

    $('#milled').on('change', function() {
       if($('#milled').val() == 1){
           $('#lab_id').removeAttr('disabled');
           $('#milled_by').attr('disabled', true);
       } else {
           $('#lab_id').attr('disabled', true);
           $('#milled_by').removeAttr('disabled');
       }
   });

    $("#unremovable").keydown(function(e) {
    var oldvalue=$(this).val();
    var field=this;
    setTimeout(function () {
        if(field.value.indexOf('{{isset($order_id[0]) && isset($order_id[1]) ? $order_id[0] . '_' . $order_id[1] . '_' : Auth()->user()->id.'_'.now()->format('Y').now()->format('m').now()->format('d').'_'}}') !== 0) {
            $(field).val(oldvalue);
        }
    }, 1);
});
</script>
@stop
