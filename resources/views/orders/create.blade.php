@extends('layout/layout')

@section('head')
<title>Create order</title>
@endsection

@section('body-header')
<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">
				New Order
			</h3>
			<span class="kt-subheader__separator kt-subheader__separator--v"></span>
			<div class="kt-subheader__group" id="kt_subheader_search">
				<span class="kt-subheader__desc" id="kt_subheader_total">
					Enter order details and submit </span>
			</div>
		</div>
	</div>
</div>
<!-- end:: Content Head -->
@endsection
@section('body')

									<!--begin::Portlet-->
									<div class="kt-portlet">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Create order
												</h3>
											</div>
										</div>

										<!--begin::Form-->
                                        <form class="kt-form" method="POST" action="{{route('orderInsert')}}">
                                            @csrf
											<div class="kt-portlet__body">
                                                    <div class="form-group row">
                                                            <label class="col-md-2 col-form-label">Case ID : </label>
                                                            <div class="input-group input-group-sm col-2">

                                                                    <input type="text" class="form-control" id="unremovable" name="order_id" aria-describedby="basic-addon2" value="{{old('order_id') ?
                                                                    old('order_id') : Auth()->user()->id.'_'.now()->format('Y').now()->format('m').now()->format('d').'_'}}">
                                                            </div>
                                                            <div class="input-group input-group-sm col-2">
                                                                @if(Auth()->user()->is_admin) Created by: @else Desgined by: @endif<font color="blue"> {{Auth()->user()->first_name . ' ' . Auth()->user()->last_name}} </font>
                                                        </div>
                                                            <div class="input-group input-group-sm col-2">
                                                                    Date: {{now()->format('m/d/Y')}}
                                                            </div>
                                                        </div>
												<div class="form-group row">
                                                        <div class="form-group col-xs-10" >
                                                                <label for="doctor_id">Doctor name: </label>
                                                                <br>
                                                                <select class="form-control" name="doctor_id">
                                                                    <option selected hidden disabled>Select your doctor</option>
                                                                    @foreach($doctors as $doctor)
                                                                    <option value="{{$doctor->id}}" {{old('doctor_id') == $doctor->id ? 'selected' : ''}}>{{$doctor->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>

                                                            <div class="form-group col-xs-6" style="margin-left: 5%">
                                                                <label for="patient_name">Patient name: </label>
                                                                <input id="patient_name" class="form-control input-group-lg reg_name" type="text" name="patient_name"
                                                                       title="Enter patient name"
                                                                       placeholder="Patient name" value="{{old('patient_name')}}"/>
                                                            </div>
                                                            <div class="form-group col-xs-6" style="margin-left: 5%">
                                                                    <label for="service_id">Impression: </label>
                                                                    <select class="form-control" id="service_id" name="service_id">
                                                                            @foreach($services as $serv)
                                                                            <option value="{{$serv->id}}" {{old('service_id') == $serv->id ? 'selected' : ''}}>{{$serv->name}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                </div>
                                                                <div class="form-group col-xs-6" style="margin-left: 5%">
                                                                        <label for="delivery_date">Delivery date: </label>
                                                                        @php
                                                                            $time = now();
                                                                            $time = str_replace(' ','T', $time);
                                                                        @endphp
                                                                        <input id="delivery_date" class="form-control input-group-lg reg_name" type="datetime-local" name="delivery_date"
                                                                               title="Enter delivery date"
                                                                               placeholder="delivery date" value="{{old('delivery_date') ? old('delivery_date') : $time}}"/>
                                                                    </div>
                                                </div>
                                                                                                    {{--
                                                        <div class="kt-portlet__head">
                                                            <div class="kt-portlet__head-label">
                                                                <h3 class="kt-portlet__head-title">
                                                                        <i class="fa fa-gavel"></i> Milling
                                                                </h3>
                                                            </div>
                                                        </div>
                                                        <br>
                                                        <div class="form-group row">
                                                                <div class="form-group col-xs-6" >
                                                                        <label for="milled">Milled: </label>
                                                                        <select class="form-control" id="milled" name="milled">
                                                                                <option value="1" {{old('milled') == 1 ? 'selected' : ''}}>Externally</option>
                                                                                <option value="0" {{old('milled') == 0 ? 'selected' : ''}}>Internally</option>
                                                                                <option disabled selected hidden>Select your milling place</option>
                                                                            </select>
                                                                    </div>

                                                                    <div class="form-group col-xs-6" style="margin-left: 5%">
                                                                        <label for="milled_by">Milled by: </label>
                                                                        <input id="milled_by" class="form-control input-group-lg reg_name" type="text" name="milled_by"
                                                                               title="Write the person name"
                                                                               placeholder="Write the person name" id="milled_by" value="{{old('milled_by')}}"/>
                                                                    </div>
                                                                    <div class="form-group col-xs-6" style="margin-left: 5%">
                                                                            <label for="lab_id">Lab name: </label>
                                                                            <select class="form-control" name="lab_id" id="lab_id">
                                                                                    <option selected hidden disabled>Select your lab</option>
                                                                                    @foreach($labs as $lab)
                                                                                    <option value="{{$lab->id}}" {{old('lab_id') == $lab->id ? 'selected' : ''}}>{{$lab->name}}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                        </div>
                                                        </div> --}}
                                                        <div class="kt-portlet__head">
                                                                <div class="kt-portlet__head-label">
                                                                    <h3 class="kt-portlet__head-title">
                                                                            <i class="fa fa-briefcase"></i> Job information
                                                                    </h3>
                                                                </div>
                                                            </div>
                                                            <br>
                                                            <div class="form-group">
                                                                    <div id="kt_repeater_1">
                                                                            <div class="form-group form-group-last row">
                                                                                <div data-repeater-list="repeat" class="col-12">
                                                                                    <div data-repeater-item class="form-group row align-items-center">
                                                                                        <div class="col-md-2">
                                                                                            <div class="kt-form__group--inline">
                                                                                                <div class="kt-form__label">
                                                                                                    <label>Unit number:</label>
                                                                                                </div>
                                                                                                <div class="kt-form__control">
                                                                                                    <select class="form-control selectpicker" multiple name="unit_num">
                                                                                                        @for($i = 1; $i < 33; $i++)
                                                                                                        <option value="{{$i}}">{{$i}}</option>
                                                                                                        @endfor
                                                                                                    </select>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="d-md-none kt-margin-b-10"></div>
                                                                                        </div>

                                                                                        <div class="col-md-2">
                                                                                            <div class="kt-form__group--inline">
                                                                                                <div class="kt-form__label">
                                                                                                    <label class="kt-label m-label--single">Job type:</label>
                                                                                                </div>
                                                                                                <div class="kt-form__control">
                                                                                                        <select class="form-control" id="type" name="type">
                                                                                                            @foreach($types as $type)
                                                                                                                <option value="{{$type->id}}">{{$type->name}}</option>
                                                                                                            @endforeach
                                                                                                        </select>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="d-md-none kt-margin-b-10"></div>
                                                                                        </div>
                                                                                        <div class="col-md-2">
                                                                                                <div class="kt-form__group--inline">
                                                                                                    <div class="kt-form__label">
                                                                                                        <label>Material:</label>
                                                                                                    </div>
                                                                                                    <div class="kt-form__control">
                                                                                                            <select class="form-control" id="material_id" name="material_id">
                                                                                                                    @foreach($materials as $m)
                                                                                                                    <option value="{{$m->id}}">
                                                                                                                        {{$m->name}}
                                                                                                                    </option>
                                                                                                                    @endforeach
                                                                                                            </select>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="d-md-none kt-margin-b-10"></div>
                                                                                            </div>
                                                                                            <div class="col-md-2">
                                                                                                    <div class="kt-form__group--inline">
                                                                                                        <div class="kt-form__label">
                                                                                                            <label>Color:</label>
                                                                                                        </div>
                                                                                                        <div class="kt-form__control">
                                                                                                                <select class="form-control" id="color" name="color">
                                                                                                                    <option value="A1">A1</option>
                                                                                                                    <option value="A2">A2</option>
                                                                                                                    <option value="A3">A3</option>
                                                                                                                    <option value="A3.5">A3.5</option>
                                                                                                                    <option value="A4">A4</option>
                                                                                                                    <option value="B1">B1</option>
                                                                                                                    <option value="B2">B2</option>
                                                                                                                    <option value="B3">B3</option>
                                                                                                                    <option value="B4">B4</option>
                                                                                                                    <option value="C1">C1</option>
                                                                                                                    <option value="C2">C2</option>
                                                                                                                    <option value="C3">C3</option>
                                                                                                                    <option value="C4">C4</option>
                                                                                                                    <option value="D2">D2</option>
                                                                                                                    <option value="D3">D3</option>
                                                                                                                    <option value="D4">D4</option>
                                                                                                                    <option value="BL1">BL1</option>
                                                                                                                    <option value="BL2">BL2</option>
                                                                                                                    <option value="BL3">BL3</option>
                                                                                                                    <option value="BL4">BL4</option>
                                                                                                                </select>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="d-md-none kt-margin-b-10"></div>
                                                                                                </div>
                                                                                                <div class="col-md-2">
                                                                                                        <div class="kt-form__group--inline">
                                                                                                                <div class="kt-form__label">
                                                                                                        <label>Style:</label>
                                                                                                                </div>
                                                                                                            <div class="kt-radio-inline">
                                                                                                                <label class="kt-radio">
                                                                                                                    <input type="radio" name="style" value="Bridge"> Bridge
                                                                                                                    <span></span>
                                                                                                                </label>
                                                                                                                <label class="kt-radio">
                                                                                                                    <input type="radio" checked="checked" name="style"  value="Single"> Single
                                                                                                                    <span></span>
                                                                                                                </label>
                                                                                                            </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        <div class="col-md-2">
                                                                                            <a href="javascript:;" data-repeater-delete="" class="btn-sm btn btn-label-danger btn-bold">
                                                                                                <i class="la la-trash-o"></i>
                                                                                                Delete
                                                                                            </a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group form-group-last row">
                                                                                <label class="col-lg-2 col-form-label"></label>
                                                                                <div class="col-lg-4">
                                                                                    <a href="javascript:;" data-repeater-create="" class="btn btn-bold btn-sm btn-label-brand" id="addLoL">
                                                                                        <i class="la la-plus"></i> Add
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                            </div>

                                                            <div class="kt-portlet__head">
                                                                    <div class="kt-portlet__head-label">
                                                                        <h3 class="kt-portlet__head-title">
                                                                                <i class="fa fa-sticky-note"></i> Additional information
                                                                        </h3>
                                                                    </div>
                                                                </div>
                                                                <br>
                                                                <div class="form-group form-group-last">
                                                                        <label for="exampleTextarea">Note</label>
                                                                        <textarea class="form-control" name="note" id="exampleTextarea" rows="3">{{old('note')}}</textarea>
                                                                    </div>
											</div>
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<button type="submit" class="btn btn-primary">Submit</button>
													<button type="reset" class="btn btn-danger">Reset</button>
												</div>
											</div>
										</form>

										<!--end::Form-->
									</div>

<!-- end:: Content -->
@endsection
@section('scripts')
<script src="/js/jquery.repeater.min.js" defer></script>
<script>
    $('#addLoL').on('click', function() {
        var matches =  $(".btn-light").length;
            setTimeout(() => {
        $('.selectpicker').selectpicker('refresh');
        $('.btn-light').eq(matches+1).remove();
    }, 500);
    });
   $('#milled').on('change', function() {
       if($('#milled').val() == 1){
           $('#lab_id').removeAttr('disabled');
           $('#milled_by').attr('disabled', true);
       } else {
           $('#lab_id').attr('disabled', true);
           $('#milled_by').removeAttr('disabled');
       }
   });

	$('select[name="position"]').on('change', function() {
		var selected = $(this).find('option:selected');
		var extra = selected.data('content');
		if (extra == 'B') {
			$('#TypeB').removeAttr('hidden')
		} else {
			$('#TypeB').prop('hidden', true)
		}
	})
    $("#unremovable").keydown(function(e) {
    var oldvalue=$(this).val();
    var field=this;
    setTimeout(function () {
        if(field.value.indexOf("{{Auth()->user()->id.'_'.now()->format('Y').now()->format('m').now()->format('d').'_'}}") !== 0) {
            $(field).val(oldvalue);
        }
    }, 1);
});
</script>
@stop
