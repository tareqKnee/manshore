@extends('layout/layout')

@section('head')
<title>Dashboard</title>
@endsection

@section('body-header')
	<h3 class="kt-subheader__title">Dashboard</h3>
	<span class="kt-subheader__separator kt-subheader__separator--v"></span>
@endsection
@section('body')
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" id="kt_apps_user_list_datatable" style="">
                        <table class="kt-datatable__table" style="display: block;">
                                <thead class="kt-datatable__head">
                                    <tr class="kt-datatable__row" style="left: 0px;">
                                        <th class="kt-datatable__cell"><span class="middle" style="width: 200px;">Appointment ID</span></th>
                                        <th class="kt-datatable__cell"><span class="middle" style="width: 80px;">Doctor Name</span></th>
                                        <th class="kt-datatable__cell"><span class="middle" style="width: 80px;">Camera Name</span></th>
                                        <th class="kt-datatable__cell"><span class="middle" style="width: 80px;">Date and time</span></th>
                                        <th class="kt-datatable__cell"><span class="middle" style="width: 80px;">State</span></th>
                                        <th class="kt-datatable__cell"><span class="middle" style="width: 80px;">Actions</span></th>
                                    </tr>
                                </thead>
                                <tbody style="" class="kt-datatable__body">
                                    @foreach($apps as $key=>$order)
                                    @php
                                        if($order->status == 0){
                                            $status = "Waiting";
                                        }else{
                                            $status = "Active";
                                        }
                                    @endphp
                                    <tr data-row="{{$order->id}}" class="kt-datatable__row" style="left: 0px;">
                                        <td class="kt-datatable__cell--sorted kt-datatable__cell" data-field="AgentName"><span class="middle" style="width: 200px;">
                                                <div class="kt-user-card-v2">
                                                    <div class="kt-user-card-v2__details"> <span class="kt-user-card-v2__name middle" href="#">{{$order->id}}</span></div>
                                                </div>
                                            </span>
                                        </td>
                                        <td data-field="Country" class="kt-datatable__cell"><span class="middle" style="width: 80px;">{{$order->doctors->name}}</span></td>
                                        <td data-field="Country" class="kt-datatable__cell"><span class="middle" style="width: 80px;">{{isset($order->camera_id) ? $order->camera->name : 'N/A'}}</span></td>
                                        <td class="kt-datatable__cell" style=""><span class="middle" style="width: 80px;">{{$order->date}} {{$order->time}}</span></td>
                                        <td class="kt-datatable__cell" style="">@if($status == "Active")
                                            <span class="badge badge-primary middle">{{$status}}</span>
                                            @else
                                            <span class="badge badge-danger middle">{{$status}}</span>
                                            @endif</td>
                                        <td data-field="Actions" data-autohide-disabled="false" class="kt-datatable__cell"><span class="middle" style="overflow: visible; position: relative; width: 80px;">
                                                <div class="dropdown"> <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md"> <i class="flaticon-more-1"></i> </a>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <ul class="kt-nav">
                                                            <li class="kt-nav__item"> <a class="kt-nav__link" href="{{route('sassign', $order->id)}}"> <i class="kt-nav__link-icon flaticon2-contract"></i> <span class="kt-nav__link-text">Assign this appointment to me</span> </a> </li>
                                                            @if(!$order->order_count && Auth()->user()->is_admin)
                                                            <li class="kt-nav__item"> <a class="kt-nav__link" href="{{route('appointmentdelete', ["id" => $order->id])}}"> <i class="kt-nav__link-icon flaticon2-contract"></i> <span class="kt-nav__link-text">Delete appointment</span> </a> </li>
                                                            @endif
                                                        </ul>
                                                    </div>
                                                </div>
                                            </span></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                </div>
            </div>
        </div>
        <div class="kt-portlet">
            <div class="kt-portlet__body">
                <div class="kt-pagination kt-pagination--brand">
                    {{$apps->links()}}
                </div>
            </div>
        </div>
    </div>
@endsection
