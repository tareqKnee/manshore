@extends('layout/layout')

@section('head')
<title>View Tasks</title>
@endsection

@section('body-header')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                My tasks
            </h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
        </div>
    </div>
</div>
@endsection

@section('body')
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body kt-portlet__body--fit">
            <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" id="kt_apps_user_list_datatable" style="">
                <table class="kt-datatable__table" style="display: block;">
                    <thead class="kt-datatable__head">
                        <tr class="kt-datatable__row" style="left: 0px;">
                            <th class="kt-datatable__cell"><span style="width: 200px;">Orders ID</span></th>
                            <th class="kt-datatable__cell"><span style="width: 150px;">Patient Name</span></th>
                            <th class="kt-datatable__cell"><span style="width: 180px;">Doctor Name</span></th>
                            <th class="kt-datatable__cell"><span style="width: 120px;">Date created</span></th>
                            <th class="kt-datatable__cell"><span style="width: 120px;">State</span></th>
                            <th class="kt-datatable__cell"><span style="width: 80px;">Actions</span></th>
                        </tr>
                    </thead>
                    <tbody style="" class="kt-datatable__body">
                        @foreach($tasks as $key=>$order)
                        @php
                        if($order->made_by == 0){
                            $status = "Waiting";
                        }else{
                            $status = "Active";
                        }
                        @endphp
                        <tr data-row="{{$order->id}}" class="kt-datatable__row" style="left: 0px;">
                            <td class="kt-datatable__cell--sorted kt-datatable__cell" data-field="AgentName"><span style="width: 200px;">
                                    <div class="kt-user-card-v2">
                                        <div class="kt-user-card-v2__details"> <span class="kt-user-card-v2__name" href="#">{{$order->order_id}}</span></div>
                                    </div>
                                </span>
                            </td>
                            <td data-field="Country" class="kt-datatable__cell"><span style="width: 150px;">{{$order->patient_name}}</span></td>
                            <td data-field="Country" class="kt-datatable__cell"><span style="width: 180px;">{{$order->doctors->name}}</span></td>
                            <td class="kt-datatable__cell" style=""><span style="width: 120px;">{{$order->created_at->format('d/m/Y')}}</span></td>
                            <td class="kt-datatable__cell" style="">@if($status == "Active")
                                    <span style="width: 120px;" class="badge badge-primary">{{$status}}</span>
                                    @else
                                    <span style="width: 120px;" class="badge badge-danger">{{$status}}</span>
                                    @endif</td>
                            <td data-field="Actions" data-autohide-disabled="false" class="kt-datatable__cell"><span style="overflow: visible; position: relative; width: 80px;">
                                    <div class="dropdown"> <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md"> <i class="flaticon-more-1"></i> </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <ul class="kt-nav">
                                                    <li class="kt-nav__item"> <a class="kt-nav__link" href="{{route('OrderViewOnly', $order->id)}}"> <i class="kt-nav__link-icon flaticon2-contract"></i> <span class="kt-nav__link-text">View Order</span> </a> </li>
                                                <li class="kt-nav__item"> <a class="kt-nav__link" href="{{route('deMark', $order->id)}}"><i class="kt-nav__link-icon flaticon2-contract"></i> <span class="kt-nav__link-text">Mark as done</span> </a> </li>
                                            </ul>
                                        </div>
                                    </div>
                                </span></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="kt-portlet">
        <div class="kt-portlet__body">
            <div class="kt-pagination kt-pagination--brand">
                {{$tasks->links()}}
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    function sweetalert() {
        swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, mark it as done!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {

            if (result.value) {
                swalWithBootstrapButtons.fire(
                    'Marked as done!',
                    'Your task has been marked as done.',
                    'success'
                )
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                swalWithBootstrapButtons.fire(
                    'Cancelled',
                    'Your imaginary task is safe :)',
                    'error'
                )
            }
        })
    }
</script>
@endsection
