<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <!--page style css-->
    <link rel="stylesheet" href="/css/style.css">
    <title>{{$title}}</title>
  </head>
  <body>
    <div class="main-body w-100">
        <h2>{{$title}}</h2>
        <table class="table table-dark">
            <thead>
              <tr>
                <th class="tableheader"  scope="col">Doctor's Name</th>
                <th class="tableheader">Camera Name</th>
                <th class="tableheader">Date and time</th>
                <th class="tableheader" scope="col">status</th>
              </tr>
            </thead>
            <tbody>
                @foreach($orders as $order)
                @php
                if($order->status == 0){
                    $status = "Waiting";
                }else{
                    $status = "Active";
                }
            @endphp
              <tr>
                <td>{{$order->doctors->name}}</td>
                <td>{{isset($order->camera_id) ? $order->camera->name : 'N/A'}}</td>
                <td>{{$order->date}} {{$order->time}}</td>
                <td>
                    @if($status == "Active")
                    <div class="status pink"><span>{{$status}}</span></div>
                    @else
                    <div class="status active">
                        <span>{{$status}}</span>
                    </div>
                    @endif
                </td>
              </tr>
              @endforeach
            </tbody>
        </table>
        <div class="invisible-cases">
            <div>
                <span><img src="/images/info.png"></span>
                <span>{{$orders->total() - 7}}+  Invisible Cases</span>
            </div>
        </div>
    </div>
     <!-- Bootstrap JS -->
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
     <script src="/js/bootstrap.min.js"></script>
     <!--main js-->
        <script>
        var url =  window.location.href;
          $(document).ready(function() {
              setInterval(function() {
                 location.reload();
              },60000);
          });
        </script>
  </body>
</html>
