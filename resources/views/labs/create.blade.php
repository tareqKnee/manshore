@extends('layout/layout')

@section('head')
<title>Create lab</title>
@endsection

@section('body-header')
<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">
				New lab
			</h3>
			<span class="kt-subheader__separator kt-subheader__separator--v"></span>
			<div class="kt-subheader__group" id="kt_subheader_search">
				<span class="kt-subheader__desc" id="kt_subheader_total">
					Enter lab details and submit </span>
			</div>
		</div>
	</div>
</div>
<!-- end:: Content Head -->
@endsection

@section('body')

									<!--begin::Portlet-->
									<div class="kt-portlet">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Create lab
												</h3>
											</div>
										</div>

										<!--begin::Form-->
                                        <form class="kt-form" method="POST" action="{{route('labInsert')}}">
                                            @csrf
											<div class="kt-portlet__body">
												<div class="form-group row">
                                                    <label for="name" class="col-2 col-form-label">Lab name</label>
                                                    <div class="col-10">
                                                    <input type="text" class="form-control" id="name" name="name" placeholder="Enter the lab name" value="{{old('name')}}">
                                                    </div>
                                                    @if ($errors->has('name'))
                                                    <span class="help-block" style="color: red">{{ $errors->first('name') }}</span>
                                                    @endif
                                                </div>
                                                <div class="form-group row">
                                                    <label for="example-tel-input" class="col-2 col-form-label">Phone</label>
                                                    <div class="col-10">
                                                        <input class="form-control" type="tel" name="phone" id="example-tel-input" placeholder="Enter the phone name" value="{{old('phone')}}">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="example-address-input" class="col-2 col-form-label">Address</label>
                                                    <div class="col-10">
                                                        <input class="form-control" type="text" name="address" placeholder="Enter the address name" id="example-address-input" value="{{old('address')}}">
                                                    </div>
                                                </div>
											</div>
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<button type="submit" class="btn btn-primary">Submit</button>
													<button type="reset" class="btn btn-danger">Reset</button>
												</div>
											</div>
										</form>

										<!--end::Form-->
									</div>

<!-- end:: Content -->
@endsection
@section('script')
<script>
	$('select[name="position"]').on('change', function() {
		var selected = $(this).find('option:selected');
		var extra = selected.data('content');
		if (extra == 'B') {
			$('#TypeB').removeAttr('hidden')
		} else {
			$('#TypeB').prop('hidden', true)
		}
		console.log(extra)
	})
</script>
@stop
