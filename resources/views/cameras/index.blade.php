@extends('layout/layout')

@section('head')
<title>View Cameras</title>
@endsection

@section('body-header')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                    Cameras
            </h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__group" id="kt_subheader_search">
                <span class="kt-subheader__desc" id="kt_subheader_total">
                    {{$cameras->total()}} Total </span>
        </div>
    </div>
</div>
</div>
@endsection

@section('body')
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="text-right mb-3">
        <a class="btn btn-brand btn-elevate" href="{{route('cameraCreate')}}"><i class="fa fa-plus mx-2"></i>Add a camera</a>
    </div>
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body kt-portlet__body--fit">
            <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" id="kt_apps_user_list_datatable" style="">
                <table class="kt-datatable__table" style="display: block;">
                    <thead class="kt-datatable__head">
                        <tr class="kt-datatable__row" style="left: 0px;">
                            <th class="kt-datatable__cell"><span style="width: 200px;">Camera ID</span></th>
                            <th class="kt-datatable__cell"><span class="middle" style="width: 150px;">Camera Name</span></th>
                            <th class="kt-datatable__cell"><span class="middle" style="width: 80px;">Actions</span></th>
                        </tr>
                    </thead>
                    <tbody style="" class="kt-datatable__body">
                        @foreach($cameras as $key=>$camera)
                        <tr data-row="{{$camera->id}}" class="kt-datatable__row" style="left: 0px;">
                            <td class="kt-datatable__cell--sorted kt-datatable__cell" data-field="AgentName"><span style="width: 200px;">
                                    <div class="kt-user-card-v2">
                                        <div class="kt-user-card-v2__details"> <span class="kt-user-card-v2__name middle" href="#">{{$camera->id}}</span></div>
                                    </div>
                                </span>
                            </td>
                            <td data-field="Country" class="kt-datatable__cell"><span class="middle" style="width: 150px;">{{$camera->name}}</span></td>
                            <td data-field="Actions" data-autohide-disabled="false" class="kt-datatable__cell"><span class="middle" style="overflow: visible; position: relative; width: 80px;">
                                <div class="dropdown"> <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md"> <i class="flaticon-more-1"></i> </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <ul class="kt-nav">
                                            <li class="kt-nav__item"> <a class="kt-nav__link" href="{{route('cameraEdit', $camera->id)}}"> <i class="kt-nav__link-icon flaticon2-contract"></i> <span class="kt-nav__link-text">Edit</span> </a> </li>
                                        </ul>
                                    </div>
                                </div>
                            </span></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="kt-portlet">
        <div class="kt-portlet__body">
            <div class="kt-pagination kt-pagination--brand">
                {{$cameras->links()}}
            </div>
        </div>
    </div>
</div>
@endsection

