@extends('layout/layout')

@section('head')
<title>Create a camera</title>
@endsection

@section('body-header')
<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">
				New camera
			</h3>
			<span class="kt-subheader__separator kt-subheader__separator--v"></span>
			<div class="kt-subheader__group" id="kt_subheader_search">
				<span class="kt-subheader__desc" id="kt_subheader_total">
					Enter camera details and submit </span>
			</div>
		</div>
	</div>
</div>
<!-- end:: Content Head -->
@endsection

@section('body')

									<!--begin::Portlet-->
									<div class="kt-portlet">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Create a camera
												</h3>
											</div>
										</div>

										<!--begin::Form-->
                                        <form class="kt-form" method="POST" action="{{route('cameraInsert')}}">
                                            @csrf
											<div class="kt-portlet__body">
                                                    <label for="name">Camera name: </label>
                                                    <input id="name" class="form-control input-group-lg reg_name" type="text" name="name" value="{{old('name')}}">
											</div>
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<button type="submit" class="btn btn-primary">Submit</button>
													<button type="reset" class="btn btn-danger">Reset</button>
												</div>
											</div>
										</form>

										<!--end::Form-->
									</div>

<!-- end:: Content -->
@endsection
@section('script')
<script>
	$('select[name="position"]').on('change', function() {
		var selected = $(this).find('option:selected');
		var extra = selected.data('content');
		if (extra == 'B') {
			$('#TypeB').removeAttr('hidden')
		} else {
			$('#TypeB').prop('hidden', true)
		}
		console.log(extra)
	})
</script>
@stop
