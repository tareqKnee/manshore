@include('../layout/header')

@if(session('success'))
<div class="alert alert-success" role="alert">
    <div class="alert-icon"><i class="flaticon-success"></i></div>
    <div class="alert-text">{{session("success")}}</div>
</div>
@else
<div class="feedback-container">
    <div class="feedback-aria">
        <form action="{{route('feedbackInsert')}}" method="POST">
            @csrf
            <input type="hidden" name="order_id" value="{{$order->id}}">
            <input type="hidden" name="doctor_id" value="{{$order->doctor_id}}">
            <input type="hidden" name="patient_name" value="{{$order->patient_name}}">
            <div class="text-center mb-3">
                <img src="../../uploads/login-logo.png" class="feedback-logo">
            </div>
            <div class="mb-3 mt-5">
                <h3 class="text-right">اسم الدكتور : {{$order->doctors->name}}</h3>
                <h3 class="text-right">اسم المريض : {{$order->patient_name}}</h3>
            </div>
            <div class="mb-1">
                <ul class="feedback-list">
                    <li>
                        <button type="button" class="feedback-btn happy">
                            <i class="icon-happy-01-01"></i>
                        </button>
                    </li>
                    <li>
                        <button type="button" class="feedback-btn neutral">
                            <i class="icon-neutral-03-01"></i>
                        </button>
                    </li>
                    <li>
                        <button type="button" class="feedback-btn unhappy">
                            <i class="icon-unhappy-02-01"></i>
                        </button>
                    </li>
                </ul>
                <div class="text-center">
                    <input id="happy" class="rate-radio" required type="radio" name="rate" value="2">
                    <input id="neutral" class="rate-radio" required type="radio" name="rate" value="1">
                    <input id="unhappy" class="rate-radio" required type="radio" name="rate" value="0">
                </div>
            </div>
            <div class="mb-3 mt-2">
                <h3 class="text-right">ملاحظات</h3>
                <textarea rows="4" class="form-control mb-4" placeholder="أكتب ملاحظاتك هنا" name="note"></textarea>
                <input type="submit" value="أرسل" class="px-5 btn btn-brand btn-elevate" />
            </div>
        </form>
    </div>
</div>
@endif
@include('../layout/scripts')
