@extends('layout/layout')

@section('head')
<title>View Orders' Feedback</title>
@endsection

@section('body-header')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
               Orders' Feedback
            </h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__group" id="kt_subheader_search">
                <span class="kt-subheader__desc" id="kt_subheader_total">
                    {{$feeds->total()}} Total </span>
            </div>
        </div>
    </div>
</div>
@endsection

@section('body')
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    @if(session('success'))
    <div class="alert alert-success" role="alert">
        <div class="alert-icon"><i class="flaticon-success"></i></div>
        <div class="alert-text">{{session("success")}}</div>
    </div>
    @endif
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body kt-portlet__body--fit">
            <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" id="kt_apps_user_list_datatable" style="">
                <table class="kt-datatable__table" style="display: block;">
                    <thead class="kt-datatable__head">
                        <tr class="kt-datatable__row" style="left: 0px;">
                            <th class="kt-datatable__cell"><span style="width: 200px;">Order ID</span></th>
                            <th class="kt-datatable__cell"><span style="width: 150px;">Doctor Name</span></th>
                            <th class="kt-datatable__cell"><span style="width: 80px;">Patient Name</span></th>
                            <th class="kt-datatable__cell"><span style="width: 80px;">Rate Name</span></th>
                            <th class="kt-datatable__cell"><span style="width: 200px;">Note</span></th>
                        </tr>
                    </thead>
                    <tbody style="" class="kt-datatable__body">
                        @foreach($feeds as $key=>$feed)
                        <tr data-row="{{$feed->id}}" class="kt-datatable__row" style="left: 0px;">
                            <td class="kt-datatable__cell--sorted kt-datatable__cell" data-field="AgentName"><span style="width: 200px;">
                                @if(isset($feed->order->order_id))
                                    <div class="kt-user-card-v2">
                                        <div class="kt-user-card-v2__details"> <span class="kt-user-card-v2__name" href="#">{{$feed->order->order_id}}</span></div>
                                    </div>
                                @else
                                    <div class="kt-user-card-v2">
                                        <div class="kt-user-card-v2__details"> <span class="kt-user-card-v2__name" href="#">N/A</span></div>
                                    </div>
                                @endif
                                </span>
                            </td>
                            <td data-field="Country" class="kt-datatable__cell"><span style="width: 150px;">{{$feed->doctor->name}}</span></td>
                            <td data-field="Country" class="kt-datatable__cell"><span style="width: 80px;">{{$feed->patient_name}}</span></td>
                            <td data-field="Country" class="kt-datatable__cell"><span style="width: 80px;">@if($feed->rate == 0)
                                    Unhappy
                                    @elseif($feed->rate == 1)
                                     Okay
                                    @else
                                      Happy
                                    @endif</span></td>
                            <td data-field="Country" class="kt-datatable__cell"><span style="width: 200px;">{{$feed->note}}</span></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="kt-portlet">
        <div class="kt-portlet__body">
            <div class="kt-pagination kt-pagination--brand">
                {{$feeds->links()}}
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    function sweetalert() {
        swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {

            if (result.value) {
                swalWithBootstrapButtons.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                )
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                swalWithBootstrapButtons.fire(
                    'Cancelled',
                    'Your imaginary file is safe :)',
                    'error'
                )
            }
        })
    }
</script>
@endsection
