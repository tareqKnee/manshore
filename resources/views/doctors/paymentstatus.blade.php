@extends('layout/layout')

@section('head')
<title>Payments by doctor</title>
@endsection

@section('body-header')
<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">
				Payments by doctor
			</h3>
			<span class="kt-subheader__separator kt-subheader__separator--v"></span>
			<div class="kt-subheader__group" id="kt_subheader_search">
				<span class="kt-subheader__desc" id="kt_subheader_total">
					Enter the details and submit </span>
			</div>
		</div>
	</div>
</div>
<!-- end:: Content Head -->
@endsection

@section('body')
@php
if(session('data')){
    $data = session('data');
} else {
    $data = null;
}
@endphp
									<!--begin::Portlet-->
									<div class="kt-portlet">

										<!--begin::Form-->
                                        <form class="kt-form" method="POST" action="{{route('doctorPaymentsPost')}}">
                                            @csrf
											<div class="kt-portlet__body">
												<div class="form-group">
													<label>From date</label>
                                                    <input type="date" class="form-control" name="from" value="{{$data ? $data['from'] : ''}}">
                                                    @if ($errors->has('from'))
                                                    <span class="help-block" style="color: red">{{ $errors->first('from') }}</span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
													<label>To date</label>
                                                    <input type="date" class="form-control" name="to" value="{{$data ? $data['to'] : ''}}">
                                                    @if ($errors->has('to'))
                                                    <span class="help-block" style="color: red">{{ $errors->first('to') }}</span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
													<label>Doctor name</label>
                                                                    <select class="form-control" id="doc" name="doc">
                                                                        <option selected value="all">All doctors</option>
                                                                        @foreach($doctors as $doc)
                                                                            <option value="{{$doc->id}}" {{$data && $data['doc'] == $doc->id ? 'selected' : ''}}>{{$doc->name}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    @if ($errors->has('doc'))
                                                                    <span class="help-block" style="color: red">{{ $errors->first('doc') }}</span>
                                                                    @endif
                                                        </div>
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<button type="submit" class="btn btn-primary">Submit</button>
													<button type="reset" class="btn btn-danger">Reset</button>
												</div>
											</div>
										</form>

                                        @if(Session::get('status'))
                                        @php
                                            $mat = Session::get('status');
                                        @endphp
                                        <strong><p> Amount of money collected : {{$mat->sum('amount')}} </p></strong>
                                        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                                            <div class="kt-portlet kt-portlet--mobile">
                                                <div class="kt-portlet__body kt-portlet__body--fit">
                                                    <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" id="kt_apps_user_list_datatable" style="">
                                                        <table class="kt-datatable__table" style="display: block;">
                                                            <thead class="kt-datatable__head">
                                                                <tr class="kt-datatable__row" style="left: 0px;">
                                                                    <th class="kt-datatable__cell"><span class="middle" style="width: 200px; margin: auto; text-align: center">Doctor Name</span></th>
                                                                    <th class="kt-datatable__cell"><span class="middle" style="width: 200px; margin: auto; text-align: center">Note</span></th>
                                                                    <th class="kt-datatable__cell"><span class="middle" style="width: 150px; margin: auto; text-align: center">Collector</span></th>
                                                                    <th class="kt-datatable__cell"><span class="middle" style="width: 80px; margin: auto; text-align: center">Amount</span></th>
                                                                    <th class="kt-datatable__cell"><span class="middle" style="width: 80px; margin: auto; text-align: center">Date created</span></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody style="" class="kt-datatable__body">
                                                                @foreach($mat as $key=>$order)
                                                                <tr data-row="{{$order->id}}" class="kt-datatable__row" style="left: 0px;">
                                                                    <td data-field="Country" class="kt-datatable__cell"><span style="width: 200px; margin: auto; text-align: center">{{isset($order->doctors->name) ? $order->doctors->name : $order->doctors->name}}</span></td>
                                                                    <td data-field="Country" class="kt-datatable__cell"><span style="width: 200px; margin: auto; text-align: center">{{$order->notes}}</span></td>
                                                                    <td data-field="Country" class="kt-datatable__cell"><span style="width: 150px; margin: auto; text-align: center">{{$order->users->first_name}} {{$order->users->last_name}}</span></td>
                                                                    <td class="kt-datatable__cell" style=""><span style="width: 80px; margin: auto; text-align: center">{{$order->amount}}</span></td>
                                                                    <td class="kt-datatable__cell" style=""><span style="width: 80px; margin: auto; text-align: center">{{$order->created_at->format('d/m/Y')}}</span></td>
                                                                </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        @endif
										<!--end::Form-->
									</div>

<!-- end:: Content -->
@endsection
@section('script')
<script>
	$('select[name="position"]').on('change', function() {
		var selected = $(this).find('option:selected');
		var extra = selected.data('content');
		if (extra == 'B') {
			$('#TypeB').removeAttr('hidden')
		} else {
			$('#TypeB').prop('hidden', true)
		}
		console.log(extra)
	})
</script>
@stop
