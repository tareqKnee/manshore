@extends('layout/layout')

@section('head')
<title>Cases value</title>
@endsection

@section('body-header')
<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">
				Cases value
			</h3>
			<span class="kt-subheader__separator kt-subheader__separator--v"></span>
			<div class="kt-subheader__group" id="kt_subheader_search">
				<span class="kt-subheader__desc" id="kt_subheader_total">
					Enter the details and submit </span>
			</div>
		</div>
	</div>
</div>
<!-- end:: Content Head -->
@endsection

@section('body')
									<!--begin::Portlet-->
									<div class="kt-portlet">

										<!--begin::Form-->
                                        <form class="kt-form" method="GET" action="{{route('CasesValue')}}">
											<div class="kt-portlet__body">
												<div class="form-group">
													<label>From date</label>
                                                    <input type="date" class="form-control" name="from" value="{{isset($_GET['from']) ? $_GET['from'] : ''}}">
                                                    @if ($errors->has('from'))
                                                    <span class="help-block" style="color: red">{{ $errors->first('from') }}</span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
													<label>To date</label>
                                                    <input type="date" class="form-control" name="to" value="{{isset($_GET['to']) ? $_GET['to'] : ''}}">
                                                    @if ($errors->has('to'))
                                                    <span class="help-block" style="color: red">{{ $errors->first('to') }}</span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
													<label>Doctor name</label>
                                                                    <select class="form-control" id="doc" name="doc">
                                                                        <option selected value="all">All doctors</option>
                                                                        @foreach($doctors as $doc)
                                                                            <option value="{{$doc->id}}" {{isset($_GET['doc']) && $_GET['doc'] == $doc->id ? 'selected' : ''}}>{{$doc->name}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    @if ($errors->has('doc'))
                                                                    <span class="help-block" style="color: red">{{ $errors->first('doc') }}</span>
                                                                    @endif
                                                        </div>
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<button type="submit" class="btn btn-primary">Submit</button>
													<button type="reset" class="btn btn-danger">Reset</button>
												</div>
											</div>
										</form>

                                        @if($orders)
                                        <strong><p> Total value : {{$orders}} </p></strong>
                                        @endif
										<!--end::Form-->
									</div>

<!-- end:: Content -->
@endsection
@section('script')
<script>
	$('select[name="position"]').on('change', function() {
		var selected = $(this).find('option:selected');
		var extra = selected.data('content');
		if (extra == 'B') {
			$('#TypeB').removeAttr('hidden')
		} else {
			$('#TypeB').prop('hidden', true)
		}
		console.log(extra)
	})
</script>
@stop
