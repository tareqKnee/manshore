@extends('layout/layout')

@section('head')
<title>View Doctor Account Statement</title>
@endsection

@section('body-header')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Account Statement
            </h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__group" id="kt_subheader_search">
                <span class="kt-subheader__desc" id="kt_subheader_total">
                    {{$stats->count()}} Total </span>
                    <form class="kt-margin-l-20 row" id="kt_subheader_search_form" method="GET" action="{{route('DoctorStatement')}}">
                        <input hidden name="id" value="{{$id}}">
                        <div class="kt-subheader__search" style="">
                            <input type="date" class="form-control" name="from" value="{{$from}}">
                        </div>
                        <div class="kt-subheader__search" style="">
                            <input type="date" class="form-control" name="to" value="{{$to}}">
                        </div>
                        <button type="submit" class="btn btn-primary">Search</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('body')
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body kt-portlet__body--fit">
            <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" id="kt_apps_user_list_datatable" style="">
                @if($before !== null)
                <strong><span class="middle" style="width: 80px;">Balance before {{$from}} : {{$before}} JOD</span></strong>
                @endif
                <table class="kt-datatable__table" style="display: block;">
                    <thead class="kt-datatable__head">
                        <tr class="kt-datatable__row" style="left: 0px;">
                            <th class="kt-datatable__cell"><span style="width: 80px;">Created at</span></th>
                            <th class="kt-datatable__cell"><span style="width: 80px;">Patient name</span></th>
                            <th class="kt-datatable__cell"><span style="width: 80px;">Credit</span></th>
                            <th class="kt-datatable__cell"><span style="width: 80px;">Debit</span></th>
                            <th class="kt-datatable__cell"><span style="width: 80px;">Balance</span></th>
                        </tr>
                    </thead>
                    <tbody style="" class="kt-datatable__body">
                        @php
                            $sum = 0;
                            $pays = 0;
                        @endphp
                        @foreach($stats as $key=>$user)
                        <tr data-row="{{$user->id}}" class="kt-datatable__row" style="left: 0px;">
                                <td data-field="Country" class="kt-datatable__cell"><span style="width: 80px;">{{$user->created_at->format('d/m/Y')}}</span></td>
                            <td class="kt-datatable__cell--sorted kt-datatable__cell" data-field="AgentName"><span style="width: 80px;">
                                    <div class="kt-user-card-v2">
                                        <div class="kt-user-card-v2__details"> <span class="kt-user-card-v2__name" href="#">{{$user->patient_name}}</span></div>
                                    </div>
                                </span>
                            </td>
                            <td data-field="Country" class="kt-datatable__cell"><span style="width: 80px;">{{$user->credit}}</span></td>
                            <td class="kt-datatable__cell" style=""><span style="width: 80px;">{{$user->debit}}</span></td>
                            @php
                                $sum += $user->debit;
                                $pays += $user->credit;
                            @endphp
                            <td data-field="Type" data-autohide-disabled="false" class="kt-datatable__cell"><span style="width: 80px;"><span class="kt-font-bold kt-font-primary">{{$user->credit ? $user->balance - $user->credit : $user->balance}}</span></span></td>
                        </tr>
                        @endforeach
                        <tr class="kt-datatable__row" style="left: 0px;">
                            <td data-field="Country" class="kt-datatable__cell"><span style="width: 80px;">Debit Total : {{$sum}}</span></td>
                            <td data-field="Country" class="kt-datatable__cell"><span style="width: 80px;">Credit Total : {{$pays}}</span></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <strong class="middle"><p> Amount : {{$sum - $pays}} </p></strong>
</div>
@endsection
@section('scripts')
<script>
        $("#amount").on('change', function(){
        $("#kt_subheader_search_form").submit();
    });
    function sweetalert() {
        swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {

            if (result.value) {
                swalWithBootstrapButtons.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                )
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                swalWithBootstrapButtons.fire(
                    'Cancelled',
                    'Your imaginary file is safe :)',
                    'error'
                )
            }
        })
    }
</script>
@endsection
