@extends('layout/layout')

@section('head')
<title>View Invoices</title>
@endsection

@section('body-header')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                    Invoices
            </h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__group" id="kt_subheader_search">
                <span class="kt-subheader__desc" id="kt_subheader_total">
                    {{$total->total()}} Total </span>
            </div>
        </div>
    </div>
</div>
@endsection

@section('body')
@php

@endphp
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body kt-portlet__body--fit">
            <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" id="kt_apps_user_list_datatable" style="">
                <table class="kt-datatable__table" style="display: block;">
                    <thead class="kt-datatable__head">
                        <tr class="kt-datatable__row" style="left: 0px;">
                            <th class="kt-datatable__cell"><span class="middle" style="width: 200px; margin: auto; text-align: center">Doctor Name</span></th>
                            <th class="kt-datatable__cell"><span class="middle" style="width: 200px; margin: auto; text-align: center">Doctor Current Balance</span></th>
                            <th class="kt-datatable__cell"><span class="middle" style="width: 150px; margin: auto; text-align: center">Case ID</span></th>
                            <th class="kt-datatable__cell"><span class="middle" style="width: 80px; margin: auto; text-align: center">Amount</span></th>
                            <th class="kt-datatable__cell"><span class="middle" style="width: 80px; margin: auto; text-align: center">Date created</span></th>
                        </tr>
                    </thead>
                    <tbody style="" class="kt-datatable__body">
                        @foreach($total as $key=>$order)
                        <tr data-row="{{$order->id}}" class="kt-datatable__row" style="left: 0px;">
                            <td data-field="Country" class="kt-datatable__cell"><span style="width: 150px; margin: auto; text-align: center">{{isset($order->doctor->name) ? $order->doctor->name : $order->doctors->name}}</span></td>
                            <td data-field="Country" class="kt-datatable__cell"><span style="width: 150px; margin: auto; text-align: center">{{isset($order->doctor->balance) ? $order->doctor->balance : $order->doctors->balance}}</span></td>
                            <td data-field="Country" class="kt-datatable__cell"><span style="width: 150px; margin: auto; text-align: center">{{isset($order->order->order_id) ? $order->order->order_id : 'N/A'}}</span></td>
                            <td data-field="Country" class="kt-datatable__cell"><span style="width: 80; margin: auto; text-align: center">{{$order->amount}}</span></td>
                            <td class="kt-datatable__cell" style=""><span style="width: 80px; margin: auto; text-align: center">{{$order->created_at->format('d/m/Y')}}</span></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
@endsection
@section('scripts')
<script>
    function sweetalert() {
        swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {

            if (result.value) {
                swalWithBootstrapButtons.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                )
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                swalWithBootstrapButtons.fire(
                    'Cancelled',
                    'Your imaginary file is safe :)',
                    'error'
                )
            }
        })
    }
</script>
@endsection
