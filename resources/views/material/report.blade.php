@extends('layout/layout')

@section('head')
<title>Materials report</title>
@endsection
@section('body-header')
<div class="kt-subheader   kt-grid__item" id="kt_subheader" style="height:auto">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <div class="kt-subheader__group" id="kt_subheader_search">
                <h3 class="kt-subheader__title">
                    Materials report
                </h3>
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                <span class="kt-subheader__desc" id="kt_subheader_total">
                    {{$orders->total()}} Total, </span>
                    <span class="kt-subheader__desc" id="kt_subheader_total">
                        Total units: {{$total_units}}</span>
            </div>
        </div>
    </div>
</div>
@endsection

@section('body')
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__body">
                <form id="kt_subheader_search_form" class="row" method="GET" action="{{route('materialreportTool')}}">
                        <div class="kt-subheader__search" style="margin-left: 20px;">
                            <input type="date" class="form-control" name="from" value="{{$from}}">
                        </div>
                        <div class="kt-subheader__search" style="margin-left: 20px;">
                            <input type="date" class="form-control" name="to" value="{{$to}}">
                        </div>
                        <div class="kt-subheader__search" style="margin-left: 20px;">
                            <select class="form-control" id="type" name="type">
                                <option selected value="all">All types</option>
                                @foreach($types as $t)
                                    <option value="{{$t->id}}" {{$type == $t->id ? 'selected' : ''}}>{{$t->name}}</option>
                                @endforeach
                            </select>
                            </div>
                        <div class="kt-subheader__search" style="margin-left: 20px;">
                        <select class="form-control selectpicker" multiple id="doctor" name="doctor[]">
                            <option selected value="all">All doctors</option>
                            @foreach($doctors as $doc)
                                <option value="{{$doc->id}}" {{$doctor == $doc->id ? 'selected' : ''}}>{{$doc->name}}</option>
                            @endforeach
                        </select>
                        </div>
                        <div class="kt-subheader__search" style="margin-left: 20px;">
                        <select class="form-control selectpicker" multiple id="material" name="material[]">
                            <option selected value="all">All materials</option>
                            @foreach($materials as $mat)
                                <option value="{{$mat->id}}" {{$material == $mat->id ? 'selected' : ''}}>{{$mat->name}}</option>
                            @endforeach
                        </select>
                        </div>
                    <button type="submit" class="btn btn-primary">Search</button>
                </form>
            </div>
        </div>
    </div>
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body kt-portlet__body--fit">
            <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" id="kt_apps_user_list_datatable" style="">
                <table class="kt-datatable__table" style="display: block;">
                    <thead class="kt-datatable__head">
                        <tr class="kt-datatable__row" style="left: 0px;">
                            <th class="kt-datatable__cell"><span class="middle" style="width: 150px;">Patient Name</span></th>
                            <th class="kt-datatable__cell"><span class="middle" style="width: 150px;">Doctor Name</span></th>
                            <th class="kt-datatable__cell"><span class="middle" style="width: 80px;">Job Type</span></th>
                            <th class="kt-datatable__cell"><span class="middle" style="width: 80px;">Material</span></th>
                            <th class="kt-datatable__cell"><span class="middle" style="width: 80px;">Total units</span></th>
                        </tr>
                    </thead>
                    <tbody style="" class="kt-datatable__body">
                        @foreach($orders as $key=>$order)
                        @php
                            $total = explode(',',$order->unit_num);
                        @endphp
                        <tr data-row="{{$order->id}}" class="kt-datatable__row" style="left: 0px;">
                            <td data-field="Country" class="kt-datatable__cell"><span class="middle" style="width: 150px;">{{$order->order->patient_name}}</span></td>
                            <td data-field="Country" class="kt-datatable__cell"><span class="middle" style="width: 150px;">{{$order->order->doctors->name}}</span></td>
                            <td data-field="Country" class="kt-datatable__cell"><span class="middle" style="width: 80px;"></span></td>
                            <td class="kt-datatable__cell"><span class="middle" style="width: 80px;">{{$order->material->name}}</span></td>
                            <td class="kt-datatable__cell"><span class="middle" style="width: 80px;">{{count($total)}}</span></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    <div class="kt-portlet">
        <div class="kt-portlet__body">
            <div class="kt-pagination kt-pagination--brand">
                {{$orders->links()}}
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    function sweetalert() {
        swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {

            if (result.value) {
                swalWithBootstrapButtons.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                )
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                swalWithBootstrapButtons.fire(
                    'Cancelled',
                    'Your imaginary file is safe :)',
                    'error'
                )
            }
        })
    }
</script>
@endsection
