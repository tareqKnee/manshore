@extends('layout/layout')

@section('head')
<title>Cases by employee</title>
@endsection

@section('body-header')
<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">
				Cases by employee
			</h3>
			<span class="kt-subheader__separator kt-subheader__separator--v"></span>
			<div class="kt-subheader__group" id="kt_subheader_search">
				<span class="kt-subheader__desc" id="kt_subheader_total">
					Enter the details and submit </span>
			</div>
		</div>
	</div>
</div>
<!-- end:: Content Head -->
@endsection

@section('body')
@php
if(session('data')){
    $data = session('data');
} else {
    $data = null;
}
@endphp
									<!--begin::Portlet-->
									<div class="kt-portlet">

										<!--begin::Form-->
                                        <form class="kt-form" method="POST" action="{{route('PostemployeesStatus')}}">
                                            @csrf
											<div class="kt-portlet__body">
												<div class="form-group">
													<label>From date</label>
                                                    <input type="date" class="form-control" name="from" value="{{$data ? $data['from'] : ''}}">
                                                    @if ($errors->has('from'))
                                                    <span class="help-block" style="color: red">{{ $errors->first('from') }}</span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
													<label>To date</label>
                                                    <input type="date" class="form-control" name="to" value="{{$data ? $data['to'] : ''}}">
                                                    @if ($errors->has('to'))
                                                    <span class="help-block" style="color: red">{{ $errors->first('to') }}</span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
													<label>Stage</label>
                                                    <select class="form-control" id="CS" name="CS">
                                                        <option value="0" {{$data && $data['CS'] == 0? 'selected' : ''}}>Scan</option>
                                                        <option value="1" {{$data && $data['CS'] == 1? 'selected' : ''}}>Design</option>
                                                        <option value="2" {{$data && $data['CS'] == 2? 'selected' : ''}}>Milling</option>
                                                        <option value="3" {{$data && $data['CS'] == 3? 'selected' : ''}}>Furnace</option>
                                                        <option value="4" {{$data && $data['CS'] == 4? 'selected' : ''}}>Finish and building up</option>
                                                        <option value="5" {{$data && $data['CS'] == 5? 'selected' : ''}}>Quality</option>
                                                        <option value="6" {{$data && $data['CS'] == 6? 'selected' : ''}}>Delivery</option>
                                                    </select>
                                                    @if ($errors->has('CS'))
                                                    <span class="help-block" style="color: red">{{ $errors->first('CS') }}</span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
													<label>Employee name</label>
                                                                    <select class="form-control" id="emp" name="emp">
                                                                        @foreach($users as $doc)
                                                                            <option value="{{$doc->id}}" {{$data && $data['emp'] == $doc->id ? 'selected' : ''}}>{{$doc->first_name}} {{$doc->last_name}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    @if ($errors->has('emp'))
                                                                    <span class="help-block" style="color: red">{{ $errors->first('emp') }}</span>
                                                                    @endif
                                                        </div>
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<button type="submit" class="btn btn-primary">Submit</button>
													<button type="reset" class="btn btn-danger">Reset</button>
												</div>
											</div>
										</form>

                                        @if(Session::get('status'))
                                        @php
                                            $status = Session::get('status');
                                        @endphp
                                        @if($status == -1)
                                        <strong><p> # of tasks done : 0 </p></strong>
                                        @else
                                        <strong><p> # of tasks done : {{$status}} </p></strong>
                                        @endif
                                        @endif
										<!--end::Form-->
									</div>

<!-- end:: Content -->
@endsection
@section('script')
<script>
	$('select[name="position"]').on('change', function() {
		var selected = $(this).find('option:selected');
		var extra = selected.data('content');
		if (extra == 'B') {
			$('#TypeB').removeAttr('hidden')
		} else {
			$('#TypeB').prop('hidden', true)
		}
		console.log(extra)
	})
</script>
@stop
