@extends('layout/layout')

@section('head')
<title>Create User</title>
@endsection

@section('body-header')
<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">
				New User
			</h3>
			<span class="kt-subheader__separator kt-subheader__separator--v"></span>
			<div class="kt-subheader__group" id="kt_subheader_search">
				<span class="kt-subheader__desc" id="kt_subheader_total">
					Enter user details and submit </span>
			</div>
		</div>
	</div>
</div>
<!-- end:: Content Head -->
@endsection

@section('body')

									<!--begin::Portlet-->
									<div class="kt-portlet">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Create User
												</h3>
											</div>
										</div>

										<!--begin::Form-->
                                        <form class="kt-form" method="POST" action="{{route('usersInsert')}}">
                                            @csrf
											<div class="kt-portlet__body">
												<div class="form-group">
													<label>User first name</label>
                                                    <input type="text" class="form-control" name="first_name" placeholder="Enter the first name" value="{{old('first_name')}}">
                                                    @if ($errors->has('first_name'))
                                                    <span class="help-block" style="color: red">{{ $errors->first('first_name') }}</span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
                                                        <label>User last name</label>
                                                        <input type="text" class="form-control" name="last_name" placeholder="Enter the first name" value="{{old('last_name')}}">
                                                        @if ($errors->has('last_name'))
                                                        <span class="help-block" style="color: red">{{ $errors->first('last_name') }}</span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group">
                                                            <label>Username</label>
                                                            <input type="text" class="form-control" name="username" placeholder="Enter the username" value="{{old('username')}}">
                                                            @if ($errors->has('username'))
                                                            <span class="help-block" style="color: red">{{ $errors->first('username') }}</span>
                                                            @endif
                                                    </div>
                                                    <div class="form-group row">
                                                            <label for="example-tel-input" class="col-2 col-form-label">User Phone Number</label>
                                                            <div class="col-10">
                                                                <input class="form-control" type="tel" name="phone" id="example-tel-input" value="{{old('phone')}}">
                                                            </div>
                                                        </div>
												<div class="form-group">
													<label>User Email address</label>
                                                    <input type="email" class="form-control" name="email" aria-describedby="emailHelp" placeholder="Enter email" value="{{old('email')}}">
                                                    @if ($errors->has('email'))
                                                    <span class="help-block" style="color: red">{{ $errors->first('email') }}</span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
													<label for="is_admin">Admin</label>
                                                    <input type="checkbox" class="form-control" id="is_admin" name="is_admin">
                                                    @if ($errors->has('is_admin'))
                                                    <span class="help-block" style="color: red">{{ $errors->first('is_admin') }}</span>
                                                    @endif
                                                </div>
												<div class="form-group">
													<label for="exampleInputPassword1">Password</label>
                                                    <input type="password" class="form-control" name="password" placeholder="Password">
                                                    @if ($errors->has('password'))
                                                    <span class="help-block" style="color: red">{{ $errors->first('password') }}</span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
													<label for="exampleInputPassword1">Confirm Password</label>
                                                    <input type="password" class="form-control" name="password_confirmation" placeholder="Password">
                                                    @if ($errors->has('password_confirmation'))
                                                    <span class="help-block" style="color: red">{{ $errors->first('password_confirmation') }}</span>
                                                    @endif
                                                </div>
                                                <div class="form-group" id="disable">
                                                        <label for="Permission">Permission</label>
                                                        <select class="form-control selectpicker" id="Permission" multiple name="permission[]">
                                                                @foreach($permissions as $perm)
                                                                <option value="{{$perm->id}}">{{$perm->name}}</option>
                                                                @endforeach
                                                            </select>
                                                    </div>
											</div>
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<button type="submit" class="btn btn-primary">Submit</button>
													<button type="reset" class="btn btn-danger">Reset</button>
												</div>
											</div>
										</form>

										<!--end::Form-->
									</div>

<!-- end:: Content -->
@endsection
@section('scripts')
<script>
    $('#is_admin').on('change', function() {
        if(this.checked){
            $('#Permission').attr('disabled', true);
            $('#disable').css('visibility', 'hidden');
        } else {
            $('#Permission').attr('disabled', false);
            $('#disable').css('visibility', 'visible');
        }
    });
	$('select[name="position"]').on('change', function() {
		var selected = $(this).find('option:selected');
		var extra = selected.data('content');
		if (extra == 'B') {
			$('#TypeB').removeAttr('hidden')
		} else {
			$('#TypeB').prop('hidden', true)
		}
		console.log(extra)
	})
</script>
@stop
