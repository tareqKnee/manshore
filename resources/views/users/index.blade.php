@extends('layout/layout')

@section('head')
<title>View Users</title>
@endsection

@section('body-header')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Users
            </h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__group" id="kt_subheader_search">
                <span class="kt-subheader__desc" id="kt_subheader_total">
                    {{$users->total()}} Total </span>
                <form class="kt-margin-l-20" id="kt_subheader_search_form" method="GET" action="{{route('UsersIndex')}}">
                        <strong style="color:black"> Sort by: </strong>
                        <div class="kt-subheader__search" style="">
                                    <select name="status" id="status" class="form-control">
                                        <option value="1" {{$status == 1 ? 'selected' : ''}}>Enabled</option>
                                        <option value="disabled" {{$status == 'disabled' ? 'selected' : ''}}>Disabled</option>
                                    </select>
                                </div>
                    <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                            <span class="kt-input-icon__icon kt-input-icon__icon--right">
                                    <span>
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24" />
                                                <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                                <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero" />
                                            </g>
                                        </svg>

                                        <!--<i class="flaticon2-search-1"></i>-->
                                    </span>
                                </span>
                        <input type="text" class="form-control" placeholder="Search..." name="search" id="generalSearch" value="{{$search}}">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('body')
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="text-right mb-3">
        <a class="btn btn-brand btn-elevate" href="{{route('usersCreate')}}"><i class="fa fa-plus mx-2"></i>Add an employee</a>
    </div>
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body kt-portlet__body--fit">
            <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" id="kt_apps_user_list_datatable" style="">
                <table class="kt-datatable__table" style="display: block;">
                    <thead class="kt-datatable__head">
                        <tr class="kt-datatable__row" style="left: 0px;">
                            <th class="kt-datatable__cell"><span style="width: 180px;">Full name</span></th>
                            <th class="kt-datatable__cell"><span class="middle" style="width: 170px;">Username</span></th>
                            <th class="kt-datatable__cell"><span class="middle" style="width: 200px;">Email</span></th>
                            <th class="kt-datatable__cell"><span class="middle" style="width: 130px;">phone</span></th>
                            <th class="kt-datatable__cell"><span class="middle" style="width: 160px;">Join Date created</span></th>
                            <th class="kt-datatable__cell"><span class="middle" style="width: 130px;">Status</span></th>
                            <th class="kt-datatable__cell"><span class="middle" style="width: 80px;">Actions</span></th>
                        </tr>
                    </thead>
                    <tbody style="" class="kt-datatable__body">
                        @foreach($users as $key=>$user)
                        <tr data-row="{{$user->id}}" class="kt-datatable__row">
                            <td class="kt-datatable__cell--sorted kt-datatable__cell" data-field="AgentName"><span style="width: 180px;">
                                    <div class="kt-user-card-v2">
                                        <div class="kt-user-card-v2__details"> <span class="kt-user-card-v2__name" href="#">{{$user->first_name}} {{$user->last_name}}</span></div>
                                    </div>
                                </span>
                            </td>
                            <td data-field="Country" class="kt-datatable__cell"><span style="width: 170px;"class="middle">{{$user->username}}</span></td>
                            <td class="kt-datatable__cell" style=""><span class="middle" style="width: 200px;">{{$user->email}}</span></td>
                            <td data-field="Type" data-autohide-disabled="false" class="kt-datatable__cell"><span class="middle" style="width: 130px;"><span class="kt-font-bold kt-font-primary middle">{{$user->phone}}</span></span></td>
                            <td data-field="ShipDate" class="kt-datatable__cell" style=""><span class="middle" style="width: 160px;">{{$user->created_at->format('d/m/Y')}}</span></td>
                            <td data-field="ShipDate" class="kt-datatable__cell" style="width: 130px;">@if($user->status)
                                    <span class="badge badge-primary middle">Enabled</span>
                                @else
                                <span class="badge badge-secondary middle">Disabled</span>
                            @endif</td>
                            <td data-field="Actions" data-autohide-disabled="false" class="kt-datatable__cell"><span class="middle" style="overflow: visible; position: relative; width: 80px;">
                                    <div class="dropdown middle"> <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md"> <i class="flaticon-more-1"></i> </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <ul class="kt-nav">
                                                <li class="kt-nav__item"> <a class="kt-nav__link" href="{{route('usersEdit', $user->id)}}"> <i class="kt-nav__link-icon flaticon2-contract"></i> <span class="kt-nav__link-text">Edit user</span> </a> </li>
                                                @if($user->status)
                                                <li class="kt-nav__item"> <a class="kt-nav__link" href="{{route('usersblock', $user->id)}}"> <i class="kt-nav__link-icon flaticon2-cross"></i> <span class="kt-nav__link-text">Block user</span> </a> </li>
                                                @else
                                                <li class="kt-nav__item"> <a class="kt-nav__link" href="{{route('usersblock', $user->id)}}"> <i class="kt-nav__link-icon flaticon2-cross"></i> <span class="kt-nav__link-text">Unblock user</span> </a> </li>
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                </span></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="kt-portlet">
        <div class="kt-portlet__body">
            <div class="kt-pagination kt-pagination--brand">
                {{$users->links()}}
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $("#status").on('change', function(){
        $("#kt_subheader_search_form").submit();
    });
    function sweetalert() {
        swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {

            if (result.value) {
                swalWithBootstrapButtons.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                )
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                swalWithBootstrapButtons.fire(
                    'Cancelled',
                    'Your imaginary file is safe :)',
                    'error'
                )
            }
        })
    }
</script>
@endsection
