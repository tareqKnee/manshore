@extends('layout/layout')

@section('head')
<title>Dashboard</title>
@endsection

@section('body-header')
	<h3 class="kt-subheader__title">Dashboard</h3>
	<span class="kt-subheader__separator kt-subheader__separator--v"></span>
	<div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
		<input type="text" class="form-control" placeholder="Search order..." id="generalSearch">
		<span class="kt-input-icon__icon kt-input-icon__icon--right">
			<span><i class="flaticon2-search-1"></i></span>
		</span>
	</div>
@endsection
@section('body')
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
<div class="row">
    <div class="col-3" style="margin-right: 0;margin-left: 0; background-color:white; margin-left: 5px; width: 100%; height: 50%;">
            <canvas id="line-chart" style="width: 100%; height: 100%;"></canvas>
    </div>
    <div class="col-3" style="margin-right: 0;margin-left: 0; background-color:white; margin-left: 5px; width: 100%; height: 50%;">
            <canvas id="line-chart2" style="width: 100%; height: 100%;"></canvas>
    </div>
    <br>
    <div class="col-3" style="margin-right: 0;margin-left: 0; background-color:white; margin-left: 5px; width: 100%; height: 50%;">
            <canvas id="line-chart3" style="width: 100%; height: 100%;"></canvas>
    </div>
</div>
<br>
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" id="kt_apps_user_list_datatable" style="">
                        <table class="kt-datatable__table" style="display: block;">
                                <thead class="kt-datatable__head">
                                    <tr class="kt-datatable__row" style="left: 0px;">
                                        <th class="kt-datatable__cell"><span style="width: 200px;">Orders ID</span></th>
                                        <th class="kt-datatable__cell"><span class="middle" style="width: 150px;">Patient Name</span></th>
                                        <th class="kt-datatable__cell"><span class="middle" style="width: 180px;">Doctor Name</span></th>
                                        <th class="kt-datatable__cell"><span class="middle" style="width: 120px;">Date created</span></th>
                                        <th class="kt-datatable__cell"><span class="middle" style="width: 120px;">State</span></th>
                                        <th class="kt-datatable__cell"><span class="middle" style="width: 80px;">Actions</span></th>
                                    </tr>
                                </thead>
                                <tbody style="" class="kt-datatable__body">
                                    @foreach($orders as $key=>$order)
                                    @php
                                        if($order->made_by == 0){
                                            $status = "Waiting";
                                        }else{
                                            $status = "Active";
                                        }
                                    @endphp
                                    <tr data-row="{{$order->id}}" class="kt-datatable__row" style="left: 0px;">
                                        <td class="kt-datatable__cell--sorted kt-datatable__cell" data-field="AgentName"><span style="width: 200px;">
                                                <div class="kt-user-card-v2">
                                                    <div class="kt-user-card-v2__details"> <span class="kt-user-card-v2__name middle" href="#">{{$order->order_id}}</span></div>
                                                </div>
                                            </span>
                                        </td>
                                        <td data-field="Country" class="kt-datatable__cell"><span class="middle" style="width: 150px;">{{$order->patient_name}}</span></td>
                                        <td data-field="Country" class="kt-datatable__cell"><span class="middle" style="width: 180px;">{{$order->doctors->name}}</span></td>
                                        <td class="kt-datatable__cell" style=""><span class="middle" style="width: 120px;">{{$order->created_at->format('d/m/Y')}}</span></td>
                                        <td class="kt-datatable__cell" style="">@if($status == "Active")
                                            <span style="width: 120px;" class="badge badge-primary middle">{{$status}}</span>
                                            @else
                                            <span style="width: 120px;" class="badge badge-danger middle">{{$status}}</span>
                                            @endif</td>
                                        <td data-field="Actions" data-autohide-disabled="false" class="kt-datatable__cell"><span class="middle" style="overflow: visible; position: relative; width: 80px;">
                                                <div class="dropdown"> <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md"> <i class="flaticon-more-1"></i> </a>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <ul class="kt-nav">
                                                            <li class="kt-nav__item"> <a class="kt-nav__link" href="{{route('OrderViewOnly', $order->id)}}"> <i class="kt-nav__link-icon flaticon2-contract"></i> <span class="kt-nav__link-text">View Order</span> </a> </li>
                                                            <li class="kt-nav__item"> <a class="kt-nav__link" href="{{route('dassign', $order->id)}}"> <i class="kt-nav__link-icon flaticon2-contract"></i> <span class="kt-nav__link-text">Assign this order to me</span> </a> </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </span></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                </div>
            </div>
        </div>
        <div class="kt-portlet">
            <div class="kt-portlet__body">
                <div class="kt-pagination kt-pagination--brand">
                    {{$orders->links()}}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script>
    new Chart(document.getElementById("line-chart"), {
  type: 'line',
  data: {
    datasets: [{
        data: ['{{$orders->total()}}'],
        label: "Waiting",
        borderColor: "#FF0000",
        fill: false
      }
    ]
  },
  options: {
    title: {
      display: true,
    }
  }
});

new Chart(document.getElementById("line-chart2"), {
  type: 'line',
  data: {
    datasets: [{
        data: ['{{$count2}}'],
        label: "Active",
        borderColor: "#000080",
        fill: false
      }
    ]
  },
  options: {
    title: {
      display: true,
    }
  }
});

new Chart(document.getElementById("line-chart3"), {
  type: 'line',
  data: {
    datasets: [{
        data: ['{{$count3}}'],
        label: "Completed",
        borderColor: "#008000",
        fill: false
      }
    ]
  },
  options: {
    title: {
      display: true,
    }
  }
});

</script>
@endsection
