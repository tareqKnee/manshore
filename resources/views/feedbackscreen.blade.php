<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <!--page style css-->
    <link rel="stylesheet" href="/css/style.css">
    <title>{{$title}}</title>
  </head>
  <body>
    <div class="main-body w-100">
        <h2>{{$title}}</h2>
        <table class="table table-dark">
            <thead>
              <tr>
                <th class="tableheader"  scope="col">Doctor name</th>
                <th class="tableheader" scope="col">Patient's Name</th>
                <th class="tableheader" scope="col">Rate</th>
                <th class="tableheader" scope="col">Note</th>
              </tr>
            </thead>
            <tbody>
                @foreach($orders as $order)
              <tr>
                <td>{{$order->doctor->name}}</td>
                <td>{{$order->patient_name}}</td>
                <td>@if($order->rate == 0)
                    Unhappy
                    @elseif($order->rate == 1)
                     Okay
                    @else
                      Happy
                    @endif</td>
                <td>
                    {{$order->note}}
                </td>
              </tr>
              @endforeach
            </tbody>
        </table>
        <div class="invisible-cases">
            <div>
                <span><img src="/images/info.png"></span>
                <span>{{max(0,$orders->total() - 7)}}+  Invisible Cases</span>
            </div>
        </div>
    </div>
     <!-- Bootstrap JS -->
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
     <script src="/js/bootstrap.min.js"></script>
     <!--main js-->
        <script>
        var url =  window.location.href;
          $(document).ready(function() {
              setInterval(function() {
                 location.reload();
              },60000);
          });
        </script>
  </body>
</html>
