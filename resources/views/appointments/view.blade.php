@extends('layout/layout')

@section('head')
<title>Edit an appointment</title>
@endsection

@section('body-header')
<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">
				New appointment
			</h3>
			<span class="kt-subheader__separator kt-subheader__separator--v"></span>
			<div class="kt-subheader__group" id="kt_subheader_search">
				<span class="kt-subheader__desc" id="kt_subheader_total">
					Enter appointment details and submit </span>
			</div>
		</div>
	</div>
</div>
<!-- end:: Content Head -->
@endsection

@section('body')

									<!--begin::Portlet-->
									<div class="kt-portlet">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Edit an appointment
												</h3>
											</div>
										</div>

										<!--begin::Form-->
                                        <form class="kt-form" method="POST" action="{{route('appointmentsupdate')}}">
                                            @csrf
                                            <input hidden name="id" value="{{$app->id}}">
											<div class="kt-portlet__body">
                                                    <label for="doctor_id">Doctor name: </label>
                                                    <select class="form-control" name="doctor_id" id="doctor_id">
                                                            <option selected hidden disabled>Select your doctor</option>
                                                            @foreach($doctors as $doctor)
                                                            <option value="{{$doctor->id}}" {{$app->doctor_id == $doctor->id ? 'selected' : ''}}>{{$doctor->name}}</option>
                                                            @endforeach
                                                            <span class="form-text text-muted">Please choose your assigned doctor's full name</span>
                                                        </select>
                                                        <label for="camera">Camera: </label>
                                                        <select class="form-control" name="camera" id="camera">
                                                                @foreach($cameras as $camera)
                                                                <option value="{{$camera->id}}" {{$app->camera_id == $camera->id ? 'selected' : ''}}>{{$camera->name}}</option>
                                                                @endforeach
                                                                <span class="form-text text-muted">Please choose your assigned camera</span>
                                                            </select>
                                                <div class="form-group">
                                                        <label for="exampleTextarea">Description: </label>
                                                        <textarea class="form-control" name="description" id="exampleTextarea" rows="3" placeholder="Enter the reason of your appointment">{{$app->description}}</textarea>
                                                    </div>
                                                    <div class="form-group">
                                                            <label>Date: </label>
                                                            <input id="delivery_date" class="form-control input-group-lg reg_name" type="date" name="date"
                                                            title="Enter delivery date"
                                                            placeholder="delivery date" value="{{$app->description}}"/>
                                                            <span class="form-text text-muted">Choose the delivery date</span>
                                                    </div>
                                                    <div class="form-group row">
                                                            <label for="example-tel-input" class="col-2 col-form-label">Time: </label>
                                                            <div class="form-group row">
                                                                    <div class="col-9">
                                                                        <div class="kt-radio-inline">
                                                                            <label class="kt-radio">
                                                                                <input type="radio" class="status" id="9AM - 10AM" name="time" value="9AM - 10PM" {{$app->time == "9AM - 10AM" ? 'checked' : ''}}> 9AM - 10AM
                                                                                <span></span>
                                                                            </label>
                                                                            <label class="kt-radio">
                                                                                <input type="radio" class="status" id="10AM - 11AM" checked="checked" name="time"  value="10AM - 11AM" {{$app->time == "10AM - 11AM" ? 'checked' : ''}}> 10AM - 11AM
                                                                                <span></span>
                                                                            </label>
                                                                            <label class="kt-radio">
                                                                                <input type="radio" class="status" id="11AM - 12PM" name="time"  value="11AM - 12PM" {{$app->time == "11AM - 12PM" ? 'checked' : ''}}> 11AM - 12PM
                                                                                <span></span>
                                                                            </label>
                                                                            <label class="kt-radio">
                                                                                    <input type="radio" class="status" id="12PM - 1PM" name="time"  value="12PM - 1PM" {{$app->time == "12PM - 1PM" ? 'checked' : ''}}> 12PM - 1PM
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="kt-radio">
                                                                                    <input type="radio" class="status" id="1PM - 2PM" name="time"  value="1PM - 2PM" {{$app->time == "1PM - 2PM" ? 'checked' : ''}}> 1PM - 2PM
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="kt-radio">
                                                                                    <input type="radio" class="status" id="2PM - 3PM" name="time"  value="2PM - 3PM" {{$app->time == "2PM - 3PM" ? 'checked' : ''}}> 2PM - 3PM
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="kt-radio">
                                                                                    <input type="radio" class="status" id="3PM - 4PM" name="time"  value="3PM - 4PM" {{$app->time == "3PM - 4PM" ? 'checked' : ''}}> 3PM - 4PM
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="kt-radio">
                                                                                    <input type="radio" class="status" id="4PM - 5PM" name="time"  value="4PM - 5PM" {{$app->time == "4PM - 5PM" ? 'checked' : ''}}> 4PM - 5PM
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="kt-radio">
                                                                                    <input type="radio" class="status" id="5PM - 6PM" name="time"  value="5PM - 6PM" {{$app->time == "5PM - 6PM" ? 'checked' : ''}}> 5PM - 6PM
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="kt-radio">
                                                                                    <input type="radio" class="status" id="6PM - 7PM" name="time"  value="6PM - 7PM" {{$app->time == "6PM - 7PM" ? 'checked' : ''}}> 6PM - 7PM
                                                                                    <span></span>
                                                                                </label>
                                                                        </div>
                                                                        <span class="form-text text-muted">Choose the time of your appointment</span>
                                                                    </div>
                                                                </div>
											</div>
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<button type="submit" class="btn btn-primary">Submit</button>
													<button type="reset" class="btn btn-danger">Reset</button>
												</div>
											</div>
										</form>

										<!--end::Form-->
									</div>

<!-- end:: Content -->
@endsection
@section('scripts')
<script>
    document.getElementById('delivery_date').valueAsDate = new Date();
    $(document).ready(function() {
    $('#delivery_date').on('change', function(){
        var date = $('#delivery_date').val();
      $.ajax({
              url: '/appoints/getAvail/'+date,
              cache: false,
            success: function(res) {
                $('.status').prop("disabled", false);
                res.apps.forEach(element => {
                    var id = "#"+element;
                    $(element).prop("disabled", true);
                });
			}
      });
  });
    });
</script>
@endsection
