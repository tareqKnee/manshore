@extends('layout/layout')

@section('head')
<title>Scans by camera</title>
@endsection

@section('body-header')
<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">
				Scans by camera
			</h3>
			<span class="kt-subheader__separator kt-subheader__separator--v"></span>
			<div class="kt-subheader__group" id="kt_subheader_search">
				<span class="kt-subheader__desc" id="kt_subheader_total">
					Cameras the details and submit </span>
			</div>
		</div>
	</div>
</div>
<!-- end:: Content Head -->
@endsection

@section('body')
@php
if(session('data')){
    $data = session('data');
} else {
    $data = null;
}
@endphp
									<!--begin::Portlet-->
									<div class="kt-portlet">

										<!--begin::Form-->
                                        <form class="kt-form" method="POST" action="{{route('postCamera')}}">
                                            @csrf
											<div class="kt-portlet__body">
												<div class="form-group">
													<label>From date</label>
                                                    <input type="date" class="form-control" name="from" value="{{$data ? $data['from'] : ''}}">
                                                    @if ($errors->has('from'))
                                                    <span class="help-block" style="color: red">{{ $errors->first('from') }}</span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
													<label>To date</label>
                                                    <input type="date" class="form-control" name="to" value="{{$data ? $data['to'] : ''}}">
                                                    @if ($errors->has('to'))
                                                    <span class="help-block" style="color: red">{{ $errors->first('to') }}</span>
                                                    @endif
                                                </div>
                                                <div class="form-group">
													<label>Camera name</label>
                                                                    <select class="form-control" id="camera" name="camera">
                                                                        <option disabled selected hidden>Select your camera</option>
                                                                        <option value="all">All cameras</option>
                                                                        @foreach($cameras as $doc)
                                                                            <option value="{{$doc->id}}" {{$data ? ($data['camera'] == $doc->id ? 'selected' : '') : ''}}>{{$doc->name}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    @if ($errors->has('camera'))
                                                                    <span class="help-block" style="color: red">{{ $errors->first('camera') }}</span>
                                                                    @endif
                                                        </div>
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<button type="submit" class="btn btn-primary">Submit</button>
													<button type="reset" class="btn btn-danger">Reset</button>
												</div>
											</div>
										</form>

                                        @if(Session::get('status'))
                                        @php
                                            $mat = Session::get('status');
                                        @endphp
                                        <strong><p> # of appointments : {{$mat}} </p></strong>
                                        @endif
										<!--end::Form-->
									</div>

<!-- end:: Content -->
@endsection
@section('script')
<script>
	$('select[name="position"]').on('change', function() {
		var selected = $(this).find('option:selected');
		var extra = selected.data('content');
		if (extra == 'B') {
			$('#TypeB').removeAttr('hidden')
		} else {
			$('#TypeB').prop('hidden', true)
		}
		console.log(extra)
	})
</script>
@stop
