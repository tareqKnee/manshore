@extends('layout/layout')

@section('head')
<title>Create an appointment</title>
@endsection

@section('body-header')
<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">
				New appointment
			</h3>
			<span class="kt-subheader__separator kt-subheader__separator--v"></span>
			<div class="kt-subheader__group" id="kt_subheader_search">
				<span class="kt-subheader__desc" id="kt_subheader_total">
					Enter appointment details and submit </span>
			</div>
		</div>
	</div>
</div>
<!-- end:: Content Head -->
@endsection

@section('body')

									<!--begin::Portlet-->
									<div class="kt-portlet">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Create an appointment
												</h3>
											</div>
										</div>

										<!--begin::Form-->
                                        <form class="kt-form" method="POST" action="{{route('appointmentsInsertion')}}">
                                            @csrf
											<div class="kt-portlet__body">
                                                    <label for="doctor_id">Doctor name: </label>
                                                    <select class="form-control" name="doctor_id" id="doctor_id">
                                                            <option selected hidden disabled>Select your doctor</option>
                                                            @foreach($doctors as $doctor)
                                                            <option value="{{$doctor->id}}" {{old('doctor_id') == $doctor->id ? 'selected' : ''}}>{{$doctor->name}}</option>
                                                            @endforeach
                                                            <span class="form-text text-muted">Please choose your assigned doctor's full name</span>
                                                        </select>
                                                        <label for="camera">Camera: </label>
                                                        <select class="form-control" name="camera" id="camera" onchange="getUnAvailableTime()">
                                                                @foreach($cameras as $camera)
                                                                <option value="{{$camera->id}}" {{old('camera') == $camera->id ? 'selected' : ''}}>{{$camera->name}}</option>
                                                                @endforeach
                                                                <span class="form-text text-muted">Please choose your assigned camera</span>
                                                            </select>
                                                <div class="form-group">
                                                        <label for="exampleTextarea">Description: </label>
                                                        <textarea class="form-control" name="description" id="exampleTextarea" rows="3" placeholder="Enter the reason of your appointment">{{old('description')}}</textarea>
                                                    </div>
                                                    <div class="form-group">
                                                            <label>Date: </label>
                                                            <input id="delivery_date" onchange="getUnAvailableTime()" class="form-control input-group-lg reg_name" type="date" name="date"
                                                            title="Enter delivery date"
                                                            placeholder="delivery date" value="{{old('delivery_date')}}"/>
                                                            <span class="form-text text-muted">Choose the delivery date</span>
                                                    </div>
                                                    <div class="form-group row">
                                                            <label for="example-tel-input" class="col-2 col-form-label">Time: </label>
                                                            <div class="form-group row">
                                                                    <div class="col-9">
                                                                        <div class="kt-radio-inline">
                                                                            <label class="kt-radio">
                                                                                <input type="radio" class="status" id="9AM-10AM" name="time" required value="9AM - 10AM" {{old('time') == '9AM - 10AM' ? 'checked' : ''}}> 9AM - 10AM
                                                                                <span></span>
                                                                            </label>
                                                                            <label class="kt-radio">
                                                                                <input type="radio" class="status" id="10AM-11AM" checked="checked" name="time" required value="10AM - 11AM" {{old('time') == '10AM - 11AM' ? 'checked' : ''}}> 10AM - 11AM
                                                                                <span></span>
                                                                            </label>
                                                                            <label class="kt-radio">
                                                                                <input type="radio" class="status" id="11AM-12PM" name="time" required value="11AM - 12PM" {{old('time') == '11AM - 12PM' ? 'checked' : ''}}> 11AM - 12PM
                                                                                <span></span>
                                                                            </label>
                                                                            <label class="kt-radio">
                                                                                    <input type="radio" class="status" id="12PM-1PM" name="time" required value="12PM - 1PM" {{old('time') == '12PM - 1PM' ? 'checked' : ''}}> 12PM - 1PM
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="kt-radio">
                                                                                    <input type="radio" class="status" id="1PM-2PM" name="time" required value="1PM - 2PM" {{old('time') == '1PM - 2PM' ? 'checked' : ''}}> 1PM - 2PM
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="kt-radio">
                                                                                    <input type="radio" class="status" id="2PM-3PM" name="time" required value="2PM - 3PM" {{old('time') == '2PM - 3PM' ? 'checked' : ''}}> 2PM - 3PM
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="kt-radio">
                                                                                    <input type="radio" class="status" id="3PM-4PM" name="time" required value="3PM - 4PM" {{old('time') == '3PM - 4PM' ? 'checked' : ''}}> 3PM - 4PM
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="kt-radio">
                                                                                    <input type="radio" class="status" id="4PM-5PM" name="time" required value="4PM - 5PM" {{old('time') == '4PM - 5PM' ? 'checked' : ''}}> 4PM - 5PM
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="kt-radio">
                                                                                    <input type="radio" class="status" id="5PM-6PM" name="time" required value="5PM - 6PM" {{old('time') == '5PM - 6PM' ? 'checked' : ''}}> 5PM - 6PM
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="kt-radio">
                                                                                    <input type="radio" class="status" id="6PM-7PM" name="time" required value="6PM - 7PM" {{old('time') == '6PM - 7PM' ? 'checked' : ''}}> 6PM - 7PM
                                                                                    <span></span>
                                                                                </label>
                                                                        </div>
                                                                        <span class="form-text text-muted">Choose the time of your appointment</span>
                                                                    </div>
                                                                </div>
											</div>
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<button type="submit" class="btn btn-primary">Submit</button>
													<button type="reset" class="btn btn-danger">Reset</button>
												</div>
											</div>
										</form>

										<!--end::Form-->
									</div>

<!-- end:: Content -->
@endsection
@section('scripts')
<script>
    document.getElementById('delivery_date').valueAsDate = new Date();
    $(document).ready(function() {
        getUnAvailableTime();
        $('#delivery_date').on('change', function(){
            getUnAvailableTime();
        });
    });

    function getUnAvailableTime() {
        var date = $('#delivery_date').val();
        var camera = $('#camera').val();
        $.ajax({
                url: '/appoints/getAvail/'+date+'/'+camera,
                cache: false,
                success: function(res) {
                    $('.kt-radio .status').prop("disabled", false);
                    $('.kt-radio .status').parent().removeClass("c-red");
                    res.apps.forEach(element => {
                        elementId = element.split(" ").join("")
                        var id = "#"+elementId;
                        $(id).prop("disabled", true);
                        $(id).prop("checked", false);
                        $(id).parent().addClass("c-red");
                    });
                }
        });
    }
</script>
@endsection
