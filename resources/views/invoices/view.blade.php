@extends('layout/layout')

@section('head')
<title>View invoice</title>
@endsection

@section('body-header')
<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">
				View invoice
			</h3>
			<span class="kt-subheader__separator kt-subheader__separator--v"></span>
			<div class="kt-subheader__group" id="kt_subheader_search">
			</div>
		</div>
	</div>
</div>
<style>
    .kt-invoice-1 {
  border-top-left-radius: 4px;
  border-top-right-radius: 4px; }
  .kt-invoice-1 .kt-invoice__container {
    width: 100%;
    margin: 0;
    padding: 0 30px; }
  .kt-invoice-1 .kt-invoice__head {
    border-top-left-radius: 4px;
    border-top-right-radius: 4px;
    background-size: cover;
    background-repeat: no-repeat;
    padding: 80px 0; }
    .kt-invoice-1 .kt-invoice__head .kt-invoice__container {
      border-top-left-radius: 4px;
      border-top-right-radius: 4px; }
    .kt-invoice-1 .kt-invoice__head .kt-invoice__brand {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -webkit-box-pack: justify;
      -ms-flex-pack: justify;
      justify-content: space-between;
      -ms-flex-wrap: wrap;
      flex-wrap: wrap; }
      .kt-invoice-1 .kt-invoice__head .kt-invoice__brand .kt-invoice__title {
        font-weight: 700;
        font-size: 2.7rem;
        margin-right: 10px;
        margin-top: 5px;
        color: #fff;
        vertical-align: top; }
      .kt-invoice-1 .kt-invoice__head .kt-invoice__brand .kt-invoice__logo {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        margin-top: 5px;
        text-align: right; }
        .kt-invoice-1 .kt-invoice__head .kt-invoice__brand .kt-invoice__logo img {
          text-align: right; }
        .kt-invoice-1 .kt-invoice__head .kt-invoice__brand .kt-invoice__logo .kt-invoice__desc {
          display: -webkit-box;
          display: -ms-flexbox;
          display: flex;
          -webkit-box-orient: vertical;
          -webkit-box-direction: normal;
          -ms-flex-direction: column;
          flex-direction: column;
          text-align: right;
          padding: 1rem 0 1rem 0;
          color: rgba(255, 255, 255, 0.7); }
    .kt-invoice-1 .kt-invoice__head .kt-invoice__items {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -ms-flex-wrap: wrap;
      flex-wrap: wrap;
      margin-top: 50px;
      width: 100%;
      border-top: 1px solid rgba(255, 255, 255, 0.2); }
      .kt-invoice-1 .kt-invoice__head .kt-invoice__items .kt-invoice__item {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        -webkit-box-flex: 1;
        -ms-flex: 1;
        flex: 1;
        color: #fff;
        margin-right: 10px;
        margin-top: 20px; }
        .kt-invoice-1 .kt-invoice__head .kt-invoice__items .kt-invoice__item:last-child {
          margin-right: 0; }
        .kt-invoice-1 .kt-invoice__head .kt-invoice__items .kt-invoice__item .kt-invoice__subtitle {
          font-weight: 500;
          padding-bottom: 0.5rem; }
        .kt-invoice-1 .kt-invoice__head .kt-invoice__items .kt-invoice__item .kt-invoice__text {
          color: rgba(255, 255, 255, 0.7); }
  .kt-invoice-1 .kt-invoice__body {
    padding: 3rem 0; }
    .kt-invoice-1 .kt-invoice__body table {
      background-color: transparent; }
      .kt-invoice-1 .kt-invoice__body table thead tr th {
        background-color: transparent;
        padding: 1rem 0 0.5rem 0;
        border-top: none;
        color: #74788d; }
        .kt-invoice-1 .kt-invoice__body table thead tr th:not(:first-child) {
          text-align: right; }
      .kt-invoice-1 .kt-invoice__body table tbody tr td {
        background-color: transparent;
        padding: 1rem 0 1rem 0;
        border-top: none;
        font-weight: 700;
        font-size: 1.1rem;
        color: #595d6e; }
        .kt-invoice-1 .kt-invoice__body table tbody tr td:not(:first-child) {
          text-align: right; }
        .kt-invoice-1 .kt-invoice__body table tbody tr td:last-child {
          color: #FE21BE;
          font-size: 1.2rem; }
      .kt-invoice-1 .kt-invoice__body table tbody tr:first-child td {
        padding-top: 1.8rem; }
  .kt-invoice-1 .kt-invoice__footer {
    padding: 3rem 0;
    background-color: #f7f8fa; }
    .kt-invoice-1 .kt-invoice__footer .kt-invoice__container {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -webkit-box-orient: horizontal;
      -webkit-box-direction: normal;
      -ms-flex-direction: row;
      flex-direction: row;
      -webkit-box-pack: justify;
      -ms-flex-pack: justify;
      justify-content: space-between;
      -ms-flex-wrap: wrap;
      flex-wrap: wrap; }
    .kt-invoice-1 .kt-invoice__footer .kt-invoice__bank {
      margin-right: 10px;
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -webkit-box-orient: vertical;
      -webkit-box-direction: normal;
      -ms-flex-direction: column;
      flex-direction: column;
      -ms-flex-wrap: wrap;
      flex-wrap: wrap; }
      .kt-invoice-1 .kt-invoice__footer .kt-invoice__bank .kt-invoice__title {
        font-size: 1.2rem;
        text-transform: capitalize;
        font-weight: 600;
        color: #74788d; }
      .kt-invoice-1 .kt-invoice__footer .kt-invoice__bank .kt-invoice__item {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        -webkit-box-pack: justify;
        -ms-flex-pack: justify;
        justify-content: space-between;
        margin-top: 10px; }
        .kt-invoice-1 .kt-invoice__footer .kt-invoice__bank .kt-invoice__item .kt-invoice__label {
          font-size: 1.1rem;
          font-weight: 500;
          margin-right: 40px;
          color: #595d6e;
          text-align: left; }
        .kt-invoice-1 .kt-invoice__footer .kt-invoice__bank .kt-invoice__item .kt-invoice__value {
          font-size: 1.1rem;
          font-weight: 400;
          color: #74788d;
          text-align: right; }
    .kt-invoice-1 .kt-invoice__footer .kt-invoice__total {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -webkit-box-orient: vertical;
      -webkit-box-direction: normal;
      -ms-flex-direction: column;
      flex-direction: column;
      text-align: right; }
      .kt-invoice-1 .kt-invoice__footer .kt-invoice__total .kt-invoice__title {
        font-size: 1.2rem;
        text-transform: capitalize;
        font-weight: 600;
        color: #74788d; }
      .kt-invoice-1 .kt-invoice__footer .kt-invoice__total .kt-invoice__price {
        color: #FE21BE;
        font-weight: 700;
        font-size: 24px; }
      .kt-invoice-1 .kt-invoice__footer .kt-invoice__total .kt-invoice__notice {
        font-size: 1rem;
        font-weight: 500;
        color: #74788d; }
  .kt-invoice-1 .kt-invoice__actions {
    padding: 2rem 0; }
    .kt-invoice-1 .kt-invoice__actions .kt-invoice__container {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -webkit-box-orient: horizontal;
      -webkit-box-direction: normal;
      -ms-flex-direction: row;
      flex-direction: row;
      -webkit-box-pack: justify;
      -ms-flex-pack: justify;
      justify-content: space-between; }

@media (min-width: 1025px) {
  .kt-invoice-1 .kt-invoice__container {
    width: 80%;
    margin: 0 auto; } }

@media (max-width: 768px) {
  .kt-invoice-1 .kt-invoice__container {
    width: 100%;
    margin: 0;
    padding: 0 20px; }
  .kt-invoice-1 .kt-invoice__head {
    padding: 20px 0; }
    .kt-invoice-1 .kt-invoice__head .kt-invoice__brand {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -webkit-box-orient: vertical;
      -webkit-box-direction: normal;
      -ms-flex-direction: column;
      flex-direction: column; }
      .kt-invoice-1 .kt-invoice__head .kt-invoice__brand .kt-invoice__title {
        font-weight: 700;
        font-size: 2rem;
        margin-bottom: 30px; }
      .kt-invoice-1 .kt-invoice__head .kt-invoice__brand .kt-invoice__logo {
        text-align: left; }
        .kt-invoice-1 .kt-invoice__head .kt-invoice__brand .kt-invoice__logo img {
          text-align: left; }
        .kt-invoice-1 .kt-invoice__head .kt-invoice__brand .kt-invoice__logo .kt-invoice__desc {
          text-align: left; }
    .kt-invoice-1 .kt-invoice__head .kt-invoice__items {
      margin-top: 20px; }
  .kt-invoice-1 .kt-invoice__body {
    padding: 2rem 0; }
  .kt-invoice-1 .kt-invoice__footer {
    padding: 2rem 0; }
    .kt-invoice-1 .kt-invoice__footer .kt-invoice__container {
      -webkit-box-orient: vertical;
      -webkit-box-direction: normal;
      -ms-flex-direction: column;
      flex-direction: column; }
    .kt-invoice-1 .kt-invoice__footer .kt-invoice__bank .kt-invoice__item .kt-invoice__label {
      margin-right: 20px; }
    .kt-invoice-1 .kt-invoice__footer .kt-invoice__total {
      margin-top: 30px;
      text-align: left; } }

@media print {
  .kt-header,
  .kt-header-mobile,
  .kt-aside,
  .kt-footer,
  .kt-subheader,
  .kt-scrolltop,
  .kt-quick-panel,
  .kt-demo-panel,
  .kt-sticky-toolbar {
    display: none !important; }
  body,
  .kt-wrapper,
  .kt-body,
  .kt-content {
    background: transparent !important;
    padding: 0 !important;
    margin: 0 !important; }
  .kt-invoice-1 {
    border-top-left-radius: 0;
    border-top-right-radius: 0; }
    .kt-invoice-1 .kt-invoice__head {
      border-top-left-radius: 0;
      border-top-right-radius: 0; }
      .kt-invoice-1 .kt-invoice__head .kt-invoice__container {
        border-top-left-radius: 0;
        border-top-right-radius: 0; }
    .kt-invoice-1 .kt-invoice__actions {
      display: none !important; }
    .kt-invoice-1 .kt-invoice__container {
      width: 100%;
      padding: 0 10px; } }

</style>
<!-- end:: Content Head -->
@endsection
@section('body')
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__body kt-portlet__body--fit">
            <div class="kt-invoice-1">
                <div class="kt-invoice__head" style="background-image: url(/uploads/bg-6.jpg);">
                    <div class="kt-invoice__container">
                        <div class="kt-invoice__brand">
                            <div class="kt-invoice__item">
                                <h1 class="kt-invoice__title invoice-text">INVOICE</h1>
                                <span class="kt-invoice__subtitle c-white invoice-text">INVOICE NO:</span>
                                <span class="kt-invoice__text c-white invoice-text">{{$order->id}}</span>
                            </div>
                            <div href="#" class="kt-invoice__logo">
                                <a href="#"><img src="/uploads/logo_client_white.png"></a>
                            </div>
                        </div>
                        <div class="row mt-5">
                            <div class="col-4">
                                <h5 class="c-white invoice-text">Doctor name:</h5>
                                <h5 class="c-white invoice-text">{{$order->order->doctors->name}}</h5>
                            </div>
                            <div class="col-4">
                                <h5 class="c-white invoice-text">Patient name:</h5>
                                <h5 class="c-white invoice-text">{{$order->order->patient_name}}</h5>
                            </div>
                            <div class="col-4">
                                <h5 class="c-white text-right invoice-text">Date:</h5>
                                <h5 class="c-white text-right invoice-text">{{$order->created_at->format('M d/Y')}}</h5>
                            </div>
                        </div>
                        <br>
                    </div>
                </div>
                <div class="kt-invoice__body">
                    <div class="kt-invoice__container">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th class="invoice-text">JOB TYPE</th>
                                        <th class="invoice-text">MATERIAL NAME</th>
                                        <th class="invoice-text">UNIT PRICE</th>
                                        {{-- <th class="invoice-text">DISCOUNT</th> --}}
                                        <th class="invoice-text">TOTAL UNIT(S)</th>
                                        <th class="invoice-text">AMOUNT</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $sum = 0;
                                    @endphp
                                    @foreach($order->order->jobs as $job)
                                    @php
                                        $num = explode(',', $job->unit_num);
                                        $total = $job->material->price * count($num);
                                        $sum += $total;
                                    @endphp
                                    <tr>
                                        <td class="invoice-text">{{$job->types->name}}</td>
                                        <td class="invoice-text">{{$job->material->name}}</td>
                                        <td class="invoice-text">{{$job->material->price}}</td>
                                        {{-- @php
                                            $type = $job->order->doctors->discounts->where('material_id', $job->material_id)->first();
                                            $amount = $type ? $type->discount : 0;
                                        @endphp
                                        <td class="invoice-text">{{$type === 0 ? $amount : $amount . '%'}}</td> --}}
                                        <td class="invoice-text">{{count($num)}}</td>
                                        <td class="invoice-text">{{$total}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="kt-invoice__footer">
                    <div class="kt-invoice__container">
                            <div class="kt-invoice__bank" style="visibility: hidden">
                                    <div class="kt-invoice__title invoice-text">DISCOUNT DETAILS</div>
                                    <div class="kt-invoice__item">
                                        <span class="kt-invoice__label invoice-text">DISCOUNT TYPE:</span>
                                        <span class="kt-invoice__value invoice-text" style="text-transform: capitalize;"></span></span>
                                    </div>
                                    <div class="kt-invoice__item">
                                        <span class="kt-invoice__label invoice-text">DISCOUNT RATE:</span>
                                        <span class="kt-invoice__value invoice-text"></span></span>
                                    </div>
                                </div>
                        <div class="kt-invoice__total">
                            <h1 class="kt-invoice__title invoice-text">Total Amount</h1>
                            <h1 class="kt-invoice__price invoice-text">{{$order->amount}}</h1>
                        </div>
                        <div class="invoice-location mt-5 w-100 text-center">
                            <p class="invoice-text">Dabouq - Khair El Deen El Ma'ani St. B # 46, Amman, Jordan</p>
                        </div>
                    </div>
                </div>
                <div class="kt-invoice__actions">
                    <div class="kt-invoice__container">
                        <button type="button" class="btn btn-label-brand btn-bold" onclick="window.print();">Download Invoice</button>
                        <button type="button" class="btn btn-brand btn-bold" onclick="window.print();">Print Invoice</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
