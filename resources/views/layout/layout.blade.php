@include('layout/header')

<head>
    @yield('head')
</head>

<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

    @include('layout/sidebar')
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
        					<!-- begin:: Header -->
					<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">
                        <div class="col-md-4">
                        <a class="back-button btn" id="goback" role="button"><i class="fa fa-chevron-left" aria-hidden="true"></i>Back</a>
                        </div>
                            <div class="kt-header__topbar">
                                <div class="kt-header__topbar-item dropdown">
                                    <div class="kt-header__topbar-wrapper row" data-offset="30px,0px" aria-expanded="true">
                                            @php
                                            $permissions = Cache::get('user'.Auth()->user()->id);
                                       @endphp
                                        @if(Auth()->user()->is_admin || $permissions && $permissions->contains('permission_id', 1))
                                        <div style="margin:auto; width: 100%; height: 50%; margin-right: 10px;">
                                                <a href="{{route('returnCreate')}}" class="btn btn-brand btn-elevate btn-pill"><i class="fa fa-plus"></i>Add case</a>
                                                @if(Auth()->user()->is_admin)
                                                <a href="{{route('usersEdit', Auth()->user()->id)}}" style="vertical-align: middle;"><i class="fas fa-2x fa-cog"></i></a>
                                                @endif
                                            </div>
                                    @endif
                                    </div>
                                </div>

                                <!--end: Quick Actions -->

                                <!--begin: My Cart -->

                                <div class="kt-header__topbar-item kt-header__topbar-item--user">
                                    <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">
                                        <div class="kt-header__topbar-user">
                                            <span class="kt-header__topbar-welcome kt-hidden-mobile">Hi,</span>
                                            <span class="kt-header__topbar-username kt-hidden-mobile">{{auth()->user()->first_name . ' ' . Auth()->user()->last_name}}</span>
                                            <img class="kt-hidden" alt="Pic" src="assets/media/users/300_25.jpg" />

                                            <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                                            <span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold">{{Auth()->user()->first_name[0]}}</span>
                                        </div>
                                    </div>
                                    <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">

                                        <!--end: Head -->

                                        <!--begin: Navigation -->
                                        <div class="kt-notification">
                                            <div class="kt-notification__custom kt-space-between">
                                                    <form action="{{route('logout')}}" method="POST" style="margin: auto;">
                                                            @csrf
                                                                    <button type="submit" class="btn btn-label btn-label-brand btn-sm btn-bold">
                                                                    <i class="fa fa-sign-out-alt"></i> Log out
                                                                    </button></form>
                                            </div>
                                        </div>

                                        <!--end: Navigation -->
                                    </div>
                                </div>

                                <!--end: User Bar -->
                            </div>

                            <!-- end:: Header Topbar -->
                        </div>
        <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
            <div class="kt-subheader  kt-grid__item" id="kt_subheader">
                <div class="kt-container  kt-container--fluid ">
                    <div class="kt-subheader__main">
                        @yield('body-header')
                    </div>
                </div>
            </div>
            <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <div class="row">
                        @if(session('success'))
                        <div class="alert alert-success" role="alert">
                            <div class="alert-icon"><i class="flaticon-success"></i></div>
                            <div class="alert-text">{{session("success")}}</div>
                        </div>
                        @endif
                        @if(session('error'))
                        <div class="alert alert-danger" role="alert">
                            <div class="alert-icon"><i class="flaticon-danger"></i></div>
                            <div class="alert-text">{{session("error")}}</div>
                        </div>
                        @endif
                        @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                    @yield('body')
                </div>
            </div>
        </div>
        <div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-footer__copyright">
                    2020&nbsp;&copy;&nbsp;<a target="_blank" class="kt-link">Manshore Medical Supplies LLC. All Rights Reserved</a>
                </div>
            </div>
        </div>
    </div>
    @include('layout/scripts')
    @yield('scripts')
</body>
